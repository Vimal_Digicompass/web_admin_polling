# 1SqFt AdminPoll Admin Interface

Admin poll is an intuitive CRM tool for Back office users to coordinate with customers in terms of various scenarios to increase the productivity of the [1SqFt](https://www.1sqft.com) Product.
- Round Robin is the key algorithm that we are using in the Admin poll to assign the records to the Back office user in order to assign records consistently. It allows us to track and monitor their mapping and log activities so they can forecast sales and plan ahead. For sales representatives, Admin Polls makes it easy to manage customer profile and lead information. 
- [Laravel 8](https://laravel.com/docs/8.x) framework is used for building application.


## Dependencies

- [PHP 7.4](https://www.php.net/) or above
- [PHP mongodb extension](https://pecl.php.net/package/mongodb)
- [Composer package manager](https://getcomposer.org/)


## Configuration

1. Copy ```.env.example file``` to ```.env``` and fill in environment variables.
2. Install dependencies by running ``composer install``.


## Authentication

Web app and first party requests are authenticated using session based on user credentials.
