<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostrejectedChangelogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_rejected_changelog', function (Blueprint $table) {
            $table->Increments('post_rej_changelog_id');
            $table->integer('post_id')->index();
            $table->string('post_type')->index();
            $table->string('fieldname');
            $table->string('newvalue');
            $table->string('oldvalue');
            $table->string('done')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postrejected_changelog');
    }
}
