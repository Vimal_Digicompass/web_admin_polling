<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFavouritesArchieveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favourites_archieve', function (Blueprint $table) {
            $table->Increments('fav_arch_id');
            $table->integer('user_id')->index();
            $table->string('post_type')->index();
            $table->integer('post_id')->index();
            $table->integer('city_id')->index();
            $table->integer('status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favourites_archieve');
    }
}
