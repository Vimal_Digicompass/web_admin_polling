<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserNotenquirePostChangelogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_notenquire_post_changelog', function (Blueprint $table) {
            $table->Increments('user_notenquire_post_changelog_id');
            $table->unsignedInteger('post_assign_trn_id')->index();
            $table->string('fieldname');
            $table->string('newvalue');
            $table->string('oldvalue');
            $table->string('done')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_notenquire_post_changelog');
    }
}
