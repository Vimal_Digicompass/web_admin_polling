<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Setting\CriteriaMasters;

class CriteriaMastersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CriteriaMasters::create([
            'criteria_name' => 'KYC Approval'
        ], [
            'criteria_name' => 'KYC Pending',
        ], [
            'criteria_name' => 'KYC Rejected',
        ], [
            'criteria_name' => 'Post Approval',
        ], [
            'criteria_name' => 'Post Rejected',
        ], [
            'criteria_name' => 'Recharge Failed',
        ], [
            'criteria_name' => 'Package Expiry',
        ], [
            'criteria_name' => 'Payment Pending',
        ], [
            'criteria_name' => 'Not Enquire Service',
        ], [
            'criteria_name' => 'Not Enquire Prop/Project',
        ], [
            'criteria_name' => 'Not Post',
        ], [
            'criteria_name' => 'Not Post Service Req',
        ], [
            'criteria_name' => 'Marked Favourite',
        ], [
            'criteria_name' => 'Inactive User',
        ], [
            'criteria_name' => 'Leads not viewed',
        ]);
    }
}
