<?php

namespace App\Http\Controllers\Useractivity;

use App\Exports\MarkedFavouriteExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Models\UserActivity\MarkedFavourite\MarkedFavouriteAssign;
use Carbon\Carbon;
use App\Models\Master\CallStatus;
use App\Models\LeadRequest;

use App\Models\SqFt\Project;
use App\Models\SqFt\Property;
use App\Models\SqFt\Service;
use App\Models\Master\State;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Master\City;
use Illuminate\Support\Facades\Auth;
use App\Models\Master\UserType;

use App\Models\Master\ServiceCategory;
use App\Models\Master\ProjectCategory;
use App\Models\Master\PropertyCategory;
use App\Models\SqFt\ServiceRequestLeads;
use App\Models\SqFt\ProjectRequestLeads;
use App\Models\SqFt\PropertyRequestLeads;
use Exception;
use App\User as CRMUser;
use App\Models\SqFt\User;
use Yajra\DataTables\DataTables;

class MarkedFavouriteController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $scenario = "Marked Favourite";
        $criteriaId = "";
        $content = "Users who marked favourites but did not make any enquiry (Property / Project / Services)";

        $role_id = Auth::user()->role_id;
        $criteriaId = Config::get('constants.CRITERION.MARKED_FAVOURITE');
        $commonFunctionData = commonFunctionData($role_id, $criteriaId);
        $state = $commonFunctionData['state'];
        $userType = $commonFunctionData['userType'];
        $callStatus = $commonFunctionData['callStatus'];
        $PostRejectedStatus = $commonFunctionData['PostRejectedStatus'];
        $KYCStatus = $commonFunctionData['KYCStatus'];
        $ServiceCategory = $commonFunctionData['ServiceCategory'];
        $PostType = $commonFunctionData['PostType'];
        $KYCRejectedStatus = $commonFunctionData['KYCRejectedStatus'];
        $KYCDocumentList = $commonFunctionData['KYCDocumentList'];
        $UserCategory = $commonFunctionData['UserCategory'];
        $ProjectCategory = $commonFunctionData['ProjectCategory'];
        $PropertyType = $commonFunctionData['PropertyType'];
        $PermissionDetail = $commonFunctionData['PermissionDetail'];
        $notificationData = $commonFunctionData['notificationData'];
        $notificationDataNew = $commonFunctionData['notificationDataNew'];
        $checkpermission = $commonFunctionData['checkpermission'];
        $propertycount = MarkedFavouriteAssign::where('post_type', 'P')
            ->where('crm_user_id', $id)
            ->where('updatestatus', '!=', 2)
            ->count();
        $propertywaitingcount = MarkedFavouriteAssign::where('post_type', 'P')
            ->where('crm_user_id', $id)
            ->where('updatestatus', '=', 0)
            ->count();
        $propertyprocessedcount = MarkedFavouriteAssign::where('post_type', 'P')
            ->where('crm_user_id', $id)
            ->where('updatestatus', '=', 1)
            ->count();
        $servicecount = MarkedFavouriteAssign::where('post_type', 'S')
            ->where('updatestatus', '!=', 2)
            ->where('crm_user_id', $id)
            ->count();
        $serviceprocessedcount = MarkedFavouriteAssign::where('post_type', 'S')
            ->where('updatestatus', '=', 1)
            ->where('crm_user_id', $id)
            ->count();
        $servicewaitingcount = MarkedFavouriteAssign::where('post_type', 'S')
            ->where('updatestatus', '=', 0)
            ->where('crm_user_id', $id)
            ->count();
        $projectcount = MarkedFavouriteAssign::where('post_type', 'PR')
            ->where('updatestatus', '!=', 2)
            ->where('crm_user_id', $id)
            ->count();
        $projectwaitingcount = MarkedFavouriteAssign::where('post_type', 'PR')
            ->where('updatestatus', '=', 0)
            ->where('crm_user_id', $id)
            ->count();
        $projectprocessedcount = MarkedFavouriteAssign::where('post_type', 'PR')
            ->where('updatestatus', '=', 1)
            ->where('crm_user_id', $id)
            ->count();
        if ($checkpermission->count() == 0) {
            return view('error.500');
        }
        return view('Scenario/Useractivity/markedfavourite', compact(
            'scenario',
            'servicecount',
            'propertycount',
            'projectcount',
            'projectwaitingcount',
            'propertywaitingcount',
            'servicewaitingcount',
            'projectprocessedcount',
            'propertyprocessedcount',
            'serviceprocessedcount',
            'content',
            'state',
            'id',
            'PostType',
            'UserCategory',
            'KYCDocumentList',
            'userType',
            'callStatus',
            'KYCStatus',
            'KYCRejectedStatus',
            'criteriaId',
            'PermissionDetail',
            'ServiceCategory',
            'ProjectCategory',
            'PropertyType',
            'notificationData',
            'notificationDataNew'
        ));
    }



    public function getmarkedfavouritelist(Request $request)
    {
        $id = $request->input('id');
        $roleData = CRMUser::where('user_id', $id)->first();
        $role_id = $roleData['role_id'];
        $PostType = $request->input('posttype');
        $getCustomerData = $this->getMarkedFavouriteData($request);
        // return $getCustomerData;
        return DataTables::of($getCustomerData)
            ->addColumn('action', function ($getCustomerData) use ($role_id) {
                $criteriaId = Config::get('constants.CRITERION.MARKED_FAVOURITE');
                $PermissionDetail = getPermissionAndAccess($criteriaId, $role_id);
                $is_kycupdate = $PermissionDetail[0]['is_kycupdate'];
                if ($is_kycupdate == 0) {
                    return '<td><button id=' . $getCustomerData->id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Update Status" class="tools updatestatus btn btn-danger btn-icon submit"> <i data-feather="edit"></i></button>
                    <button type="button" id=' . $getCustomerData->id . '  data-tooltip="tooltip" data-placement="top" title="Detail View" class="tools postdetailview btn btn-primary btn-icon submit"> <i data-feather="more-horizontal"></i></button></td>';
                } else {
                    return '<td><button id=' . $getCustomerData->id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Update Status" class="tools updatestatus btn btn-danger btn-icon submit"> <i data-feather="edit"></i></button>
                    <button id=' . $getCustomerData->id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Upload Document" class="tools uploaddocument btn btn-danger btn-icon submit"> <i data-feather="upload"></i></button>
                    <button type="button" id=' . $getCustomerData->id . '  data-tooltip="tooltip" data-placement="top" title="Detail View" class="tools postdetailview btn btn-primary btn-icon submit"> <i data-feather="more-horizontal"></i></button></td>';
                }
            })
            ->addColumn('totalnoofleads', function ($getCustomerData) use ($PostType) {
                if ($PostType == "PR") {
                    $getCount = LeadRequest::where('post_id', $getCustomerData->id)
                        ->where('post_type', 'PR')
                        ->count();
                    return $getCount;
                } else if ($PostType == "P") {
                    $getCount = LeadRequest::where('post_id', $getCustomerData->id)
                        ->where('post_type', 'P')
                        ->count();
                    return $getCount;
                } else {
                    $getCount = LeadRequest::where('post_id', $getCustomerData->id)
                        ->where('post_type', 'S')
                        ->count();
                    return $getCount;
                }
            })
            ->addColumn('name', function ($getCustomerData) {
                return $getCustomerData['user']->firstname . ' ' . $getCustomerData['user']->lastname;
            })->addColumn('mobilenumber', function ($getCustomerData) use ($role_id) {
                $criteriaId = Config::get('constants.CRITERION.MARKED_FAVOURITE');
                $PermissionDetail = getPermissionAndAccess($criteriaId, $role_id);
                $is_mobilenumberread = $PermissionDetail[0]['is_mobilenumberread'];
                if ($is_mobilenumberread == 0) {
                    return "-";
                } else {
                    return $getCustomerData['user']->mobile_number;
                }
            })
            ->addColumn('usertype', function ($getCustomerData) {
                $usertype = UserType::where('titlevalue', $getCustomerData['user']->user_type)->first();
                if ($usertype) {
                    return $usertype['title'];
                } else {
                    return "";
                }
            })->addColumn('city', function ($getCustomerData) {
                $city = City::where('id', $getCustomerData->city_id)->first();
                if ($city) {
                    return $city['title'];
                } else {
                    return "";
                }
            })->addColumn('state', function ($getCustomerData) {
                $state = City::where('id', $getCustomerData->city_id)->first();
                if ($state) {
                    $stateid = $state['state_id'];
                    $statetitle = State::where('id', $stateid)->first();
                    if ($statetitle) {
                        return $statetitle['title'];
                    } else {
                        return "";
                    }
                } else {
                    return "";
                }
            })
            ->addColumn('remarks', function ($getCustomerData)  use ($PostType) {
                $getRemarks = MarkedFavouriteAssign::select('remarks')
                    ->where('post_id', $getCustomerData->id)
                    ->where('post_type', $PostType)
                    ->first();
                if ($getRemarks) {
                    return $getRemarks['remarks'];
                } else {
                    return '';
                }
            })
            ->addColumn('crmusername', function ($getCustomerData) use($PostType) {
                $getcrmData = MarkedFavouriteAssign::select('crm_user_id')
                    ->where('post_id', $getCustomerData->id)
                    ->where('post_type', $PostType)
                    ->first();
                $crm_user_id =  $getcrmData['crm_user_id'];
                $crmData = CRMUser::where('user_id', $crm_user_id)->first();
                return $crmData['username'];
            })
            ->addColumn('followup_date', function ($getCustomerData) use ($PostType) {
                $getFollowdate = MarkedFavouriteAssign::select('followup_date')
                    ->where('post_id', $getCustomerData->id)
                    ->where('post_type', $PostType)
                    ->first();
                if ($getFollowdate['followup_date'] != NULL) {
                    return Carbon::createFromFormat('Y-m-d', $getFollowdate['followup_date'])
                        ->setTimezone('Asia/Calcutta')
                        ->format('d-m-Y');
                } else {
                    return '';
                }
            })->addColumn('status_id', function ($getCustomerData) use ($PostType) {
                $getStatus = MarkedFavouriteAssign::select('status')
                    ->where('post_id', $getCustomerData->id)
                    ->where('post_type', $PostType)
                    ->first();
                return $getStatus['status'];
            })->addColumn('status', function ($getCustomerData) use ($PostType) {
                $getStatus = MarkedFavouriteAssign::select('status')
                    ->where('post_id', $getCustomerData->id)
                    ->where('post_type', $PostType)
                    ->first();
                return getCallStatus($getStatus);
            })->addColumn('checkbox', function ($getCustomerData) {

                return "";
            })
            ->addColumn('category', function ($getCustomerData) use ($PostType) {
                if ($PostType == "PR") {
                    $category = ProjectCategory::where('id', $getCustomerData->category_id)->first();
                } else if ($PostType == "P") {
                    $category = PropertyCategory::where('id', $getCustomerData->property_category_id)->first();
                } else {
                    $category = ServiceCategory::where('id', $getCustomerData->service_type_id)->first();
                }
                if ($category) {
                    return $category['title'];
                } else {
                    return "";
                }
            })->rawColumns(['action', 'status', 'remarks', 'kycdocument', 'kycstatus'])
            ->make(true);
    }

    public function getMarkedFavouriteData($request)
    {

        $id = $request->input('id');
        $PostType = $request->input('posttype');
        $roleData = CRMUser::where('user_id', $id)->first();
        if ($roleData['role_type'] == Config::get('constants.ROLE_TYPE.TEAM_LEAD')) {
            $getTeamInfo = CRMUser::select('user_id')->where('team_lead_id', $id)->get();
            $user_id = $getTeamInfo->pluck('user_id');
        } else if ($roleData['role_type'] == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
            $getTeamInfo = CRMUser::select('user_id')->get();
            $user_id = $getTeamInfo->pluck('user_id');
        } else {
            $user_id = [$id];
        }
        $fromdate =  Carbon::parse($request->input('fromdate') . " 00:00:00");
        $todate =  Carbon::parse($request->input('todate') . " 23:59:59");
        $callstatus = $request->input('searchcallstatus');
        $city = $request->input('city');
        $queryPendingAssign = MarkedFavouriteAssign::query();
        $queryPendingAssign = $queryPendingAssign->where('updatestatus', '!=', 2);
        // if ($callstatus != 5) {
        //     $queryPendingAssign = $queryPendingAssign->where('status', $callstatus);
        // }

        if ($callstatus != Config::get('constants.CALL_STATUS.ALL')) {

            $queryPendingAssign = $queryPendingAssign->where('status', $callstatus);
        } else {
            if (
                Auth::user()->role_type != Config::get('constants.ROLE_TYPE.TEAM_LEAD') ||
                Auth::user()->role_type != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')
            ) {
                $queryPendingAssign = $queryPendingAssign->where('status', '!=', Config::get('constants.CALL_STATUS.CLOSED')); //closed
            }
        }

        if ($PostType == "PR") {
            $query = Project::query();
            $query = $query->whereBetween('posted_date', array($fromdate, $todate));
            $ProjectCategory = $request->input('projectcategory');
            if ($ProjectCategory != 0) {
                $query = $query->where('category_id', $ProjectCategory);
            }
        } else if ($PostType == "P") {
            $query = Property::query();
            $query = $query->whereBetween('created_at', array($fromdate, $todate));
            $PropertyCategory = $request->input('propertycategory');
            if ($PropertyCategory != "") {
                $query = $query->where('property_category_id', $PropertyCategory);
            }
            $PropertyType = $request->input('propertytype');
            if ($PropertyType != "") {
                $query = $query->where('property_type_id', $PropertyType);
            }
        } else if ($PostType == "S") {
            $query = Service::query();
            $query = $query->whereBetween('created_at', array($fromdate, $todate));
            $ServiceCategory = $request->input('servicecategory');
            if ($ServiceCategory != "") {
                $query = $query->where('service_type_id', $ServiceCategory);
            }
            $ServiceSubCategory = $request->input('servicesubcategory');
            if ($ServiceSubCategory != "") {
                $query = $query->where('sub_category_id', $ServiceSubCategory);
            }
        }

        if (
            $city == ""
        ) {
            $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id);
        } else {
            $query = $query->where('city_id', $city);
            $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id)->where('city_id', $city);
        }
        $queryPendingAssign = $queryPendingAssign
            ->where('post_type', $PostType)
            ->orderBy('created_at', 'DESC')
            ->get();
        $pluckPostId = $queryPendingAssign->pluck('post_id');
        $query = $query->whereIn('id', $pluckPostId);
        $query = $query->get();
        $dataArr = [];
        foreach ($query as $key => $value) {
            $id = $value['id'];
            $getUserId = MarkedFavouriteAssign::where('post_id', $id)
                ->where('post_type', $PostType)->first();
            $user_id = $getUserId['user_id'];
            // return $getUserId;
            $getUserdata = User::where('id', $user_id)->first();
            $value['user'] = $getUserdata;
            $getCount = LeadRequest::where('post_id', (int)$id)
            ->where('post_type', $PostType)
            ->where('request_user_id', (int)$user_id)
            ->where('is_post_active', true)
            ->count();
            if ($getCount == 0) {
                $value->hideout = 0;
            } else {
                $update = MarkedFavouriteAssign::where('post_type', $PostType)->where('post_id', $value['id'])
                ->where('user_id', $user_id)
                ->first();
                $updatestatus = $update['updatestatus'];
                if ($updatestatus == 0) {
                    $value->hideout = 1;
                    MarkedFavouriteAssign::where('post_type', $PostType)->where('post_id', $value['id'])->update([
                        'updatestatus' => 3
                    ]);
                } else {
                    if ($updatestatus == 3) {
                        $value->hideout = 1;
                    } else {
                        $value->hideout = 0;
                    }
                }
            }
            array_push($dataArr, $value);
        }
        return $dataArr;
    }


    public function markedfavouriteexport(Request $request)
    {
        unlinkfiles('public/reports/');
        $status = $request->input('status');
        $PostType = $request->input('posttype');
        if ($PostType == "PR") {
            $text = "MarkedFavourite_Project";
        } else if ($PostType == "P") {
            $text = "MarkedFavourite_Property";
        } else {
            $text = "MarkedFavourite_Service";
        }
        $fileNamewithDate = $text . '_' . date('d_m_Y') . '.xlsx';
        $url = 'public/reports/' . $fileNamewithDate;
        $urlresponse = "reports/" . $fileNamewithDate;
        $getCustomerData = $this->getMarkedFavouriteData($request);
        $getCustomerData = array_filter($getCustomerData, function ($a) {
            return $a->hideout !== 1;
        });
        if (count($getCustomerData) == 0) {
            return 0;
        } else {
            // return $getCustomerData;
            Excel::store(new MarkedFavouriteExport($getCustomerData, $request), '/' . $url, 'local');
            return '/' . $urlresponse;
        }
    }
}
