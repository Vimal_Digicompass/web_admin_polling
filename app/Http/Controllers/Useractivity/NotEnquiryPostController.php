<?php

namespace App\Http\Controllers\Useractivity;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use MongoDB\BSON\UTCDateTime;

use App\Exports\NEPExport;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Models\UserActivity\NotEnquirePost\NotEnquirePostAssign;
use App\Models\Master\CallStatus;
use App\Models\LeadRequest;
use App\Models\SqFt\Project;
use App\Models\Master\UserCategory;
use App\Models\Searches;
use App\Models\Master\State;
use App\Models\Master\City;
use Maatwebsite\Excel\Facades\Excel;
use Exception;
use App\Models\SqFt\Property;
use App\Models\SqFt\Service;
use Illuminate\Support\Facades\Auth;
use App\Models\Master\UserType;
use App\User as CRMUser;
use App\Models\SqFt\User;
use Yajra\DataTables\DataTables;

class NotEnquiryPostController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $scenario = "Not Enquiry Post";
        $content = "Users who did not enquire Properties / Projects/ Services (Who did not view and enquire Properties / Projects/ Services )";
        $role_id = Auth::user()->role_id;
        $criteriaId = Config::get('constants.CRITERION.NOT_ENQUIRE_POST');
        $commonFunctionData = commonFunctionData($role_id, $criteriaId);
        $state = $commonFunctionData['state'];
        $userType = $commonFunctionData['userType'];
        $callStatus = $commonFunctionData['callStatus'];
        $PostRejectedStatus = $commonFunctionData['PostRejectedStatus'];
        $KYCStatus = $commonFunctionData['KYCStatus'];
        $ServiceCategory = $commonFunctionData['ServiceCategory'];
        $PostType = $commonFunctionData['PostType'];
        $notificationData = $commonFunctionData['notificationData'];
        $notificationDataNew = $commonFunctionData['notificationDataNew'];
        $KYCRejectedStatus = $commonFunctionData['KYCRejectedStatus'];
        $KYCDocumentList = $commonFunctionData['KYCDocumentList'];
        $UserCategory = $commonFunctionData['UserCategory'];
        $ProjectCategory = $commonFunctionData['ProjectCategory'];
        $PropertyType = $commonFunctionData['PropertyType'];
        $PermissionDetail = $commonFunctionData['PermissionDetail'];
        $checkpermission = $commonFunctionData['checkpermission'];
        if ($checkpermission->count() == 0) {
            return view('error.500');
        }
        return view('Scenario/Useractivity/notenquirypost', compact(
            'scenario',
            'content',
            'ServiceCategory',
            'ProjectCategory',
            'PropertyType',
            'state',
            'UserCategory',
            'id',
            'KYCDocumentList',
            'userType',
            'PostType',
            'callStatus',
            'KYCStatus',
            'KYCRejectedStatus',
            'criteriaId',
            'PermissionDetail',
            'notificationData',
            'notificationDataNew'
        ));
    }

    public function getnotenquirepostlist(Request $request)
    {

        $id = $request->input('id');
        $roleData = CRMUser::where('user_id', $id)->first();
        $role_id = $roleData['role_id'];
        $getCustomerData = $this->getNotEnquirePostData($request);
        // return $getCustomerData;
        return DataTables::of($getCustomerData)
            ->addColumn('action', function ($getCustomerData) use ($role_id) {
                $criteriaId = Config::get('constants.CRITERION.NOT_ENQUIRE_POST');
                $PermissionDetail = getPermissionAndAccess($criteriaId, $role_id);
                $is_kycupdate = $PermissionDetail[0]['is_kycupdate'];
                if ($is_kycupdate == 0) {
                    return '<td><button id=' . $getCustomerData->trn_id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Update Status" class="tools updatestatus btn btn-danger btn-icon submit"> <i data-feather="edit"></i></button>
                    <button type="button" id=' . $getCustomerData->trn_id . '  data-tooltip="tooltip" data-placement="top" title="Detail View" class="tools nepdetailview btn btn-primary btn-icon submit"> <i data-feather="more-horizontal"></i></button></td>';
                } else {
                    return '<td><button id=' . $getCustomerData->trn_id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Update Status" class="tools updatestatus btn btn-danger btn-icon submit"> <i data-feather="edit"></i></button>
                    <button id=' . $getCustomerData->trn_id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Upload Document" class="tools uploaddocument btn btn-danger btn-icon submit"> <i data-feather="upload"></i></button>
                    <button type="button" id=' . $getCustomerData->trn_id . '  data-tooltip="tooltip" data-placement="top" title="Detail View" class="tools nepdetailview btn btn-primary btn-icon submit"> <i data-feather="more-horizontal"></i></button></td>';
                }
            })

            ->addColumn('name', function ($getCustomerData) {
                return $getCustomerData->firstname . ' ' . $getCustomerData->lastname;
            })->addColumn('usertype', function ($getCustomerData) {
                $usertype = UserType::where('titlevalue', $getCustomerData->user_type)->first();
                if ($usertype) {
                    return $usertype['title'];
                } else {
                    return "";
                }
            })->addColumn('city', function ($getCustomerData) {
                $city = City::where('id', $getCustomerData->city_id)->first();
                if ($city) {
                    return $city['title'];
                } else {
                    return "";
                }
            })->addColumn('state', function ($getCustomerData) {
                $state = City::where('id', $getCustomerData->city_id)->first();
                if ($state) {
                    $stateid = $state['state_id'];
                    $statetitle = State::where('id', $stateid)->first();
                    if ($statetitle) {
                        return $statetitle['title'];
                    } else {
                        return "";
                    }
                } else {
                    return "";
                }
            })
            ->addColumn('remarks', function ($getCustomerData) {
                $getRemarks = NotEnquirePostAssign::select('remarks')
                    ->where('post_assign_trn_id', $getCustomerData->trn_id)
                    ->first();
                if ($getRemarks) {
                    return $getRemarks['remarks'];
                } else {
                    return '';
                }
            })->addColumn('crmusername', function ($getCustomerData) {

                $getcrmData = NotEnquirePostAssign::select('crm_user_id')
                    ->where('post_assign_trn_id', $getCustomerData->trn_id)
                    ->first();
                $crm_user_id =  $getcrmData['crm_user_id'];
                $crmData = CRMUser::where('user_id', $crm_user_id)->first();
                return $crmData['username'];
            })
            ->addColumn('followup_date', function ($getCustomerData) {
                $getFollowdate = NotEnquirePostAssign::select('followup_date')
                    ->where('post_assign_trn_id', $getCustomerData->trn_id)
                    ->first();
                if ($getFollowdate['followup_date'] != NULL) {
                    return Carbon::createFromFormat('Y-m-d', $getFollowdate['followup_date'])
                        ->setTimezone('Asia/Calcutta')
                        ->format('d-m-Y');
                } else {
                    return '';
                }
            })->addColumn('mobile_number', function ($getCustomerData)  use ($role_id) {
                $criteriaId = Config::get('constants.CRITERION.NOT_ENQUIRE_POST');
                $PermissionDetail = getPermissionAndAccess($criteriaId, $role_id);
                $is_mobilenumberread = $PermissionDetail[0]['is_mobilenumberread'];
                if ($is_mobilenumberread == 0) {
                    return "-";
                } else {
                    return $getCustomerData->mobile_number;
                }
            })->addColumn('serviceposted', function ($getCustomerData) {
                return Service::where('user_id', $getCustomerData->id)->count();
            })->addColumn('propertyposted', function ($getCustomerData) {
                return Property::where('user_id', $getCustomerData->id)->count();
            })->addColumn('projectposted', function ($getCustomerData) {
                return Project::where('user_id', $getCustomerData->id)->count();
            })->addColumn('servicerequirementposted', function ($getCustomerData) {
                return DB::connection('mysql_1')->table('service_post_requirements')
                    ->where('user_id', $getCustomerData->id)
                    ->count();
            })->addColumn('checkbox', function ($getCustomerData) {
                return "";
            })->addColumn('status_id', function ($getCustomerData) {
                $getStatus = NotEnquirePostAssign::select('status')
                    ->where('post_assign_trn_id', $getCustomerData->trn_id)
                    ->first();
                return $getStatus['status'];
            })->addColumn('status', function ($getCustomerData) {
                $getStatus = NotEnquirePostAssign::select('status')
                    ->where('post_assign_trn_id', $getCustomerData->trn_id)
                    ->first();
                return getCallStatus($getStatus);
            })
            ->addColumn('category', function ($getCustomerData) {
                $category = UserCategory::where('id', $getCustomerData->category_id)->first();
                if ($category) {
                    return $category['title'];
                } else {
                    return "";
                }
            })->editColumn('dateofregistration', function ($getCustomerData) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $getCustomerData->created_at)
                    ->setTimezone('Asia/Calcutta')
                    ->format('d-m-Y');
            })->editColumn('last_login_date', function ($getCustomerData) {
                return Carbon::parse($getCustomerData->last_login_date)
                    ->setTimezone('Asia/Calcutta')
                    ->format('d-m-Y');
            })
            ->rawColumns(['action', 'active', 'remarks', 'status'])
            ->make(true);
    }

    public function getNotEnquirePostData($request)
    {

        $id = $request->input('id');
        $roleData = CRMUser::where('user_id', $id)->first();
        if ($roleData['role_type'] == Config::get('constants.ROLE_TYPE.TEAM_LEAD')) {
            $getTeamInfo = CRMUser::select('user_id')->where('team_lead_id', $id)->get();
            $user_id = $getTeamInfo->pluck('user_id');
        } else if ($roleData['role_type'] == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
            $getTeamInfo = CRMUser::select('user_id')->get();
            $user_id = $getTeamInfo->pluck('user_id');
        } else {
            $user_id = [$id];
        }

        $callstatus = $request->input('searchcallstatus');
        $usertype = $request->input('usertype');


        $city = $request->input('city');

        $queryPendingAssign = NotEnquirePostAssign::query();
        if ($callstatus != Config::get('constants.CALL_STATUS.ALL')) {

            $queryPendingAssign = $queryPendingAssign->where('status', $callstatus);
        } else {
            if (
                Auth::user()->role_type != Config::get('constants.ROLE_TYPE.TEAM_LEAD') ||
                Auth::user()->role_type != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')
            ) {
                $queryPendingAssign = $queryPendingAssign->where('status', '!=', Config::get('constants.CALL_STATUS.CLOSED')); //closed
            }
        }
        if ($city == "") {
            $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id);
        } else {
            $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id)->where('city_id', $city);
        }
        $queryPendingAssign = $queryPendingAssign->where('updatestatus', '!=', 2);
        $queryPendingAssign = $queryPendingAssign->orderBy('created_at', 'DESC')->get();
        $Arr = [];
        // echo count($queryPendingAssign);
        foreach ($queryPendingAssign as $key => $value) {
            $query = User::query();
            if ($usertype == "") {
                $getusertype = UserType::pluck('titlevalue');
                $query  =  $query->whereIn('user_type', $getusertype);
            } else {
                $query  =  $query->where('user_type', $usertype);
            }
            if ($city == "") {
                $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id);
            } else {
                $query =  $query->where('city_id', $city);
                $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id)->where('city_id', $city);
            }
            $user_id = $value['user_id'];
            $trn_id = $value['post_assign_trn_id'];
            // echo $trn_id.'<r>';
            $query =  $query->where('id', $user_id)->first();
            $searchedText = [];
            if ($query) {
                $getcount = Searches::where('user_id', $user_id)->orderBy('id', 'DESC')->get();
                if (count($getcount) == 0) {
                    $lastsearchdate  = $value['created_at'];
                } else {
                    $lastsearchdate =  $getcount[0]['search_date'];
                    if (count($getcount) < 5) {
                        for ($i = 0; $i < count($getcount); $i++) {
                            if ($getcount[$i]['post_type'] == "P") {
                                array_push($searchedText, 'Property');
                            } else if ($getcount[$i]['post_type'] == "PR") {
                                array_push($searchedText, 'Project');
                            } else {
                                array_push($searchedText, 'Service');
                            }
                        }
                    } else {
                        for ($i = 0; $i < 5; $i++) {
                            if ($getcount[$i]['post_type'] == "P") {
                                array_push($searchedText, 'Property');
                            } else if ($getcount[$i]['post_type'] == "PR") {
                                array_push($searchedText, 'Project');
                            } else {
                                array_push($searchedText, 'Service');
                            }
                        }
                    }
                }
                if (count($searchedText) == 0) {
                    $text = "";
                } else {
                    $result = array_unique($searchedText);
                    $text = implode(',', $result);
                }
                $getCount = LeadRequest::where('request_user_id', $user_id)
                    ->where('request_date', '>=', new UTCDateTime(Carbon::now()->subHours((15 * 24))))
                    ->count();
                if ($getCount == 0) {
                    $query->hideout = 0;
                } else {
                    $update = NotEnquirePostAssign::where('user_id', $user_id)
                        ->where('post_assign_trn_id',$trn_id)
                        ->first();
                    $updatestatus = $update['updatestatus'];
                    if ($updatestatus == 0) {
                        $query->hideout = 1;
                        NotEnquirePostAssign::where('user_id', $user_id)
                        ->where('post_assign_trn_id', $trn_id)->update([
                            'updatestatus' => 3
                        ]);
                    } else {
                        if ($updatestatus == 3) {
                            $query->hideout = 1;
                        } else {
                            $query->hideout = 0;
                        }
                    }
                }
                $query->searchedText = $text;
                $query->last_search_date = $lastsearchdate;
                $query->trn_id = $trn_id;
                array_push($Arr, $query);
            }
        }
        $Arr = collect($Arr);
        return $Arr;
    }
    public function nepexport(Request $request)
    {
        unlinkfiles('public/reports/');
        $status = $request->input('searchcallstatus');
        $getCallStatus = CallStatus::where('id', $status)->first();
        $strReplace = str_replace(" ", "_", $getCallStatus['title']);
        $fileNamewithDate = $strReplace . '_' . date('d_m_Y') . '.xlsx';
        $url = 'public/reports/' . $fileNamewithDate;
        $urlresponse = "reports/" . $fileNamewithDate;
        $getCustomerData = $this->getNotEnquirePostData($request);
        $getCustomerData = array_filter($getCustomerData->toArray(), function ($a) {
            return $a['hideout'] !== 1;
        });
        if (count($getCustomerData) == 0) {
            return 0;
        } else {
            $text = "Not Enquire Post";
            Excel::store(new NEPExport($getCustomerData, $request, $text), '/' . $url, 'local');
            return '/' . $urlresponse;
        }
    }
}
