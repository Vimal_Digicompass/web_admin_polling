<?php

namespace App\Http\Controllers\Useractivity;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserLogCommoner;
use App\Models\UserLogBroker;
use App\Models\UserLogBuilder;
use App\Models\UserLogServiceProvider;
use App\Models\Post;
use App\Models\LeadRequest;
use App\Models\SqFt\ProjectRequestLeads;
use App\Models\SqFt\PropertyRequestLeads;
use App\Exports\NotPostExport;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Models\UserActivity\NotPost\NotPostAssign;
use App\Models\Master\CallStatus;

use App\Models\SqFt\Project;
use App\Models\Master\UserCategory;
use App\Models\Master\State;
use App\Models\Master\City;
use Maatwebsite\Excel\Facades\Excel;
use Exception;
use MongoDB\BSON\UTCDateTime;

use App\Models\SqFt\Property;
use App\Models\SqFt\Service;
use Illuminate\Support\Facades\Auth;
use App\Models\Master\UserType;
use App\User as CRMUser;
use App\Models\SqFt\User;
use Yajra\DataTables\DataTables;

class NotPostController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $scenario = "Not Post";
        $content = "Users who did not post (List of users who did not post Property / Project / PG / Service) - User logged in or accessed the app but did not post";

        $role_id = Auth::user()->role_id;

        $criteriaId = Config::get('constants.CRITERION.NOT_POST');
        $commonFunctionData = commonFunctionData($role_id, $criteriaId);
        $state = $commonFunctionData['state'];
        $userType = $commonFunctionData['userType'];
        $callStatus = $commonFunctionData['callStatus'];
        $PostRejectedStatus = $commonFunctionData['PostRejectedStatus'];
        $KYCStatus = $commonFunctionData['KYCStatus'];
        $ServiceCategory = $commonFunctionData['ServiceCategory'];
        $PostType = $commonFunctionData['PostType'];
        $KYCRejectedStatus = $commonFunctionData['KYCRejectedStatus'];
        $KYCDocumentList = $commonFunctionData['KYCDocumentList'];
        $UserCategory = $commonFunctionData['UserCategory'];
        $ProjectCategory = $commonFunctionData['ProjectCategory'];
        $PropertyType = $commonFunctionData['PropertyType'];
        $PermissionDetail = $commonFunctionData['PermissionDetail'];
        $checkpermission = $commonFunctionData['checkpermission'];
        $notificationData = $commonFunctionData['notificationData'];
        $notificationDataNew = $commonFunctionData['notificationDataNew'];
        if ($checkpermission->count() == 0) {
            return view('error.500');
        }
        return view('Scenario/Useractivity/notpost', compact(
            'scenario',
            'content',
            'ServiceCategory',
            'ProjectCategory',
            'PropertyType',
            'state',
            'UserCategory',
            'id',
            'KYCDocumentList',
            'userType',
            'PostType',
            'callStatus',
            'KYCStatus',
            'KYCRejectedStatus',
            'criteriaId',
            'PermissionDetail',
            'notificationData',
            'notificationDataNew'
        ));
    }

    public function getnotpostuserlist(Request $request)
    {

        $id = $request->input('id');
        $roleData = CRMUser::where('user_id', $id)->first();
        $role_id = $roleData['role_id'];
        $getCustomerData = $this->getNotPostData($request);
        // return $getCustomerData;
        return DataTables::of($getCustomerData)
            ->addColumn('kycstatus', function ($getCustomerData) {
                $is_kyc_updated = $getCustomerData->is_kyc_updated;
                $is_kyc_approved = $getCustomerData->is_kyc_approved;
                if ($is_kyc_updated == 1 && $is_kyc_approved == 0) {

                    return '<p style="margin-top: 30px;margin-left: -78px;font-size:14px;" class="badge badge-info">Waiting</p>';
                } else if ($is_kyc_updated == 1 && $is_kyc_approved == 1) {
                    return '<p style="margin-top: 30px;margin-left: -78px;font-size:14px;" class="badge badge-success">Approved</p>';
                } else if ($is_kyc_updated == 1 && $is_kyc_approved == 2) {
                    return '<p style="margin-top: 30px;margin-left: -78px;font-size:14px;" class="badge badge-danger">Rejected</p>';
                } else if ($is_kyc_updated == 0 && $is_kyc_approved == 1) {
                    return '<p style="margin-top: 30px;margin-left: -78px;font-size:14px;" class="badge badge-warning">Doc without Approved</p>';
                } else if ($is_kyc_updated == 0 && $is_kyc_approved == 0) {

                    return '<p style="margin-top: 30px;margin-left: -78px;font-size:14px;" class="badge badge-primary">Document Pending</p>';
                } else {
                    return "";
                }
            })
            ->addColumn('action', function ($getCustomerData) use ($role_id) {
                $criteriaId = Config::get('constants.CRITERION.NOT_POST');
                $PermissionDetail = getPermissionAndAccess($criteriaId, $role_id);
                $is_kycupdate = $PermissionDetail[0]['is_kycupdate'];
                if ($is_kycupdate == 0) {
                    return '<td><button id=' . $getCustomerData->trn_id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Update Status" class="tools updatestatus btn btn-danger btn-icon submit"> <i data-feather="edit"></i></button>
                    <button type="button" id=' . $getCustomerData->trn_id . '  data-tooltip="tooltip" data-placement="top" title="Detail View" class="tools notpostdetailview btn btn-primary btn-icon submit"> <i data-feather="more-horizontal"></i></button></td>';
                } else {
                    return '<td><button id=' . $getCustomerData->trn_id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Update Status" class="tools updatestatus btn btn-danger btn-icon submit"> <i data-feather="edit"></i></button>
                    <button id=' . $getCustomerData->trn_id . ' type="button" data-tooltip="tooltip" data-placement="top" title="Upload Document" class="tools uploaddocument btn btn-danger btn-icon submit"> <i data-feather="upload"></i></button>
                    <button type="button" id=' . $getCustomerData->trn_id . '  data-tooltip="tooltip" data-placement="top" title="Detail View" class="tools notpostdetailview btn btn-primary btn-icon submit"> <i data-feather="more-horizontal"></i></button></td>';
                }
            })

            ->addColumn('name', function ($getCustomerData) {
                return $getCustomerData->firstname . ' ' . $getCustomerData->lastname;
            })->addColumn('usertype', function ($getCustomerData) {
                $usertype = UserType::where('titlevalue', $getCustomerData->user_type)->first();
                if ($usertype) {
                    return $usertype['title'];
                } else {
                    return "";
                }
            })->addColumn('city', function ($getCustomerData) {
                $city = City::where('id', $getCustomerData->city_id)->first();
                if ($city) {
                    return $city['title'];
                } else {
                    return "";
                }
            })->addColumn('state', function ($getCustomerData) {
                $state = City::where('id', $getCustomerData->city_id)->first();
                if ($state) {
                    $stateid = $state['state_id'];
                    $statetitle = State::where('id', $stateid)->first();
                    if ($statetitle) {
                        return $statetitle['title'];
                    } else {
                        return "";
                    }
                } else {
                    return "";
                }
            })->addColumn('crmusername', function ($getCustomerData) {

                $getcrmData = NotPostAssign::select('crm_user_id')
                    ->where('user_notpost_assign_trn_id', $getCustomerData->trn_id)
                    ->first();
                $crm_user_id =  $getcrmData['crm_user_id'];
                $crmData = CRMUser::where('user_id', $crm_user_id)->first();
                return $crmData['username'];
            })
            ->addColumn('remarks', function ($getCustomerData) {
                $getRemarks = NotPostAssign::select('remarks')
                    ->where('user_notpost_assign_trn_id', $getCustomerData->trn_id)
                    ->first();
                if ($getRemarks) {
                    return $getRemarks['remarks'];
                } else {
                    return '';
                }
            })
            ->addColumn('followup_date', function ($getCustomerData) {
                $getFollowdate = NotPostAssign::select('followup_date')
                    ->where('user_notpost_assign_trn_id', $getCustomerData->trn_id)
                    ->first();
                if ($getFollowdate['followup_date'] != NULL) {
                    return Carbon::createFromFormat('Y-m-d', $getFollowdate['followup_date'])
                        ->setTimezone('Asia/Calcutta')
                        ->format('d-m-Y');
                } else {
                    return '';
                }
            })->addColumn('mobile_number', function ($getCustomerData)  use ($role_id) {
                $criteriaId = Config::get('constants.CRITERION.NOT_POST');
                $PermissionDetail = getPermissionAndAccess($criteriaId, $role_id);
                $is_mobilenumberread = $PermissionDetail[0]['is_mobilenumberread'];
                if ($is_mobilenumberread == 0) {
                    return "-";
                } else {
                    return $getCustomerData->mobile_number;
                }
            })->addColumn('checkbox', function ($getCustomerData) {
                return "";
            })->addColumn('status_id', function ($getCustomerData) {
                $getStatus = NotPostAssign::select('status')
                    ->where('user_notpost_assign_trn_id', $getCustomerData->trn_id)
                    ->first();
                return $getStatus['status'];
            })->addColumn('status', function ($getCustomerData) {
                $getStatus = NotPostAssign::select('status')
                    ->where('user_notpost_assign_trn_id', $getCustomerData->trn_id)
                    ->first();
                return getCallStatus($getStatus);
            })->addColumn('serviceposted', function ($getCustomerData) {
                return Service::where('user_id', $getCustomerData->id)->count();
            })->addColumn('propertyposted', function ($getCustomerData) {
                return Property::where('user_id', $getCustomerData->id)->count();
            })->addColumn('projectposted', function ($getCustomerData) {
                return Project::where('user_id', $getCustomerData->id)->count();
            })->addColumn('totalnoofleadsproject', function ($getCustomerData) {

                $getCount = LeadRequest::where('post_type', 'PR')
                    ->where('post_id', $getCustomerData->id)
                    ->count();
                return $getCount;
            })->addColumn('totalnoofleadsproperty', function ($getCustomerData) {
                $getCount = LeadRequest::where('post_type', 'P')
                    ->where('post_id', $getCustomerData->id)
                    ->count();
                return $getCount;
            })->addColumn('totalnoofleadsservice', function ($getCustomerData) {
                $getCount = LeadRequest::where('post_type', 'S')
                    ->where('post_id', $getCustomerData->id)
                    ->count();
                return $getCount;
            })->addColumn('totalnoofviewedleadsproject', function ($getCustomerData) {

                $getCount = LeadRequest::where('post_type', 'PR')
                    ->where('post_id', $getCustomerData->id)
                    ->where('is_viewed', true)
                    ->count();
                return $getCount;
            })->addColumn('totalnoofviewedleadsproperty', function ($getCustomerData) {
                $getCount = LeadRequest::where('post_type', 'P')
                    ->where('post_id', $getCustomerData->id)
                    ->where('is_viewed', true)
                    ->count();
                return $getCount;
            })->addColumn('totalnoofviewedleadsservice', function ($getCustomerData) {
                $getCount = LeadRequest::where('post_type', 'S')
                    ->where('post_id', $getCustomerData->id)
                    ->where('is_viewed', true)
                    ->count();
                return $getCount;
            })->addColumn('servicerequirementposted', function ($getCustomerData) {
                return DB::connection('mysql_1')->table('service_post_requirements')
                    ->where('user_id', $getCustomerData->id)
                    ->count();
            })
            ->addColumn('category', function ($getCustomerData) {
                $category = UserCategory::where('id', $getCustomerData->category_id)->first();
                if ($category) {
                    return $category['title'];
                } else {
                    return "";
                }
            })->editColumn('dateofregistration', function ($getCustomerData) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $getCustomerData->created_at)
                    ->setTimezone('Asia/Calcutta')
                    ->format('d-m-Y');
            })->editColumn('last_login_date', function ($getCustomerData) {
                return Carbon::parse($getCustomerData->last_login_date)
                    ->setTimezone('Asia/Calcutta')
                    ->format('d-m-Y');
            })
            ->rawColumns(['action', 'active', 'status', 'remarks', 'kycstatus'])
            ->make(true);
    }

    public function getNotPostData($request)
    {

        $id = $request->input('id');
        $roleData = CRMUser::where('user_id', $id)->first();
        if ($roleData['role_type'] == Config::get('constants.ROLE_TYPE.TEAM_LEAD')) {
            $getTeamInfo = CRMUser::select('user_id')->where('team_lead_id', $id)->get();
            $user_id = $getTeamInfo->pluck('user_id');
        } else if ($roleData['role_type'] == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
            $getTeamInfo = CRMUser::select('user_id')->get();
            $user_id = $getTeamInfo->pluck('user_id');
        } else {
            $user_id = [$id];
        }


        $callstatus = $request->input('searchcallstatus');
        $usertype = $request->input('usertype');


        $city = $request->input('city');

        $queryPendingAssign = NotPostAssign::query();
        if ($callstatus != Config::get('constants.CALL_STATUS.ALL')) {

            $queryPendingAssign = $queryPendingAssign->where('status', $callstatus);
        } else {
            if (
                Auth::user()->role_type != Config::get('constants.ROLE_TYPE.TEAM_LEAD') ||
                Auth::user()->role_type != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')
            ) {
                $queryPendingAssign = $queryPendingAssign->where('status', '!=', Config::get('constants.CALL_STATUS.CLOSED')); //closed
            }
        }
        if ($city == "") {
            $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id);
        } else {
            $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id)->where('city_id', $city);
        }
        $queryPendingAssign = $queryPendingAssign->where('updatestatus', '!=', 2);
        $queryPendingAssign = $queryPendingAssign->orderBy('created_at', 'DESC')->get();
        $Arr = [];
        $fromdate =  Carbon::parse($request->input('fromdate') . " 00:00:00");
        $todate =  Carbon::parse($request->input('todate') . " 23:59:59");
        // echo count($queryPendingAssign);
        foreach ($queryPendingAssign as $key => $value) {
            $query = User::query();
            if ($usertype == "") {
                $getusertype = UserType::pluck('titlevalue');
                $query  =  $query->whereIn('user_type', $getusertype);
            } else {
                $query  =  $query->where('user_type', $usertype);
            }
            if ($city == "") {
                $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id);
            } else {
                $query =  $query->where('city_id', $city);
                $queryPendingAssign = $queryPendingAssign->whereIn('crm_user_id', $user_id)->where('city_id', $city);
            }
            $user_id = $value['user_id'];
            $trn_id = $value['user_notpost_assign_trn_id'];
            // echo $trn_id.'<r>';
            $query =  $query->where('id', $user_id)->first();
            if ($query) {
                $query->trn_id = $trn_id;
                $getCount = Post::where('user_id', $user_id)
                    ->where('posted_date', '>=', new UTCDateTime(Carbon::now()->subHours((16 * 24))))
                    ->count();
                if ($getCount == 0) {
                    $query->hideout = 0;
                } else {
                    $update = NotPostAssign::where('user_id', $user_id)
                        ->where('user_notpost_assign_trn_id', $trn_id)
                        ->first();
                    $updatestatus = $update['updatestatus'];
                    if ($updatestatus == 0) {
                        $query->hideout = 1;
                        NotPostAssign::where('user_id', $user_id)
                            ->where('user_notpost_assign_trn_id', $trn_id)->update([
                                'updatestatus' => 3
                            ]);
                    } else {
                        if ($updatestatus == 3) {
                            $query->hideout = 1;
                        } else {
                            $query->hideout = 0;
                        }
                    }
                }
                $user_type = $query['user_type'];
                if ($user_type == "C") {
                    $matchingUsers = UserLogCommoner::whereBetween('created_at', [$fromdate, $todate])
                        ->where('page_name_slug', Config::get('pageNameConstants.ADD_NEW_POST.PROPERTY'))
                        ->where('user_id', $query['id'])
                        ->get();
                    if (count($matchingUsers) != 0) {
                        array_push($Arr, $query);
                    }
                } elseif ($user_type == "CT") {
                    $matchingUsers = UserLogServiceProvider::whereBetween('created_at', [$fromdate, $todate])
                        ->where('user_id', $query['id'])
                        ->get();
                    if (count($matchingUsers) != 0) {
                        array_push($Arr, $query);
                    }
                } elseif ($user_type == "B") {
                    $matchingUsers = UserLogBuilder::whereBetween('created_at', [$fromdate, $todate])
                        ->where('user_id', $query['id'])
                        ->get();
                    if (count($matchingUsers) != 0) {
                        array_push($Arr, $query);
                    }
                } elseif ($user_type == "BR") {
                    $matchingUsers = UserLogBroker::whereBetween('created_at', [$fromdate, $todate])
                        ->where('user_id', $query['id'])
                        ->get();
                    if (count($matchingUsers) != 0) {
                        array_push($Arr, $query);
                    }
                }
            }
        }
        $Arr = collect($Arr);
        return $Arr;
    }
    public function notpostexport(Request $request)
    {
        unlinkfiles('public/reports/');
        $status = $request->input('searchcallstatus');
        $getCallStatus = CallStatus::where('id', $status)->first();
        $strReplace = str_replace(" ", "_", $getCallStatus['title']);
        $fileNamewithDate = $strReplace . '_' . date('d_m_Y') . '.xlsx';
        $url = 'public/reports/' . $fileNamewithDate;
        $urlresponse = "reports/" . $fileNamewithDate;
        $getCustomerData = $this->getNotPostData($request);
        $getCustomerData = array_filter($getCustomerData->toArray(), function ($a) {
            return $a['hideout'] !== 1;
        });
        if (count($getCustomerData) == 0) {
            return 0;
        } else {
            $text = "NotPost User";
            Excel::store(new NotPostExport($getCustomerData, $request, $text), '/' . $url, 'local');
            return '/' . $urlresponse;
        }
    }
}
