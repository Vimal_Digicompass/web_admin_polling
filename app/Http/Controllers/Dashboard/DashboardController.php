<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Setting\NotificationMaster;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Config;


class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $scenario = "Dashboard";
        $user = Auth::user();
        $user_id = $user->user_id;
        $id = $user_id;
        $role_id = $user->role_id;
        $notificationData = NotificationMaster::where('crm_user_id', $user_id)->orderBy('notification_id', 'DESC')
            ->get();
        $notificationDataNew = NotificationMaster::where('is_read', 0)
            ->where('crm_user_id', $user_id)->orderBy('notification_id', 'DESC')
            ->count();
        // return $notificationData;
        $PermissionDetail = getPermissionAndAccess(0, $role_id);
        $userinfo = User::with('roles')->where('user_id', $user_id)->first();
        $criteriaId = "";

        $content = "Dashboard - Summary of the all the transactions by the CRM user";
        return view('dashboard', compact(
            'scenario',
            'content',
            'id',
            'criteriaId',
            'PermissionDetail',
            'notificationData',
            'notificationDataNew'
        ));
    }

    public function home()
    {
        return view('sample');
    }

    public function getdashboarddata(Request $request)
    {
        $user = Auth::user();
        $user_id = $user->user_id;
        $role_id = $user->role_id;
        $fromdate =  Carbon::parse($request->input('fromdate') . " 00:00:00");
        $todate =  Carbon::parse($request->input('todate') . " 23:59:59");
        $PermissionDetail = getPermissionAndAccess(0, $role_id);
        $userinfo = User::with('roles')->where('user_id', $user_id)->first();
        if (

            $userinfo['role_type'] ==  Config::get('constants.ROLE_TYPE.CRM_USER') ||
            $userinfo['role_type'] == Config::get('constants.ROLE_TYPE.SUB_ADMIN')
        ) {
            $dashboardInfo = getUserDahboardData(
                1,
                [$user_id],
                $PermissionDetail,
                $fromdate,
                $todate
            );
        } else if (
            $userinfo['role_type'] == Config::get('constants.ROLE_TYPE.TEAM_LEAD')
        ) {
            $dashboardInfo = [];
            $selectUser = User::where('team_lead_id', $user_id)->get();
            if ($userinfo['role_type'] == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                $selectUser = User::all();
            }

            foreach ($selectUser as $key => $value) {
                $role_id = $value['role_id'];
                $PermissionDetails = getPermissionAndAccess(0, $role_id);
                $Arr = getUserDahboardData(
                    1,
                    [$value['user_id']],
                    $PermissionDetails,
                    $fromdate,
                    $todate
                );
                $value['permissionDetail'] = $Arr;
                array_push($dashboardInfo, $value);
            }
        } else {
            $dashboardInfo = [];
            $selectUser = User::where('role_type', Config::get('constants.ROLE_TYPE.TEAM_LEAD'))->get();
            foreach ($selectUser as $key => $value) {
                $PermissionDetails = getPermissionAndAccess(0, $value['role_id']);
                $getCRMUserID = User::where('team_lead_id', $value['user_id'])->get();
                $userData = [];
                foreach ($getCRMUserID as $key => $val) {

                    $Arr = getUserDahboardData(
                        1,
                        [$val['user_id']],
                        $PermissionDetails,
                        $fromdate,
                        $todate
                    );
                    $val['permissionDetail'] = $Arr;
                    array_push($userData, $val);
                }
                $value['UserInfo'] = $userData;
                array_push($dashboardInfo, $value);
            }
        }
        $returnHTML = view('Components.dashboardview', compact(
            'dashboardInfo',
            'userinfo',
        ))->render();
        return $returnHTML;
    }
}
