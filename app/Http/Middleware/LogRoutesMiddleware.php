<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LogRoutesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        try {
            $responseData = "";
            if ($response instanceof \Illuminate\Http\JsonResponse)
                $responseData = $response->getData();
            Log::notice("Logging request for " . $response->status() . " response on route " . $request->getPathInfo() . "\n", [
                "request" => $request->all(),
                "response" => $responseData,
            ]);
        } catch (\Throwable $th) {
            Log::info($th->getMessage(), $th->getTrace());
        }

        return $response;
    }
}
