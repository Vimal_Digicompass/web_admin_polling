<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Log;

use Closure;
use Illuminate\Support\Facades\Config;

class BlockIpMiddleware
{

    // set IP addresses
    public function __construct()
    {
        $this->blockIps = Config::get('constants.BlockIPAddress');
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        if (count($this->blockIps) != 0) {
            if (!in_array($request->ip(), $this->blockIps)) {
                Log::notice('illegal Ip address');
                Log::notice($request->ip());
                return view('error.404')->with(['ip' => $request->ip()]);
            }
        }
        return $next($request);
    }
}
