<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Jenssegers\Mongodb\Eloquent\Model;
use App\Models\LeadRequest;
use App\Models\Searches;
use App\Models\UserLogCommoner;
use App\Models\UserLogBroker;
use App\Models\UserLogBuilder;
use App\Models\UserLogServiceProvider;


class User extends Model
{

    protected $connection = 'mongodb';

    protected $collection = 'users';

    public $timestamps = true;

    public function getleadrequest(): HasMany
    {
        return $this->hasMany(LeadRequest::class, 'user_id', 'post_user_id');
    }

    public function getsearches(): HasMany
    {
        return $this->hasMany(Searches::class, 'user_id', 'user_id');
    }

    public function commonerLogs(): HasMany
    {
        return $this->hasMany(UserLogCommoner::class, 'user_id', 'user_id');
    }

    public function commonerFirstLog(): HasOne
    {
        return $this->hasOne(UserLogCommoner::class, 'user_id', 'user_id');
    }

    public function builderLogs(): HasMany
    {
        return $this->hasMany(UserLogBuilder::class, 'user_id', 'user_id');
    }

    public function builderFirstLog(): HasOne
    {
        return $this->hasOne(UserLogBuilder::class, 'user_id', 'user_id');
    }

    public function brokerLogs(): HasMany
    {
        return $this->hasMany(UserLogBroker::class, 'user_id', 'user_id');
    }

    public function brokerFirstLog(): HasOne
    {
        return $this->hasOne(UserLogBroker::class, 'user_id', 'user_id');
    }

    public function serviceProviderLogs(): HasMany
    {
        return $this->hasMany(UserLogServiceProvider::class, 'user_id', 'user_id');
    }

    public function serviceProviderFirstLog(): HasOne
    {
        return $this->hasOne(UserLogServiceProvider::class, 'user_id', 'user_id');
    }
}
