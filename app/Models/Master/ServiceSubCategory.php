<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceSubCategory extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'service_subcategories_master';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'is_active', 'service_category_id' ,'sortorder', 'title'];
}
