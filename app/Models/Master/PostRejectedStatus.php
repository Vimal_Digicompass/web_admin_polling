<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostRejectedStatus extends Model
{
    use HasFactory;

    protected $connection = 'mysql_1';

    protected $table = 'rejection_reasons';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = ['id', 'reason','title','type'];
}
