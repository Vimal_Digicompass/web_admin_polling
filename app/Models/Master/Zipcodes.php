<?php

namespace App\Models\Master;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Zipcodes extends Model
{
    use HasFactory;

    protected $connection = 'mysql_1';

    protected $table = 'area_zips';

    public $timestamps = false;

    protected $fillable = [
        'area_id',
        'zip_code',
        'residential_property_lead_cost',
        'commercial_property_lead_cost',
        'agricultural_property_lead_cost',
        'farm_house_property_lead_cost',
        'paying_guest_property_lead_cost',
        'flat_mate_property_lead_cost',
        'residential_project_lead_cost',
        'commercial_project_lead_cost',
        'service_lead_cost',
        'latitude',
        'longitude'
    ];
}
