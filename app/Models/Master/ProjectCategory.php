<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectCategory extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'product_categories_master';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'sortorder', 'title'];
}
