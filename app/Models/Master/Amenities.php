<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Amenities extends Model
{
    use HasFactory;
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'amenities_master';
    public $timestamps = false;

    protected $fillable =  [
        'id', 'title', 'is_residential',
        'is_commercial', 'sortorder', 'is_active'
    ];
}
