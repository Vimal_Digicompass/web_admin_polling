<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CallStatus extends Model
{
    use HasFactory;
    
    protected $connection = 'mysql';

    protected $table = 'callstatus';

    protected $primaryKey = 'id';

    protected $fillable = ['id','title'];
}
