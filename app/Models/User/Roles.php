<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Roles extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';
    protected $dates = ['deleted_at'];
    protected $guarded = ['role_id'];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    public function roletypes()
    {
        return $this->hasMany('App\Models\User\RoleTypes', 'role_type_id', 'role_type_id');
    }
}
