<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectAmenities extends Model
{

    protected $table = 'project_amenities';
    public $timestamps = false;

    protected $connection = 'mysql_1';
    protected $fillable = [
        'project_id',
        'amenity_id',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];


}
