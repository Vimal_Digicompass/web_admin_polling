<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CriteriaMasters extends Model
{
    use HasFactory, softDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $guarded = ['criteria_id '];

    protected $fillable = [
            'criteria_id'];

    protected $table = 'criteria_masters';

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];
}
