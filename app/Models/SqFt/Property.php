<?php

namespace App\Models\SqFt;

use Illuminate\Support\Facades\Config;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\libraries\AWSElasticSearch;

use App\Models\Property\Property as PropertyProperty;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\SqFt\PropertyImages;
use App\Models\SqFt\PropertyTenant;
use App\Models\SqFt\PropertyIdealForBusiness;


class Property extends Model
{


    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'properties';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'user_id', 'parent_id', 'title', 'builder_name', 'property_type_id',
        'property_category_id', 'property_sub_category_id', 'bhk_id', 'furnish_type_id',
        'floor_id', 'bathroom_id', 'balcony_id', 'width_of_road', 'construction_type_id',
        'available_from', 'monthly_rent', 'transaction_type_id', 'maintenance_charges',
        'security_deposit', 'builtup_area', 'carpet_area', 'no_open_side_id', 'price',
        'negotiable', 'expected_duration_of_stay', 'expected_duration_of_month',
        'expected_duration_of_year', 'property_age', 'corner_plot', 'area_unit',
        'plot_area', 'token_advance', 'price_negotiable', 'boundary_wall_made',
        'personal_washroom', 'cafeteria_area_id', 'address', 'city_id', 'pincode',
        'description', 'sortorder', 'main_image_name', 'PG_or_hostel_name',
        'PG_type', 'PG_preferred_tenant', 'room_type', 'attach_bathroom',
        'food_facility', 'parking', 'room_amenities', 'gate_closing_time',
        'PG_rules', 'common_amenities', 'is_property_approved',
        'is_send_for_approve_request', 'reject_message', 'crm_remark',
        'crm_outbound_call_status', 'is_crm_inbound', 'crm_inbound_call_status',
        'is_published', 'property_status', 'property_status_id', 'latitude',
        'longitude', 'assigned_to', 'created_at', 'updated_at', 'published_date',
        'approved_by', 'area_id', 'property_visible_for', 'influencer_video_link',
        'influencer_property_looking_for', 'influencer_lead_criteria',
        'influencer_commission_on_lead', 'influencer_lead_state_id',
        'influencer_lead_city_id', 'influencer_lead_age_group',
        'influencer_lead_salaried_business', 'influencer_lead_annual_income_range',
        'influencer_commission_on_sale', 'influencer_sale_criteria',
        'influencer_sale_state_id', 'influencer_sale_city_id',
        'influencer_sale_age_group', 'influencer_sale_salaried_business',
        'influencer_sale_annual_income_range', 'influencer_sale_token_advance',
        'flatmates_property_type', 'flatmates_tenant_type', 'flatmates_room_type',
        'flatmates_non_veg_allowed', 'flatmates_gated_security', 'flatmates_floor',
        'flatmates_preferred_tenant_type', 'possession_date',
        'is_pending', 'is_property_expire'
    ];
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function getimages(): HasMany
    {
        return $this->hasMany(PropertyImages::class, 'property_id', 'id');
    }
    public function gettenant(): HasMany
    {
        return $this->hasMany(PropertyTenant::class, 'property_id', 'id');
    }

    public function getideal(): HasMany
    {
        return $this->hasMany(PropertyIdealForBusiness::class, 'property_id', 'id');
    }

    public function PGroomType()
    {
        $PGroomType = Config::get('constants.property-container.PG-Room-Type');
        return isset($PGroomType[$this->room_type]) ? $PGroomType[$this->room_type] : '';
    }

    public function getPropertyApproveStatusAttribute()
    {
        $status = Config::get('constants.property-container.property-approve-status');
        return isset($status[$this->is_property_approved]) ? $status[$this->is_property_approved] : '';
    }
    public function PropertyVisibleFor()
    {
        $propertyVisibleForArray = Config::get('constants.property-container.property-visible-for');
        return isset($propertyVisibleForArray[$this->property_visible_for]) ? $propertyVisibleForArray[$this->property_visible_for] : '';
    }

    public function getCafeteriaAreaAttribute()
    {
        $cafeteria = Config::get('constants.property-container.pantry-cafeteria');
        return isset($cafeteria[$this->cafeteria_area_id]) ? $cafeteria[$this->cafeteria_area_id] : '';
    }

    protected static function booted()
    {


        static::updated(function ($property) {
            $property->refreshESIndex();
        });
    }


    private function refreshESIndex()
    {
        $AWSElasticSearch = new AWSElasticSearch();
        $propertytElastic = PropertyProperty::where('id', $this->id)->first();
        //Check property index already exists
        $isExists = $AWSElasticSearch->checkIndexExists($propertytElastic);
        // echo $isExists;
        // exit;
        if ($isExists) {
            $AWSElasticSearch->deleteIndexDoc($propertytElastic, 'P');
        }
        // if ($propertytElastic->is_property_approved == 1 && $propertytElastic->is_published == 1) {
            $AWSElasticSearch->createIndex($propertytElastic);
        // }

        unset($AWSElasticSearch);
    }
}
