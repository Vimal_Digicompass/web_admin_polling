<?php

namespace App\Models\SqFt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectFlats extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'project_flats';

    protected $primaryKey = 'id';
    
    public $timestamps = false;

    protected $fillable = [
        'id', 'parent_id', 'project_id', 'title', 'bhk_id', 'builtup_area',
        'furnish_type_id', 'floor_id', 'carpet_area', 'price', 'transaction_type_id',
        'flat_age', 'floor_plan_file_name', 'loan_offered_bank_ids',
        'is_published', 'flat_status', 'possession_date', 'created_at'
    ];
}
