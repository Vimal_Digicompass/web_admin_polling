<?php

namespace App\Models\SqFt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\SqFt\ServiceImages;
use App\Models\SqFt\ServiceArea;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\libraries\AWSElasticSearch;

use App\Models\Service\Service as ServiceService;

class ServicePostRequirement extends Model
{
    use HasFactory;
    protected $connection = 'mysql_1';

    protected $table = 'service_post_requirements';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'user_id', 'sub_category_id', 'payment_mode_id',
        'min_price', 'max_price', 'city_id', 'pincode', 'is_active', 'created_at', 'area_id'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
