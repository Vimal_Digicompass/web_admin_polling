<?php

namespace App\Models\SqFt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyImages extends Model
{
    use HasFactory;

    protected $connection = 'mysql_1';

    protected $table = 'property_images';

    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'id', 'property_id', 'image_name'
    ];
}
