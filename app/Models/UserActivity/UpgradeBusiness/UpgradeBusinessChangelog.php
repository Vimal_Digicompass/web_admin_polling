<?php
namespace App\Models\UserActivity\UpgradeBusiness;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class UpgradeBusinessChangelog extends Model
{
    use HasFactory,softDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'upgradebusiness_changelog_id';

    protected $table = 'upgradebusiness_changelog';

    public $timestamps = true;

}
