<?php

namespace App\Models\UserActivity\UpgradeBusiness;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class UpgradeBusinessArcheive extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'upgradebusiness_arch_id';

    protected $fillable = ['city_id', 'user_id', 'status'];

    protected $table = 'upgradebusiness_archeive';
}
