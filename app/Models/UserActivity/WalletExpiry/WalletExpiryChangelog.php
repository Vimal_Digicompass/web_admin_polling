<?php

namespace App\Models\UserActivity\WalletExpiry;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletExpiryChangelog extends Model
{
    use HasFactory;
    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_cpexp_changelog_id';

    protected $fillable = ['cpexp_trn_id', 'fieldname', 'oldvalue', 'newvalue', 'done'];

    protected $table = 'user_cpexp_changelog';
}
