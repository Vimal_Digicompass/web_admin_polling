<?php

namespace App\Models\UserActivity\WalletExpiry;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletExpiryAssign extends Model
{
    use HasFactory;
    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_cpexp_assign_id';

    protected $fillable = ['cpexp_trn_id', 'crm_user_id', 'city_id','package_id' ,'user_id','status', 'followup_date', 'updatestatus', 'remarks'];

    protected $table = 'user_cpexp_assign';
}
