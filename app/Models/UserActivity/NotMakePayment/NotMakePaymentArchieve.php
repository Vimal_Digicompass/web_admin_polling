<?php

namespace App\Models\UserActivity\NotMakePayment;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotMakePaymentArchieve extends Model
{

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_makepayment_arch_id';

    protected $fillable = ['city_id', 'user_id' ,'status', 'makepayment_trn_id'];

    protected $table = 'user_makepayment_archieve';
}
