<?php

namespace App\Models\UserActivity\NotMakePayment;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotMakePaymentPoll extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $primaryKey  = 'user_makepayment_poll_id';

    protected $fillable = ['city_id', 'user_id' , 'makepayment_trn_id'];

    protected $table = 'user_makepayment_poll';
}
