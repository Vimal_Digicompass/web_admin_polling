<?php

namespace App\Models\UserActivity\NotPost;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotPostAssign extends Model
{
    use HasFactory;
    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_notpost_assign_id';

    protected $fillable = ['user_notpost_assign_trn_id', 'crm_user_id', 'city_id', 'user_id', 'status', 'followup_date', 'updatestatus', 'remarks'];

    protected $table = 'user_notpost_assign';
}
