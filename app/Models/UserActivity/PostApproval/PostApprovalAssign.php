<?php
namespace App\Models\UserActivity\PostApproval;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PostApprovalAssign extends Model
{
    use HasFactory;
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'crm_user_id', 'user_id', 'post_id',
        'city_id', 'post_type', 'published_status',
        'updatestatus', 'followup_date'
    ];

    protected $primaryKey  = 'post_approve_assign_id';

    protected $table = 'postapproval_assign';
}
