<?php
namespace App\Models\UserActivity\PostApproval;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostApprovalChangelog extends Model
{
    use HasFactory, softDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'post_approve_changelog_id';

    protected $table = 'postapproval_changelog';
}
