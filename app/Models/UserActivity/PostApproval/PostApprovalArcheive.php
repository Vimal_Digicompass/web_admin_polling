<?php
namespace App\Models\UserActivity\PostApproval;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PostApprovalArcheive extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'post_approve_arch_id';

    protected $fillable = ['city_id', 'post_id', 'user_id', 'post_type', 'status'];

    protected $table = 'postapproval_archieve';
}
