<?php

namespace App\Models\UserActivity\KYCRejected;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class KYCRejectedAssign extends Model
{
    use HasFactory;
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $fillable = ['crm_user_id', 'user_id', 'city_id', 'followup_date'];

    protected $primaryKey  = 'kyc_rej_assign_id';

    protected $table = 'kycrejected_assign';
}
