<?php

namespace App\Models\UserActivity\KYCRejected;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KYCRejectedPoll extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $primaryKey  = 'kyc_rej_poll_id';

    protected $fillable = ['city_id', 'user_id'];

    protected $table = 'kycrejected_poll';
}
