<?php
namespace App\Models\UserActivity\KYCPending;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class KYCPendingChangelog extends Model
{
    use HasFactory,softDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'kyc_pen_changelog_id';

    protected $table = 'kycpending_changelog';

}
