<?php

namespace App\Models\UserActivity\NotPostServiceRequirement;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotPostServiceRequirementAssign extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_notpost_req_assign_id';

    protected $fillable = ['user_notpost_req_assign_trn_id', 'crm_user_id', 'city_id','user_id','status','followup_date', 'updatestatus', 'remarks'];

    protected $table = 'user_notpost_req_assign';
}
