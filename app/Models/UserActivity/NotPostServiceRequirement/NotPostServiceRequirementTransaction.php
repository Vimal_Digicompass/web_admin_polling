<?php

namespace App\Models\UserActivity\NotPostServiceRequirement;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotPostServiceRequirementTransaction extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_notpost_req_assign_trn_id';

    protected $fillable = ['city_id', 'user_id'];

    protected $table = 'user_notpost_req_assign_trn';
}
