<?php
namespace App\Models\UserActivity\KYCApproval;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class KYCApprovalChangelog extends Model
{
    use HasFactory,softDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'kyc_approval_changelog_id';

    protected $table = 'kycapproval_changelog';

    public $timestamps = true;

}
