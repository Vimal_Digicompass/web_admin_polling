<?php

namespace App\Models\UserActivity\PostRejected;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostRejectedPoll extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $primaryKey  = 'post_rej_poll_id';

    protected $fillable = ['city_id', 'user_id', 'post_id', 'post_type'];

    protected $table = 'postrejected_poll';
}
