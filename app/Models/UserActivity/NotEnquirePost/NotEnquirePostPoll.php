<?php

namespace App\Models\UserActivity\NotEnquirePost;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotEnquirePostPoll extends Model
{
    use HasFactory;

    protected $connection = 'mysql';
    protected $primaryKey  = 'user_notenquire_post_poll_id';

    protected $fillable = ['city_id', 'user_id', 'user_notenquire_post_assign_trn_id'];

    protected $table = 'user_notenquire_post_poll';
}
