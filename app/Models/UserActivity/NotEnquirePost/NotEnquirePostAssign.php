<?php

namespace App\Models\UserActivity\NotEnquirePost;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotEnquirePostAssign extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'mysql';

    protected $dates = ['deleted_at'];

    protected $primaryKey  = 'user_notenquire_post_assign_id';

    protected $fillable = ['post_assign_trn_id', 'crm_user_id', 'status','city_id','user_id' ,'followup_date', 'updatestatus', 'remarks'];

    protected $table = 'user_notenquire_post_assign';
}
