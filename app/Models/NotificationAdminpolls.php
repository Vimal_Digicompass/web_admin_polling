<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Jenssegers\Mongodb\Eloquent\Model;
use App\Models\User;

class NotificationAdminpolls extends Model
{

    protected $connection = 'mongodb';

    protected $collection = 'notification_adminpolls';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'topicARN',
        'content',
        'date',
        'status',
        'isvalidToken',
        'criteria',
        'MessageId'
    ];
}
