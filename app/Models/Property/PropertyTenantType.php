<?php

namespace App\Models\Property;


use Illuminate\Database\Eloquent\Model;


class PropertyTenantType extends Model
{
    protected $table = 'property_tenant_type';
    protected $connection = 'mysql_1';
	public $timestamps = false;
    protected $fillable = [

        'property_id',
        'tenant_type_id'
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

  
}



