<?php

namespace App\Models\Property;


use Illuminate\Database\Eloquent\Model;


class PropertyType extends Model
{
    protected $table = 'property_type_master';
    public $timestamps = false;
    protected $connection = 'mysql_1';

    protected $fillable = [
        'title',
        'sortorder',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

}
