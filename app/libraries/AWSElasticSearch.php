<?php

//$client = ClientBuilder::create()
//->setHosts(["http://search-onesqft-vrs4myi5r32blyxovf3estc4la.ap-south-1.es.amazonaws.com:80"])
//->build();
//$client = ClientBuilder::create()->build();
namespace App\libraries;
use Exception;
use Elasticsearch\ClientBuilder;

class AWSElasticSearch
{
    public function createClient()
    {
        try{
            $client = ClientBuilder::create()
                ->setHosts([env('ELASTICSEARCH_ENDPOINT').':'.env('ELASTICSEARCH_PORT')])
                ->build();
            return $client;
        }catch(Exception $e){
             return $e->getMessage();
        }

    }

    public function getIndices($type)
    {
        $client = $this->createClient();

        $indexParams['index']  = env('ElasticSearchPropertyIndex');
        $exists = $client->indices()->exists($indexParams);


        $indexParams = [];
        $EStype = '';

        if ($type == 'P') {
            $indexParams['index']  = env('ElasticSearchPropertyIndex');
            $EStype  = env('ElasticSearchPropertyType');
            $exists = $client->indices()->exists($indexParams);
        } elseif ($type == 'S') {
            $indexParams['index']  = env('ElasticSearchServiceIndex');
            $EStype  = env('ElasticSearchServiceType');
            $exists = $client->indices()->exists($indexParams);
        } elseif ($type == 'PR') {
            $indexParams['index']  = env('ElasticSearchProjectIndex');
            $EStype  = env('ElasticSearchProjectType');
            $exists = $client->indices()->exists($indexParams);
        }


        if ($exists) {
            $isApprovedTerm = ["term" => ['IsApproved' => 1]];
            $isPublishedTerm = ["term" => ['IsPublished' => 1]];

            //Define All searchable fields here
            $mustArray = [
                'must' => [
                    $isApprovedTerm,
                    $isPublishedTerm,
                ]
            ];

            $newArray = $mustArray['must'];
            $mustFilteredArray = array_filter($newArray);
            $mustFilteredArray = array_values($mustFilteredArray);

            //Generate
            $params = [
                'index' => $indexParams['index'],
                'type' => $EStype,
                'body' => [
                    //"from"=> 20,
                    //"size"=> 10,
                    'query' => [
                        'bool' => [
                            'filter' => [
                                'bool' => [
                                    'must' => [
                                        $mustFilteredArray
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];


            $response = $client->search($params);
            return $response;
        }
    }

    public function checkIndexExists($object)
    {
        $client = $this->createClient();

        //Object like property
        $params = $object->getElasticSearchGetIndexParametres(); //Get from trait

        $response = $client->exists($params);
        return $response;
    }

    public function checkInfluencerIndexExists($object)
    {
        $client = $this->createClient();

        //Object like property
        $params = $object->getESGetInfluencerIndexParametres(); //Get from trait

        $response = $client->exists($params);
        return $response;
    }

    public function getIndex($object)
    {
        if ($object->property_visible_for == 1)
            $isExists = $this->checkInfluencerIndexExists($object);
        else
            $isExists = $this->checkIndexExists($object);

        if ($isExists) {
            $client = $this->createClient();

            //Object like property
            $params = $object->getElasticSearchGetIndexParametres(); //Get from trait

            $response = $client->get($params);
            return $response;
        }
    }

    public function createIndex($object)
    {
        $client = $this->createClient();

        //Object like property
        $params = $object->getElasticSearchCreateIndexParametres(); //Get from trait

        $index = $client->create($params);
        return $index;
    }

    public function updateIndex($object)
    {
        $client = $this->createClient();

        $this->deletePropertyIndexDoc($object);

        //Object like property
        $params = $object->getElasticSearchCreateIndexParametres(); //Get from trait

        $index = $client->create($params);
        return $index;
    }

    public function updateObjectApproveStatusIndex($object)
    {
        $client = $this->createClient();

        //Object like property
        $params = $object->getElasticSearchUpdateApproveStatusParametres(); //Get from trait

        $index = $client->update($params);
        return $index;
    }

    public function updateApproveStatusIndexByUser($object, $approveStatus)
    {
        $client = $this->createClient();

        //Object like property
        $params = $object->getESUpdateApproveByStatus($approveStatus); //Get from trait

        $index = $client->update($params);
        return $index;
    }

    public function updateObjectPublishStatusIndex($object)
    {
        $client = $this->createClient();

        //Object like property
        $params = $object->getElasticSearchUpdatePublishStatusParametres(); //Get from trait

        $index = $client->update($params);
        return $index;
    }

    public function deleteWholeIndex($type)
    {
        $client = $this->createClient();

        if ($type == 'P') {
            $deleteParams = ['index' => env('ElasticSearchPropertyIndex')];
        } elseif ($type == 'S') {
            $deleteParams = ['index' => env('ElasticSearchServiceIndex')];
        } elseif ($type == 'PR') {
            $deleteParams = ['index' => env('ElasticSearchProjectIndex')];
        }

        $response = $client->indices()->delete($deleteParams);
        return $response;
    }

    public function deleteIndexDoc($object, $type, $visibleFor = 0)
    {
        $client = $this->createClient();

        if ($type == 'P') {
            if ($visibleFor == 0) {
                $deleteParams = ['index' => env('ElasticSearchPropertyIndex'), 'type' => env('ElasticSearchPropertyType'), 'id' => $object->id];
            } elseif ($visibleFor == 1) {
                $deleteParams = ['index' => env('ESInfluencerPropertyIndex'), 'type' => env('ElasticSearchPropertyType'), 'id' => $object->id];
            } elseif ($visibleFor == 2) {
                $deleteParams = ['index' => env('ElasticSearchPropertyIndex'), 'type' => env('ElasticSearchPropertyType'), 'id' => $object->id];
                $response = $client->delete($deleteParams);

                $deleteParams = ['index' => env('ESInfluencerPropertyIndex'), 'type' => env('ElasticSearchPropertyType'), 'id' => $object->id];
                $response = $client->delete($deleteParams);
                return $response;
            }
        } elseif ($type == 'S') {
            $deleteParams = ['index' => env('ElasticSearchServiceIndex'), 'type' => env('ElasticSearchServiceType'), 'id' => $object->id];
        } elseif ($type == 'PR') {
            if ($visibleFor == 0) {
                $deleteParams = ['index' => env('ElasticSearchProjectIndex'), 'type' => env('ElasticSearchProjectType'), 'id' => $object->id];
            } elseif ($visibleFor == 1) {
                $deleteParams = ['index' => env('ESInfluencerProjectIndex'), 'type' => env('ESInfluencerProjectType'), 'id' => $object->id];
            } elseif ($visibleFor == 2) {
                $deleteParams = ['index' => env('ElasticSearchProjectIndex'), 'type' => env('ElasticSearchProjectType'), 'id' => $object->id];
                $response = $client->delete($deleteParams);

                $deleteParams = ['index' => env('ESInfluencerProjectIndex'), 'type' => env('ESInfluencerProjectType'), 'id' => $object->id];
                $response = $client->delete($deleteParams);
                return $response;
            }
        }

        $response = $client->delete($deleteParams);
        return $response;
    }

    public function GetSearchMust($paramArray, $key, $must)
    {
        $returnMust = '';
        if (array_key_exists($key, $paramArray)) {
            if ($paramArray[$key] != '') {
                if ($must == 'prefix') {
                    $value = strtolower($paramArray[$key]);
                    $returnMust = ['match_phrase_prefix' => [$key => $value]];
                } elseif ($must == 'term') {
                    $value = $paramArray[$key];
                    $returnMust = [$must => [$key => $value]];
                } elseif ($must == 'terms') {
                    if (is_array($paramArray[$key]))
                        $valueArray = array_map('trim', $paramArray[$key]);
                    else
                        $valueArray = [$paramArray[$key]];

                    $returnMust = [$must => [$key . '.keyword' => $valueArray]];
                }
            }
        }

        return $returnMust;
    }

    public function GetRangeSearchMust($paramArray, $from, $to, $fieldName)
    {
        $priceRange = '';

        if ($fieldName == 'PropertyAge') {
            if (isset($paramArray[$from])  || isset($paramArray[$to])) {
                $priceRange =  [
                    'range' => [
                        $fieldName => [
                            'from' => $paramArray[$from],
                            'to' => $paramArray[$to],
                        ]
                    ]
                ];
            }
        }

        if (array_key_exists($from, $paramArray) || array_key_exists($to, $paramArray)) {
            if ($paramArray[$from] > 0 || $paramArray[$to] > 0) {
                $priceRange =  [
                    'range' => [
                        $fieldName => [
                            'from' => $paramArray[$from],
                            'to' => $paramArray[$to],
                        ]
                    ]
                ];
            }
        }

        return $priceRange;
    }

    public function GetRangeFromFields($paramArray, $from, $field)
    {
        $priceRange = '';
        if (array_key_exists($from, $paramArray)) {
            if ($paramArray[$from] > 0 || $paramArray[$from] == 0) {
                $priceRange =  [
                    'range' => [
                        $field => [
                            "from" => $paramArray[$from]
                        ],
                    ],
                ];
            }
        }

        return $priceRange;
    }

    public function GetRangeToFields($paramArray, $to, $field)
    {

        $priceRange = '';
        if (array_key_exists($to, $paramArray)) {
            if ($paramArray[$to] > 0) {
                $priceRange =  [
                    'range' => [
                        $field => [
                            "to" => $paramArray[$to]
                        ],
                    ],
                ];
            }
        }

        return $priceRange;
    }

    public function GetDateRangeSearchMust($paramArray, $fromDate, $fieldName)
    {
        $dateRange = '';
        if (array_key_exists($fromDate, $paramArray)) {
            if ($paramArray[$fromDate] != '') {
                $dateRange =  [
                    'range' => [
                        $fieldName => [
                            "gte" => $paramArray[$fromDate]
                        ]
                    ]
                ];
            }
        }

        return $dateRange;
    }


    public function searchPropertyIndex($paramArray)
    {
        $client = $this->createClient();

        $indexParams['index']  = env('ElasticSearchPropertyIndex');
        $exists = $client->indices()->exists($indexParams);
        if ($exists) {
            $sortByTypeTitle = $paramArray['SortByTypeTitle'];
            $sortByOrder = $paramArray['SortByOrder'];

            $indexNO = 0;
            if (array_key_exists('indexNO', $paramArray)) {
                if ($paramArray['indexNO'])
                    $indexNO = $paramArray['indexNO'];
            }

            $noOfObjects = 10;
            if (array_key_exists('noOfObjects', $paramArray)) {
                if ($paramArray['noOfObjects'])
                    $noOfObjects = $paramArray['noOfObjects'];
            }


            //Prefix search
            $searchPrefixOption = "prefix";
            $TypePrefix = $this->GetSearchMust($paramArray, 'Type', $searchPrefixOption);
            $CityPrefix = $this->GetSearchMust($paramArray, 'City', $searchPrefixOption);
            $CategoryPrefix = $this->GetSearchMust($paramArray, 'Category', $searchPrefixOption);

            //MultiMatch
            $MultiMatch = '';
            if (array_key_exists('Location', $paramArray)) {
                if ($paramArray['Location'] != '') {
                    $locationValue = strtolower($paramArray['Location']);
                    $MultiMatch = [
                        "multi_match" => [
                            'operator' => "or",
                            "type" => "most_fields",
                            "query" => $locationValue,
                            "lenient" => true,
                            'fields' => [
                                'Title',
                                'Address',
                                'Description',
                                'Pincode'
                            ],
                        ]
                    ];
                }
            }

            //Array Search
            $searchTermsOption = "terms";
            $BuilderNameTerm = $this->GetSearchMust($paramArray, 'BuilderName', $searchTermsOption);
            $SubCategoryTerm = $this->GetSearchMust($paramArray, 'SubCategory', $searchTermsOption);
            $BHKTerm = $this->GetSearchMust($paramArray, 'Bhk', $searchTermsOption);
            $BathroomTerm = $this->GetSearchMust($paramArray, 'Bathroom', $searchTermsOption);
            $BalconyTerm = $this->GetSearchMust($paramArray, 'Balcony', $searchTermsOption);
            $TenantTypesTerm = $this->GetSearchMust($paramArray, 'TenantTypes', $searchTermsOption);
            $IdealForBusinessTerm = $this->GetSearchMust($paramArray, 'IdealForBusiness', $searchTermsOption);
            $PersonalWashroomTerm = $this->GetSearchMust($paramArray, 'PersonalWashroom', $searchTermsOption);
            $CafeteriaAreaTerm = $this->GetSearchMust($paramArray, 'CafeteriaArea', $searchTermsOption);
            $FurnishTypeTerm = $this->GetSearchMust($paramArray, 'FurnishType', $searchTermsOption);
            $FloorTerm = $this->GetSearchMust($paramArray, 'Floor', $searchTermsOption);
            $PriceNegotiableTerm = $this->GetSearchMust($paramArray, 'PriceNegotiable', $searchTermsOption);
            $PGTypeTerm = $this->GetSearchMust($paramArray, 'PGType', $searchTermsOption);
            $PreferredTenantTerm = $this->GetSearchMust($paramArray, 'PreferredTenant', $searchTermsOption);
            $PGRoomTypeTerm = $this->GetSearchMust($paramArray, 'PGRoomType', $searchTermsOption);
            $AttachBathroomTerm = $this->GetSearchMust($paramArray, 'AttachBathroom', $searchTermsOption);
            $FoodFacilityTerm = $this->GetSearchMust($paramArray, 'FoodFacility', $searchTermsOption);
            $ParkingTerm = $this->GetSearchMust($paramArray, 'Parking', $searchTermsOption);
            $RoomAmenitiesTerm = $this->GetSearchMust($paramArray, 'RoomAmenities', $searchTermsOption);
            $PGRulesTerm = $this->GetSearchMust($paramArray, 'PGRules', $searchTermsOption);
            $CommonAmenitiesTerm = $this->GetSearchMust($paramArray, 'CommonAmenities', $searchTermsOption);
            $TransactionTypeTerm = $this->GetSearchMust($paramArray, 'TransactionType', $searchTermsOption);
            $NoOfOpenSiteTerm = $this->GetSearchMust($paramArray, 'NoOfOpenSite', $searchTermsOption);
            $AreaTerm = $this->GetSearchMust($paramArray, 'Area', $searchTermsOption);

            $FlatmatePropertyTypes = $this->GetSearchMust($paramArray, 'FlatmatePropertyTypes', $searchTermsOption);
            $FlatmateTenantType = $this->GetSearchMust($paramArray, 'FlatmateTenantType', $searchTermsOption);
            $FlatmateRoomType = $this->GetSearchMust($paramArray, 'FlatmateRoomType', $searchTermsOption);
            $FlatmateNonVegAllowed = $this->GetSearchMust($paramArray, 'FlatmateNonVegAllowed', $searchTermsOption);
            $FlatmateGatedSecurity = $this->GetSearchMust($paramArray, 'FlatmateGatedSecurity', $searchTermsOption);
            $FlatmatesPreferredTenantType = $this->GetSearchMust($paramArray, 'FlatmatesPreferredTenantType', $searchTermsOption);

            //Term Search
            $searchTermOption = "term";
            $IsApprovedTerm = $this->GetSearchMust($paramArray, 'IsApproved', $searchTermOption);
            $IsPublishedTerm = $this->GetSearchMust($paramArray, 'IsPublished', $searchTermOption);
            $ExpectedDurationOfMonthTerm = $this->GetSearchMust($paramArray, 'ExpectedDurationOfMonth', $searchTermOption);
            $ExpectedDurationOfYearTerm = $this->GetSearchMust($paramArray, 'ExpectedDurationOfYear', $searchTermOption);

            //Rang Search
            $PriceRange = $this->GetRangeSearchMust($paramArray, 'PriceFrom', 'PriceTo', 'Price');
            $MonthlyRentRange = $this->GetRangeSearchMust($paramArray, 'MonthlyRentFrom', 'MonthlyRentTo', 'MonthlyRent');
            $MaintenanceChargesRange = $this->GetRangeSearchMust($paramArray, 'MaintenanceChargesFrom', 'MaintenanceChargesTo', 'MaintenanceCharges');
            $SecurityDepositRange = $this->GetRangeSearchMust($paramArray, 'SecurityDepositFrom', 'SecurityDepositTo', 'SecurityDeposit');
            $BuiltupAreaRange = $this->GetRangeSearchMust($paramArray, 'BuiltupAreaFrom', 'BuiltupAreaTo', 'BuiltupArea');
            $CarpetAreaRange = $this->GetRangeSearchMust($paramArray, 'CarpetAreaFrom', 'CarpetAreaTo', 'CarpetArea');
            $TokenAdvanceRange = $this->GetRangeSearchMust($paramArray, 'TokenAdvanceFrom', 'TokenAdvanceTo', 'TokenAdvance');
            $PropertyAgeRange = $this->GetRangeSearchMust($paramArray, 'PropertyAgeFrom', 'PropertyAgeTo', 'PropertyAge');

            //Date Search
            $AvailableFromRange = $this->GetDateRangeSearchMust($paramArray, 'AvailableFrom', 'AvailableFrom');

            //Define All searchable fields here
            $mustArray = [
                'must' => [
                    $MultiMatch,
                    $TypePrefix,
                    $CityPrefix,
                    $CategoryPrefix,
                    $BuilderNameTerm,
                    $SubCategoryTerm,
                    $BHKTerm,
                    $BathroomTerm,
                    $BalconyTerm,
                    $TenantTypesTerm,
                    $IdealForBusinessTerm,
                    $PersonalWashroomTerm,
                    $CafeteriaAreaTerm,
                    $FurnishTypeTerm,
                    $FloorTerm,
                    $PriceNegotiableTerm,
                    $PGTypeTerm,
                    $PreferredTenantTerm,
                    $PGRoomTypeTerm,
                    $AttachBathroomTerm,
                    $FoodFacilityTerm,
                    $ParkingTerm,
                    $RoomAmenitiesTerm,
                    $PGRulesTerm,
                    $CommonAmenitiesTerm,
                    $TransactionTypeTerm,
                    $PriceRange,
                    $MonthlyRentRange,
                    $MaintenanceChargesRange,
                    $SecurityDepositRange,
                    $BuiltupAreaRange,
                    $CarpetAreaRange,
                    $TokenAdvanceRange,
                    $PropertyAgeRange,
                    $AvailableFromRange,
                    $NoOfOpenSiteTerm,
                    $AreaTerm,
                    $FlatmatePropertyTypes,
                    $FlatmateTenantType,
                    $FlatmateRoomType,
                    $FlatmateNonVegAllowed,
                    $FlatmateGatedSecurity,
                    $FlatmatesPreferredTenantType,
                    $ExpectedDurationOfMonthTerm,
                    $ExpectedDurationOfYearTerm,
                    $IsApprovedTerm,
                    $IsPublishedTerm,
                ]
            ];

            $newArray = $mustArray['must'];
            $mustFilteredArray = array_filter($newArray);
            $mustFilteredArray = array_values($mustFilteredArray);

            //echo '<pre>';
            //print_r($mustFilteredArray);exit;

            if (count($mustFilteredArray)) {
                //Generate
                $params = [
                    'index' => env('ElasticSearchPropertyIndex'),
                    'type' => env('ElasticSearchPropertyType'),
                    'body' => [
                        "from" => $indexNO,
                        "size" => $noOfObjects,
                        'query' => [
                            'bool' => [
                                'filter' => [
                                    'bool' => [
                                        'must' => [
                                            $mustFilteredArray
                                        ]
                                    ]
                                ]
                            ]
                        ],

                    ]
                ];


                $params['body']['sort'] = [
                    [$sortByTypeTitle => ['order' => $sortByOrder]]
                ];


                //echo '<pre>';
                //print_r($response);exit;
                $response = $client->search($params);
                //echo '<pre>';
                //print_r($response);exit;
                return $response;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    public function searchServiceIndex($paramArray)
    {
        $client = $this->createClient();

        $indexParams['index']  = env('ElasticSearchServiceIndex');
        $exists = $client->indices()->exists($indexParams);
        if ($exists) {
            $sortByTypeTitle = $paramArray['SortByTypeTitle'];
            $sortByOrder = $paramArray['SortByOrder'];

            $indexNO = 0;
            if (array_key_exists('indexNO', $paramArray)) {
                if ($paramArray['indexNO'])
                    $indexNO = $paramArray['indexNO'];
            }

            $noOfObjects = 10;
            if (array_key_exists('noOfObjects', $paramArray)) {
                if ($paramArray['noOfObjects'])
                    $noOfObjects = $paramArray['noOfObjects'];
            }

            //Prefix search
            $searchPrefixOption = "prefix";
            $CityPrefix = $this->GetSearchMust($paramArray, 'City', $searchPrefixOption);

            //Array Search
            $searchTermsOption = "terms";
            $CategoryTerms = $this->GetSearchMust($paramArray, 'Category', $searchTermsOption);
            $SubCategoryTerms = $this->GetSearchMust($paramArray, 'SubCategory', $searchTermsOption);
            $PaymentTypesTerms = $this->GetSearchMust($paramArray, 'PaymentTypes', $searchTermsOption);
            $AreasTerms = $this->GetSearchMust($paramArray, 'Areas', $searchTermsOption);


            $ZipTerms = $this->GetSearchMust($paramArray, 'Zips', $searchTermsOption);

            //Rang Search
            $PriceFromRange = $this->GetRangeFromFields($paramArray, 'PriceFrom', 'MinPrice');
            $PriceToRange = $this->GetRangeToFields($paramArray, 'PriceTo', 'MaxPrice');

            //Term Search
            $searchTermOption = "term";
            $IsApprovedTerm = $this->GetSearchMust($paramArray, 'IsApproved', $searchTermOption);
            $IsPublishedTerm = $this->GetSearchMust($paramArray, 'IsPublished', $searchTermOption);

            //Define All searchable fields here
            $mustArray = [
                'must' => [
                    $CityPrefix,
                    $CategoryTerms,
                    $SubCategoryTerms,
                    $PaymentTypesTerms,
                    $AreasTerms,
                    $ZipTerms,
                    $PriceFromRange,
                    $PriceToRange,
                    $IsApprovedTerm,
                    $IsPublishedTerm,
                ]
            ];

            $newArray = $mustArray['must'];
            $mustFilteredArray = array_filter($newArray);
            $mustFilteredArray = array_values($mustFilteredArray);

            //echo '<pre>';
            //print_r($mustFilteredArray);exit;

            if (count($mustFilteredArray)) {
                //Generate
                $params = [
                    'index' => env('ElasticSearchServiceIndex'),
                    'type' => env('ElasticSearchServiceType'),
                    'body' => [
                        "from" => $indexNO,
                        "size" => $noOfObjects,
                        'query' => [
                            'bool' => [
                                'filter' => [
                                    'bool' => [
                                        'must' => [
                                            $mustFilteredArray
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ];

                $params['body']['sort'] = [
                    [$sortByTypeTitle => ['order' => $sortByOrder]]
                ];

                $response = $client->search($params);
                return $response;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    public function searchProjectIndex($paramArray)
    {
        $client = $this->createClient();

        $indexParams['index']  = env('ElasticSearchProjectIndex');
        $exists = $client->indices()->exists($indexParams);
        if ($exists) {
            $sortByTypeTitle = $paramArray['SortByTypeTitle'];
            $sortByOrder = $paramArray['SortByOrder'];

            $indexNO = 0;
            if (array_key_exists('indexNO', $paramArray)) {
                if ($paramArray['indexNO'])
                    $indexNO = $paramArray['indexNO'];
            }

            $noOfObjects = 10;
            if (array_key_exists('noOfObjects', $paramArray)) {
                if ($paramArray['noOfObjects'])
                    $noOfObjects = $paramArray['noOfObjects'];
            }

            //Prefix search
            $searchPrefixOption = "prefix";
            $CategoryPrefix = $this->GetSearchMust($paramArray, 'Category', $searchPrefixOption);
            $PincodePrefix = $this->GetSearchMust($paramArray, 'Pincode', $searchPrefixOption);
            $CityPrefix = $this->GetSearchMust($paramArray, 'City', $searchPrefixOption);


            //Term Search
            $searchTermOption = "term";
            $IsApprovedTerm = $this->GetSearchMust($paramArray, 'IsApproved', $searchTermOption);
            $IsPublishedTerm = $this->GetSearchMust($paramArray, 'IsPublished', $searchTermOption);

            //Array Search
            $searchTermsOption = "terms";
            $ApprovalAuthority = $this->GetSearchMust($paramArray, 'ApprovalAuthority', $searchTermsOption);
            $PriceNegotiable = $this->GetSearchMust($paramArray, 'PriceNegotiable', $searchTermsOption);
            $Amenities = $this->GetSearchMust($paramArray, 'Amenities', $searchTermsOption);
            $AreaTerm = $this->GetSearchMust($paramArray, 'Area', $searchTermsOption);

            //Date Search
            $LaunchDateFromRange = $this->GetDateRangeSearchMust($paramArray, 'LaunchDate', 'LaunchDate');
            //print_r($LaunchDateFromRange);exit;

            //Define All searchable fields here
            $mustArray = [
                'must' => [
                    $CityPrefix,
                    $CategoryPrefix,
                    $PincodePrefix,
                    $ApprovalAuthority,
                    $PriceNegotiable,
                    $Amenities,
                    $AreaTerm,
                    $LaunchDateFromRange,
                    $IsApprovedTerm,
                    $IsPublishedTerm,
                ]
            ];

            $newArray = $mustArray['must'];
            $mustFilteredArray = array_filter($newArray);
            $mustFilteredArray = array_values($mustFilteredArray);

            //echo '<pre>';
            //print_r($mustFilteredArray);exit;

            if (count($mustFilteredArray)) {
                //Generate
                $params = [
                    'index' => env('ElasticSearchProjectIndex'),
                    'type' => env('ElasticSearchProjectType'),
                    'body' => [
                        "from" => $indexNO,
                        "size" => $noOfObjects,
                        'query' => [
                            'bool' => [
                                'filter' => [
                                    'bool' => [
                                        'must' => [
                                            $mustFilteredArray
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ];

                $params['body']['sort'] = [
                    [$sortByTypeTitle => ['order' => $sortByOrder]]
                ];


                //echo '<pre>';
                //print_r($params);exit;
                $response = $client->search($params);
                return $response;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    public function testsearchPropertyIndex($paramArray)
    {
        $client = $this->createClient();

        $indexParams['index']  = env('ElasticSearchPropertyIndex');
        $exists = $client->indices()->exists($indexParams);
        if ($exists) {
            $cityPrefix = $this->GetSearchMust($paramArray, 'City', "prefix");

            if (array_key_exists('City', $paramArray)) {
                if ($paramArray['City'] != '') {
                    $cityValue = strtolower($paramArray['City']);
                    $cityPrefix = ["prefix" => ['City' => $cityValue]];
                }
            }

            print_r($cityPrefix);
            exit;

            $typePrefix = '';
            if (array_key_exists('Type', $paramArray)) {
                if ($paramArray['Type'] != '') {
                    $typeValue = strtolower($paramArray['Type']);
                    $typePrefix = ["prefix" => ['Type' => $typeValue]];
                }
            }

            $categoryPrefix = '';
            if (array_key_exists('Category', $paramArray)) {
                if ($paramArray['Category'] != '') {
                    $categoryValue = strtolower($paramArray['Category']);
                    $categoryPrefix = ["prefix" => ['Category' => $categoryValue]];
                }
            }

            $multiMatch = '';
            if (array_key_exists('Location', $paramArray)) {
                if ($paramArray['Location'] != '') {
                    $locationValue = strtolower($paramArray['Location']);
                    $multiMatch = [
                        "multi_match" => [
                            'operator' => "or",
                            "type" => "most_fields",
                            "query" => $locationValue,
                            "lenient" => true,
                            'fields' => [
                                'Title',
                                'Address',
                                'Description',
                                'Pincode'
                            ],
                        ]
                    ];
                }
            }

            $builderNameTerm = '';
            if (array_key_exists('BuilderName', $paramArray)) {
                if (count($paramArray['BuilderName'])) {
                    $builderNamesArray = array_map('trim', $paramArray['BuilderName']);
                    $builderNameTerm = ["terms" => ['BuilderName.keyword' => $builderNamesArray]];
                }
            }

            $subCategoryTerm = '';
            if (array_key_exists('SubCategory', $paramArray)) {
                if (count($paramArray['SubCategory'])) {
                    $subCategoryArray = array_map('trim', $paramArray['SubCategory']);
                    $subCategoryTerm = ["terms" => ['SubCategory.keyword' => $subCategoryArray]];
                }
            }

            $BHKTerm = '';
            if (array_key_exists('Bhk', $paramArray)) {
                if (count($paramArray['Bhk'])) {
                    $BhkArray = array_map('trim', $paramArray['Bhk']);
                    $BHKTerm = ["terms" => ['Bhk.keyword' => $BhkArray]];
                }
            }

            $tenantTerm = '';
            if (array_key_exists('TenantTypes', $paramArray)) {
                if (count($paramArray['TenantTypes'])) {
                    $tenantTypesArray = array_map('trim', $paramArray['TenantTypes']);
                    $tenantTerm = ["terms" => ['TenantTypes.keyword' => $tenantTypesArray]];
                }
            }

            $idealForBusinessTerm = '';
            if (array_key_exists('IdealForBusiness', $paramArray)) {
                if (count($paramArray['IdealForBusiness'])) {
                    $IdealForBusinessArray = array_map('trim', $paramArray['IdealForBusiness']);
                    $idealForBusinessTerm = ["terms" => ['IdealForBusiness.keyword' => $IdealForBusinessArray]];
                }
            }


            // print_r($idealForBusinessTerm);exit;

            $isApprovedTerm = ["term" => ['IsApproved' => 1]];
            $isPublishedTerm = ["term" => ['IsPublished' => 1]];


            $priceRange = '';
            $priceFromValue = 0;
            $priceToValue = 0;
            if (array_key_exists('PriceFrom', $paramArray))
                $priceFromValue = $paramArray['PriceFrom'];
            if (array_key_exists('PriceTo', $paramArray))
                $priceToValue = $paramArray['PriceTo'];

            if ($priceFromValue || $priceToValue) {
                $priceRange =  [
                    'range' => [
                        'Price' => [
                            'from' => $priceFromValue,
                            'to' => $priceToValue,
                        ]
                    ]
                ];
            }


            //Define All searchable fields here
            $mustArray = [
                'must' => [
                    $multiMatch,
                    $cityPrefix,
                    $typePrefix,
                    $categoryPrefix,
                    $isApprovedTerm,
                    $isPublishedTerm,
                    $builderNameTerm,
                    $subCategoryTerm,
                    $BHKTerm,
                    $tenantTerm,
                    $idealForBusinessTerm,
                    $priceRange,
                ]
            ];

            $newArray = $mustArray['must'];
            $mustFilteredArray = array_filter($newArray);
            $mustFilteredArray = array_values($mustFilteredArray);

            //echo '<pre>';
            //print_r($mustFilteredArray);exit;

            if (count($mustFilteredArray)) {
                //Generate
                $params = [
                    'index' => env('ElasticSearchPropertyIndex'),
                    'type' => env('ElasticSearchPropertyType'),
                    'body' => [
                        //"from"=> 20,
                        //"size"=> 10,
                        'query' => [
                            'bool' => [
                                'filter' => [
                                    'bool' => [
                                        'must' => [
                                            $mustFilteredArray
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ];


                //echo '<pre>';
                //print_r($params);exit;
                $response = $client->search($params);
                return $response;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }
}
