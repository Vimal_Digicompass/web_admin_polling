<?php

namespace App\Services\AdminPollsScheduleWatch;

use App\Models\Schedule\AdminPollsTiming;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use App\Services\CreateKYCApprovalService;


class AdminPollsScheduleWatchService
{
    public function __invoke()
    {

        $StartTime = Carbon::now()->hour;
        Log::info($StartTime);
        $CurrentDate = Carbon::now()->format('d-m-Y');
        AdminPollsTiming::query()
            ->select(['poll_timing_id', 'criteria_id', 'city_id'])
            ->where('starttime', (int) $StartTime)
            ->whereHas('logs', function (BUILDER $query) use ($CurrentDate) {
                $query->where('date', '==', $CurrentDate);
            }, '<', 11)
            ->orderBy('criteria_id')
            ->orderBy('city_id')
            ->chunk(50, function ($items) {
                $grouped = $items
                    ->groupBy(['criteria_id', function ($item) {
                        return $item['city_id'];
                    }]);

                $this->handleServices($grouped);
            });
    }

    private function handleServices($groupedSchedules)
    {
        /**
         * TODO: How to tackle multiple instances running this service?
         */

        Log::info($groupedSchedules);
        foreach ($groupedSchedules as $criteriaId => $cityArray) {
            Log::info($criteriaId);
            foreach ($cityArray as $cityID => $channelsArray) {
                $arrayOfAdminPollsCriteria = $channelsArray->map->only(['poll_timing_id', 'criteria_id', 'city_id']);
                Log::info(Config::get('constants.CRITERION.KYC_APPROVAL'));
                Log::info($arrayOfAdminPollsCriteria);
                switch ($criteriaId) {
                    case Config::get('constants.CRITERION.KYC_APPROVAL'):
                        Log::info($criteriaId);
                        (new CreateKYCApprovalService(
                            $cityID,
                            $arrayOfAdminPollsCriteria
                        ))();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
