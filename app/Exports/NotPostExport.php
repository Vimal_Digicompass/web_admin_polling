<?php

namespace App\Exports;

use stdClass;
use App\Models\Master\CallStatus;
use App\Models\Master\KYCRejectedStatus;
use App\Models\Master\KYCStatus;
use App\Models\SqFt\User;
use App\Models\Master\State;
use Log;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Master\City;
use App\Models\Master\UserType;
use App\Models\Master\UserCategory;
use App\Models\UserActivity\NotPost\NotPostAssign;
use App\User as CRMUser;

use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class NotPostExport implements FromView
{
    use Exportable;
    /**
     * @return \Illuminate\Support\Collection
     */

    public function __construct($getCustomerData, $request, $text)
    {
        $this->getCustomerData = $getCustomerData;
        $this->request = $request;
        $this->text = $text;
    }

    public function view(): View
    {
        $getCustomerData = $this->getCustomerData;
        $Arr = [];
        foreach ($getCustomerData as $key => $value) {

            $name = $value['firstname'] . ' ' . $value['lastname'];
            $usertype = UserType::where('titlevalue', $value['user_type'])->first();
            $usertypetitle = $usertype['title'];
            $category = UserCategory::where('id', $value['category_id'])->first();
            if ($category) {
                $category = $category['title'];
            } else {
                $category = "";
            }
            $mobilenumber = $value['mobile_number'];
            $dateofregistration = Carbon::parse( $value['created_at'])
                ->setTimezone('Asia/Calcutta')
                ->format('d-m-Y');
            $city = City::where('id', $value['city_id'])->first();
            if ($city) {
                $city_title = $city['title'];
                $stateid = $city['state_id'];

                $statetitle = State::where('id', $stateid)->first();
                if ($statetitle) {
                    $state =  $statetitle['title'];
                } else {
                    $state =  "";
                }
            } else {
                $city_title = "";
                $state  = "";
            }
            $address = $value['address'];
            $getData =    NotPostAssign::where('user_notpost_assign_trn_id', $value['trn_id'])->first();
            $crm_user_id = $getData['crm_user_id'];
            $crmData = CRMUser::where('user_id', $crm_user_id)->first();
            $assignedTo =  $crmData['username'];
            $callstatus = $getData['status'];
            if ($callstatus != 0) {
                $getcallstatus = CallStatus::where('id', $callstatus)->first();
                $callstatus = $getcallstatus['title'];
            } else {
                $callstatus = "";
            }

            if ($getData['followup_date'] != NULL) {
                $followdate =  Carbon::createFromFormat('Y-m-d', $getData['followup_date'])
                    ->setTimezone('Asia/Calcutta')
                    ->format('d-m-Y');
            } else {

                $followdate =  '';
            }

            if ($getData) {
                $remarks =  $getData['remarks'];
            } else {
                $remarks =  '';
            }
            array_push($Arr, [
                'Name' => $name,
                'UserType' => $usertypetitle,
                'UserCategory' => $category,
                'MobileNumber' => $mobilenumber,
                'DateofRegistration' => $dateofregistration,
                'State' => $state,
                'City' => $city_title,
                'Area' => $address,
                'CallingStatus' => $callstatus,
                'FollowupDate' => $followdate,
                'CallRemarks' => $remarks,
                'assignedTo'=> $assignedTo
            ]);
        }

        return view('Exports.notpost', [
            'notpost' => $Arr,
            'text' => $this->text
        ]);
    }
}
