<?php

declare(strict_types=1);

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Carbon;
use App\Models\SqFt\User;
use App\Models\Master\CallStatus;
use App\libraries\AWSElasticSearch;
use App\Models\Service\Service as ServiceService;
use App\Models\Master\PostRejectedStatus;
use App\Models\Master\KYCRejectedStatus;
use App\Models\Master\KYCDocumentList;
use App\Models\Master\KYCStatus;
use App\Models\Master\State;
use App\Models\Master\UserType;
use App\Models\Master\PropertyType;
use App\Models\Master\UserCategory;
use App\Models\Master\PostType;
use App\Models\Master\ServiceCategory;
use App\Models\Master\ProjectCategory;
use App\Models\SqFt\UserKYCDetail;
use App\Models\User\Roles;
use App\Models\SqFt\Project;
use App\Models\SqFt\Property;
use App\Models\SqFt\Service;
use App\Models\UserActivity\NotEnquirePost\NotEnquirePostAssign;
use App\Models\UserActivity\NotMakePayment\NotMakePaymentAssign;
use App\Models\UserActivity\WalletRechargeFailed\WalletRechargeFailedAssign;
use App\Models\UserActivity\WalletExpiry\WalletExpiryAssign;
use App\Models\UserActivity\UpgradeBusiness\UpgradeBusinessAssign;
use App\Models\UserActivity\NotPostServiceRequirement\NotPostServiceRequirementAssign;
use App\Models\UserActivity\KYCApproval\KYCApprovalAssign;
use App\Models\UserActivity\KYCPending\KYCPendingAssign;
use App\Models\UserActivity\KYCRejected\KYCRejectedAssign;
use App\Models\UserActivity\LeadsNotViewed\LeadsNotViewedAssign;
use App\Models\UserActivity\MarkedFavourite\MarkedFavouriteAssign;
use App\Models\UserActivity\PostApproval\PostApprovalAssign;
use App\Models\UserActivity\PostRejected\PostRejectedAssign;
use App\Models\UserActivity\InActive\InActiveAssign;
use App\Models\UserActivity\NotPost\NotPostAssign;
use App\Models\Setting\NotificationMaster;
use Illuminate\Support\Facades\Auth;
use Aws\S3\S3Client;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

if (!function_exists('makeAWSClient')) {
    function makeAWSClient($serviceNameKey)
    {
        return App::make('aws')->createClient($serviceNameKey);
    }
}


if (!function_exists('commonFunctionData')) {
    function commonFunctionData($role_id, $criteriaId)
    {
        $user_id = Auth::user()->user_id;
        $notificationData = NotificationMaster::where('crm_user_id', $user_id)
            ->orderBy('notification_id', 'DESC')
            ->get();
        $notificationDataNew = NotificationMaster::where('is_read', 0)
            ->where('crm_user_id', $user_id)->orderBy('notification_id', 'DESC')
            ->count();
        $state = State::all()->where('is_active', 1);
        $userType = UserType::all();
        $callStatus = CallStatus::all();
        $PostRejectedStatus = PostRejectedStatus::all();
        $KYCStatus = KYCStatus::all();
        $ServiceCategory = ServiceCategory::where('is_active', 1)->get();
        $PostType = PostType::all();
        $KYCRejectedStatus = KYCRejectedStatus::all();
        $KYCDocumentList = KYCDocumentList::all();
        $UserCategory = UserCategory::all();
        $ProjectCategory = ProjectCategory::all();
        $PropertyType = PropertyType::where('id', '!=', 1)->get();
        $PermissionDetail = getPermissionAndAccess(0, $role_id);
        $checkpermission = getPermissionAndAccess($criteriaId, $role_id);
        return [
            'state' => $state, 'userType' => $userType, 'callStatus' => $callStatus,
            'PostRejectedStatus' => $PostRejectedStatus, 'KYCStatus' => $KYCStatus, 'ServiceCategory' => $ServiceCategory,
            'PostType' => $PostType, 'KYCRejectedStatus' => $KYCRejectedStatus, 'KYCDocumentList' => $KYCDocumentList,
            'UserCategory' => $UserCategory, 'ProjectCategory' => $ProjectCategory, 'PropertyType' => $PropertyType,
            'PermissionDetail' => $PermissionDetail, 'checkpermission' => $checkpermission,
            'notificationData' => $notificationData, 'notificationDataNew' => $notificationDataNew
        ];
    }
}

if (!function_exists('ConvertNoToWord')) {
    function ConvertNoToWord($no)
    {

        $finalval = 0;
        if ($no <= 100) {
            return $no;
        } else {
            $n =  strlen((string)((int)($no))); // 7
            switch ($n) {
                case 3:
                    $val = $no / 100;
                    $val = round($val, 2);
                    //$finalval =  $val ." hundred";
                    $finalval =  $no;
                    break;
                case 4:
                    $val = $no / 1000;
                    $val = round($val, 2);
                    //$finalval =  $val ." K";
                    $finalval = number_format($no);
                    break;
                case 5:
                    $val = $no / 1000;
                    $val = round($val, 2);
                    //$finalval =  $val ." K";
                    $finalval =  number_format($no);
                    break;
                case 6:
                    $val = $no / 100000;
                    $val = round($val, 2);
                    $finalval =  $val . " L";
                    break;
                case 7:
                    $val = $no / 100000;
                    $val = round($val, 2);
                    $finalval =  $val . " L";
                    break;
                case 8:
                    $val = $no / 10000000;
                    $val = round($val, 2);
                    $finalval =  $val . " Cr";
                    break;
                case 9:
                    $val = $no / 10000000;
                    $val = round($val, 2);
                    $finalval =  $val . " Cr";
                    break;
                case 10:
                    $val = $no / 10000000;
                    $val = round($val, 2);
                    $finalval =  $val . " Cr";
                    break;
                case 11:
                    $val = $no / 10000000;
                    $val = round($val, 2);
                    $finalval =  $val . " Cr";
                    break;

                default:
                    echo "";
            }

            return $finalval;
        }
    }
}
if (!function_exists('getUserDahboardData')) {
    function getUserDahboardData(
        $criteria,
        $userId,
        $PermissionDetail = null,
        $fromdate,
        $todate
    ) {
        $Arr = [];
        $currentDate = Carbon::now()->format('Y-m-d');
        foreach ($PermissionDetail as $key => $value) {
            if ($value['permission_id'] == Config::get('constants.CRITERION.KYC_APPROVAL')) {
                $totalAssignCount = KYCApprovalAssign::whereIn('crm_user_id', $userId)
                    ->whereNotIn('updatestatus', [2, 3]);
                $totalFollowpCount = KYCApprovalAssign::whereIn('crm_user_id', $userId)
                    ->where('followup_date', $currentDate);
                $totalProcessed = KYCApprovalAssign::whereIn('crm_user_id', $userId)
                    ->where('updatestatus', 1);

                $waitingCount = KYCApprovalAssign::whereIn('crm_user_id', $userId)
                    ->where('updatestatus', 0);

                // $followupcount =
            } elseif ($value['permission_id'] == Config::get('constants.CRITERION.KYC_PENDING')) {
                $totalAssignCount = KYCPendingAssign::whereIn('crm_user_id', $userId)
                    ->whereNotIn('updatestatus', [2, 3]);
                $totalFollowpCount = KYCPendingAssign::whereIn('crm_user_id', $userId)
                    ->where('followup_date', $currentDate);
                $totalProcessed = KYCPendingAssign::whereIn('crm_user_id', $userId)
                    ->where('updatestatus', 1);

                $waitingCount = KYCPendingAssign::whereIn('crm_user_id', $userId)
                    ->where('updatestatus', 0);
            } else if ($value['permission_id'] == Config::get('constants.CRITERION.KYC_REJECTED')) {
                $totalAssignCount = KYCRejectedAssign::whereIn('crm_user_id', $userId)
                    ->whereNotIn('updatestatus', [2, 3]);
                $totalFollowpCount = KYCRejectedAssign::whereIn('crm_user_id', $userId)
                    ->where('followup_date', $currentDate);
                $totalProcessed = KYCRejectedAssign::whereIn('crm_user_id', $userId)
                    ->where('updatestatus', 1);

                $waitingCount = KYCRejectedAssign::whereIn('crm_user_id', $userId)
                    ->where('updatestatus', 0);
            } else if ($value['permission_id'] == Config::get('constants.CRITERION.LEADS_NOT_VIEWED')) {
                $totalAssignCount = LeadsNotViewedAssign::whereIn('crm_user_id', $userId)
                    ->whereNotIn('updatestatus', [2, 3]);
                $totalFollowpCount = LeadsNotViewedAssign::whereIn('crm_user_id', $userId)
                    ->where('followup_date', $currentDate);
                $totalProcessed = LeadsNotViewedAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 1);
                $waitingCount = LeadsNotViewedAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 0);
            } else if ($value['permission_id'] == Config::get('constants.CRITERION.MARKED_FAVOURITE')) {
                $totalAssignCount = MarkedFavouriteAssign::whereIn('crm_user_id', $userId)->whereNotIn('updatestatus', [2, 3]);
                $totalProcessed = MarkedFavouriteAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 1);
                $waitingCount = MarkedFavouriteAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 0);
                $totalFollowpCount = MarkedFavouriteAssign::whereIn('crm_user_id', $userId)
                    ->where('followup_date', $currentDate);
            } else if ($value['permission_id'] == Config::get('constants.CRITERION.POST_REJECTED')) {
                $totalAssignCount = PostRejectedAssign::whereIn('crm_user_id', $userId)->whereNotIn('updatestatus', [2, 3]);
                $totalProcessed = PostRejectedAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 1);
                $waitingCount = PostRejectedAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 0);
                $totalFollowpCount = PostRejectedAssign::whereIn('crm_user_id', $userId)
                    ->where('followup_date', $currentDate);
            } else if ($value['permission_id'] == Config::get('constants.CRITERION.POST_APPROVAL')) {
                $totalAssignCount = PostApprovalAssign::whereIn('crm_user_id', $userId)->whereNotIn('updatestatus', [2, 3]);
                $totalProcessed = PostApprovalAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 1);
                $waitingCount = PostApprovalAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 0);
                $totalFollowpCount = PostApprovalAssign::whereIn('crm_user_id', $userId)
                    ->where('followup_date', $currentDate);
            } else if ($value['permission_id'] == Config::get('constants.CRITERION.NOT_ENQUIRE_POST')) {
                $totalAssignCount = NotEnquirePostAssign::whereIn('crm_user_id', $userId)->whereNotIn('updatestatus', [2, 3]);
                $totalProcessed = NotEnquirePostAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 1);
                $waitingCount = NotEnquirePostAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 0);
                $totalFollowpCount = NotEnquirePostAssign::whereIn('crm_user_id', $userId)
                    ->where('followup_date', $currentDate);
            } else if ($value['permission_id'] == Config::get('constants.CRITERION.NOT_POST_SERVICE_REQ')) {
                $totalAssignCount = NotPostServiceRequirementAssign::whereIn('crm_user_id', $userId)->whereNotIn('updatestatus', [2, 3]);
                $totalProcessed = NotPostServiceRequirementAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 1);
                $waitingCount = NotPostServiceRequirementAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 0);
                $totalFollowpCount = NotPostServiceRequirementAssign::whereIn('crm_user_id', $userId)
                    ->where('followup_date', $currentDate);
            } else if ($value['permission_id'] == Config::get('constants.CRITERION.INACTIVE_USER')) {
                $totalAssignCount = InActiveAssign::whereIn('crm_user_id', $userId)->whereNotIn('updatestatus', [2, 3]);
                $totalProcessed = InActiveAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 1);
                $waitingCount = InActiveAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 0);
                $totalFollowpCount = InActiveAssign::whereIn('crm_user_id', $userId)
                    ->where('followup_date', $currentDate);
            } else if ($value['permission_id'] == Config::get('constants.CRITERION.NOT_POST')) {
                $totalAssignCount = NotPostAssign::whereIn('crm_user_id', $userId)->whereNotIn('updatestatus', [2, 3]);
                $totalProcessed = NotPostAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 1);
                $waitingCount = NotPostAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 0);
                $totalFollowpCount = NotPostAssign::whereIn('crm_user_id', $userId)
                    ->where('followup_date', $currentDate);
            } else if ($value['permission_id'] == Config::get('constants.CRITERION.RECHARGE_FAILED')) {
                $totalAssignCount = WalletRechargeFailedAssign::whereIn('crm_user_id', $userId)->whereNotIn('updatestatus', [2, 3]);
                $totalProcessed = WalletRechargeFailedAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 1);
                $waitingCount = WalletRechargeFailedAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 0);
                $totalFollowpCount = WalletRechargeFailedAssign::whereIn('crm_user_id', $userId)
                    ->where('followup_date', $currentDate);
            } else if ($value['permission_id'] == Config::get('constants.CRITERION.PAYMENT_PENDING')) {
                $totalAssignCount = NotMakePaymentAssign::whereIn('crm_user_id', $userId)->whereNotIn('updatestatus', [2, 3]);
                $totalProcessed = NotMakePaymentAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 1);
                $waitingCount = NotMakePaymentAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 0);
                $totalFollowpCount = NotMakePaymentAssign::whereIn('crm_user_id', $userId)
                    ->where('followup_date', $currentDate);
            } else if ($value['permission_id'] == Config::get('constants.CRITERION.PACKAGE_EXPIRY')) {
                $totalAssignCount = WalletExpiryAssign::whereIn('crm_user_id', $userId)->whereNotIn('updatestatus', [2, 3]);
                $totalProcessed = WalletExpiryAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 1);
                $waitingCount = WalletExpiryAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 0);
                $totalFollowpCount = WalletExpiryAssign::whereIn('crm_user_id', $userId)
                    ->where('followup_date', $currentDate);
            } else if ($value['permission_id'] == Config::get('constants.CRITERION.UPGRADE_TO_BUSINESS_USER')) {
                $totalAssignCount = UpgradeBusinessAssign::whereIn('crm_user_id', $userId)->whereNotIn('updatestatus', [2, 3]);
                $totalProcessed = UpgradeBusinessAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 1);
                $waitingCount = UpgradeBusinessAssign::whereIn('crm_user_id', $userId)->where('updatestatus', 0);
                $totalFollowpCount = UpgradeBusinessAssign::whereIn('crm_user_id', $userId)
                    ->where('followup_date', $currentDate);
            }
            $totalAssignCount = $totalAssignCount->whereBetween('created_at', array($fromdate, $todate))
                ->count();
            $totalProcessed = $totalProcessed->whereBetween('created_at', array($fromdate, $todate))
                ->count();
            $waitingCount = $waitingCount->whereBetween('created_at', array($fromdate, $todate))
                ->count();
            $totalFollowpCount = $totalFollowpCount->count();
            array_push($Arr, [
                'criteria' => $value['permission_name'],
                'path' => $value['permission_path'],
                'totalAssignCount' => number_format($totalAssignCount),
                'totalProcessed' => number_format($totalProcessed),
                'waitingCount' => number_format($waitingCount),
                'totalFollowupCount' => number_format($totalFollowpCount),
                'currentDate' => $currentDate
            ]);
        }

        return $Arr;
    }
}
if (!function_exists('active_class')) {
    function active_class($path, $active = 'active')
    {
        return call_user_func_array('Request::is', (array)$path) ? $active : '';
    }
}
if (!function_exists('is_active_route')) {
    function is_active_route($path)
    {
        return call_user_func_array('Request::is', (array)$path) ? 'true' : 'false';
    }
}
if (!function_exists('show_class')) {
    function show_class($path)
    {
        return call_user_func_array('Request::is', (array)$path) ? 'show' : '';
    }
}
/**
 * Function that returns default success json response
 *
 * @param array $dataForResponse
 * @return \Illuminate\Http\JsonResponse
 */
if (!function_exists('successJsonResponce')) {
    function successJsonResponce(array $dataForResponse = []): JsonResponse
    {
        if (array_key_exists('message', $dataForResponse))
            return response()->json($dataForResponse);
        return response()->json(array_merge(['message' => 'Success'], $dataForResponse));
    }
}

/**
 * Function that returns default record created json response
 *
 * @param array $dataForResponse
 * @return \Illuminate\Http\JsonResponse
 */
if (!function_exists('createdJsonResponse')) {
    function createdJsonResponse(array $dataForResponse = []): JsonResponse
    {
        if (array_key_exists('message', $dataForResponse))
            return response()->json(
                $dataForResponse,
                Config::get('constants.HTTP_STATUS.SUCCESS.CREATE')
            );
        return response()->json(
            array_merge(['message' => 'Record created successfully.'], $dataForResponse),
            Config::get('constants.HTTP_STATUS.SUCCESS.CREATE')
        );
    }
}

/**
 * Function that returns default record updated json response
 *
 * @param array $dataForResponse
 * @return \Illuminate\Http\JsonResponse
 */
if (!function_exists('updatedJsonResponse')) {
    function updatedJsonResponse(array $dataForResponse = []): JsonResponse
    {
        if (array_key_exists('message', $dataForResponse))
            return response()->json(
                $dataForResponse,
                Config::get('constants.HTTP_STATUS.SUCCESS.UPDATE')
            );
        return response()->json(
            array_merge(['message' => 'Record edited successfully.'], $dataForResponse),
            Config::get('constants.HTTP_STATUS.SUCCESS.UPDATE')
        );
    }
}

/**
 * Function that returns default bad request json response
 *
 * @param array $dataForResponse
 * @return \Illuminate\Http\JsonResponse
 */
if (!function_exists('badRequestJsonResponse')) {
    function badRequestJsonResponse(array $dataForResponse = []): JsonResponse
    {
        if (array_key_exists('message', $dataForResponse))
            return response()->json(
                $dataForResponse,
                Config::get('constants.HTTP_STATUS.BAD_REQUEST')
            );
        return response()->json(
            array_merge(['message' => 'Record not found.'], $dataForResponse),
            Config::get('constants.HTTP_STATUS.BAD_REQUEST')
        );
    }
}



if (!function_exists('getObjectFromS3')) {
    function getObjectFromS3(string $s3FullFileName)
    {
        $data = Storage::disk('s3')->get($s3FullFileName);
        return $data;
    }
}

if (!function_exists('putObjectToS3')) {
    function putObjectToS3($data, $filename)
    {
        $s3 = Storage::disk('s3');
        $path = $s3->put($filename, fopen($data->path(), 'r+'), 'public');
        // $path = Storage::disk('s3')->put($path, $data);
        return $path;
    }
}

if (!function_exists('unlinkfiles')) {

    function unlinkfiles($path)
    {

        $files = glob(base_path() . '/' . $path . '*'); // get all file names
        foreach ($files as $file) { // iterate files
            if (is_file($file)) {
                unlink($file); // delete file
            }
        }
    }
}
if (!function_exists('isFileExist')) {

    function isFileExist($filepath)
    {
        $filepath = Storage::disk('s3')->exists($filepath);
        return $filepath;
    }
}

if (!function_exists('deleteFile')) {

    function deleteFile($filepath)
    {

        $isFileExist = isFileExist($filepath);
        if ($isFileExist)
            Storage::disk('s3')->delete(env('AWS_BUCKET'), $filepath);
    }
}
if (!function_exists('uploadFile')) {

    function uploadFile($fileObj, $destinationpath, $makepublic = true)
    {

        $success = false;

        $s3 = Storage::disk('s3');
        if ($makepublic) {

            $s3->put($destinationpath, fopen($fileObj->path(), 'r+'), 'public');
            $success = true;
        } else {
            $success = false;
        }

        return $success;
    }
}

if (!function_exists('removeSpacesFromFileName')) {

    function removeSpacesFromFileName($filename, $prefix = "")
    {
        $prefix = $prefix ? $prefix . "_" : '';
        return $prefix . str_replace(" ", "_", $filename);
    }
}
if (!function_exists('GetMyKey')) {

    function GetMyKey($encryption)
    {

        // Store the cipher method
        $ciphering = "AES-256-CTR";

        // Use OpenSSl Encryption method
        $iv_length = openssl_cipher_iv_length($ciphering);
        $options = 0;

        // Non-NULL Initialization Vector for decryption
        $decryption_iv = '1234567891011121';
        // Store the decryption key
        $decryption_key = "1sqftAK";
        // Use openssl_decrypt() function to decrypt the data
        $decryption = openssl_decrypt(
            $encryption,
            $ciphering,
            $decryption_key,
            $options,
            $decryption_iv
        );
        // Display the decrypted string
        return $decryption;
    }
}
if (!function_exists('getFileURLPublicPath')) {

    function getFileURLPublicPath($filepath, $foceCheckS3 = 0)
    {
        $exists = Storage::disk('s3')->exists($filepath);
        if ($exists) {
            $AK = getMyKey(env('AWS_ACCESS_KEY_ID_1'));
            $SK = getMyKey(env('AWS_SECRET_ACCESS_KEY_1'));

            $client =  new S3Client([
                'region' => env('AWS_DEFAULT_REGION'),
                'version' => 'latest',
                'credentials' => [
                    'key' => $AK,
                    'secret' => $SK
                ],
            ]);

            $cmd =     $client->getCommand('GetObject', [
                'Bucket' => env('AWS_BUCKET'),
                'Key' => $filepath
            ]);
            $request = $client->createPresignedRequest($cmd, '+30 seconds');
            $presignedUrl = (string)$request->getUri();
            $filepath = $presignedUrl;
            return $filepath;
        } else {
            return $filepath = '';
        }
    }
}

if (!function_exists('getFileURLPublicPathPost')) {

    function getFileURLPublicPathPost($filepath, $posttype, $post_id, $imagetype)
    {
        if ($posttype == "PR") {
            if ($imagetype == "image") {
                $destDirPath = Config::get('constants.PROJECT_FILE_UPLOAD_PATH') . "/" . $post_id . '/images' . '/' . $filepath;
            } else if ($imagetype == "video") {
                $destDirPath = Config::get('constants.PROJECT_FILE_UPLOAD_PATH') . "/" . $post_id . '/video' . '/' . $filepath;
            } else {
                $destDirPath = Config::get('constants.PROJECT_FILE_UPLOAD_PATH') . "/" . $post_id . '/' . $imagetype . '/' . $filepath;
            }
        } else if ($posttype == "S") {
            if ($imagetype == "image") {
                $destDirPath = Config::get('constants.SERVICE_FILE_UPLOAD_PATH') . "/" . $post_id . '/images' . '/' . $filepath;
            }
        } else {
            if ($imagetype == "image") {
                $destDirPath = Config::get('constants.PROPERTY_FILE_UPLOAD_PATH') . "/" . $post_id . '/images' . '/' . $filepath;
            }
        }
        $exists = Storage::disk('s3')->exists($destDirPath);
        if ($exists) {
            $AK = getMyKey(env('AWS_ACCESS_KEY_ID_1'));
            $SK = getMyKey(env('AWS_SECRET_ACCESS_KEY_1'));
            $client =  new S3Client([
                'region' => env('AWS_DEFAULT_REGION'),
                'version' => 'latest',
                'credentials' => [
                    'key' => $AK,
                    'secret' => $SK
                ],
            ]);
            $cmd =     $client->getCommand('GetObject', [
                'Bucket' => env('AWS_BUCKET'),
                'Key' => $destDirPath
            ]);
            $request = $client->createPresignedRequest($cmd, '+30 seconds');
            return $presignedUrl = (string)$request->getUri();
        } else {
            return $filepath = '';
        }
    }
}
if (!function_exists('getFileTypeByFileName')) {

    function getFileTypeByFileName($fileName)
    {
        $myfileextension = pathinfo($fileName, PATHINFO_EXTENSION);

        switch ($myfileextension) {

                ///Image Mime Types

            case 'csv':
                $mimetype = "text/csv";
                break;
            case 'jpg':
                $mimetype = "image/jpg";
                break;
            case 'jpeg':
                $mimetype = "image/jpeg";
                break;
            case 'gif':
                $mimetype = "image/gif";
                break;
            case 'png':
                $mimetype = "image/png";
                break;
            case 'bm':
                $mimetype = "image/bmp";
                break;
            case 'bmp':
                $mimetype = "image/bmp";
                break;
            case 'art':
                $mimetype = "image/x-jg";
                break;
            case 'dwg':
                $mimetype = "image/x-dwg";
                break;
            case 'dxf':
                $mimetype = "image/x-dwg";
                break;
            case 'flo':
                $mimetype = "image/florian";
                break;
            case 'fpx':
                $mimetype = "image/vnd.fpx";
                break;
            case 'g3':
                $mimetype = "image/g3fax";
                break;
            case 'ief':
                $mimetype = "image/ief";
                break;
            case 'jfif':
                $mimetype = "image/pjpeg";
                break;
            case 'jfif-tbnl':
                $mimetype = "image/jpeg";
                break;
            case 'jpe':
                $mimetype = "image/pjpeg";
                break;
            case 'jps':
                $mimetype = "image/x-jps";
                break;
            case 'jut':
                $mimetype = "image/jutvision";
                break;
            case 'mcf':
                $mimetype = "image/vasa";
                break;
            case 'nap':
                $mimetype = "image/naplps";
                break;
            case 'naplps':
                $mimetype = "image/naplps";
                break;
            case 'nif':
                $mimetype = "image/x-niff";
                break;
            case 'niff':
                $mimetype = "image/x-niff";
                break;
            case 'cod':
                $mimetype = "image/cis-cod";
                break;
            case 'ief':
                $mimetype = "image/ief";
                break;
            case 'svg':
                $mimetype = "image/svg+xml";
                break;
            case 'tif':
                $mimetype = "image/tiff";
                break;
            case 'tiff':
                $mimetype = "image/tiff";
                break;
            case 'ras':
                $mimetype = "image/x-cmu-raster";
                break;
            case 'cmx':
                $mimetype = "image/x-cmx";
                break;
            case 'ico':
                $mimetype = "image/x-icon";
                break;
            case 'pnm':
                $mimetype = "image/x-portable-anymap";
                break;
            case 'pbm':
                $mimetype = "image/x-portable-bitmap";
                break;
            case 'pgm':
                $mimetype = "image/x-portable-graymap";
                break;
            case 'ppm':
                $mimetype = "image/x-portable-pixmap";
                break;
            case 'rgb':
                $mimetype = "image/x-rgb";
                break;
            case 'xbm':
                $mimetype = "image/x-xbitmap";
                break;
            case 'xpm':
                $mimetype = "image/x-xpixmap";
                break;
            case 'xwd':
                $mimetype = "image/x-xwindowdump";
                break;
            case 'rgb':
                $mimetype = "image/x-rgb";
                break;
            case 'xbm':
                $mimetype = "image/x-xbitmap";
                break;
            case "wbmp":
                $mimetype = "image/vnd.wap.wbmp";
                break;

                //Files MIME Types

            case 'css':
                $mimetype = "text/css";
                break;
            case 'htm':
                $mimetype = "text/html";
                break;
            case 'html':
                $mimetype = "text/html";
                break;
            case 'stm':
                $mimetype = "text/html";
                break;
            case 'c':
                $mimetype = "text/plain";
                break;
            case 'h':
                $mimetype = "text/plain";
                break;
            case 'txt':
                $mimetype = "text/plain";
                break;
            case 'rtx':
                $mimetype = "text/richtext";
                break;
            case 'htc':
                $mimetype = "text/x-component";
                break;
            case 'vcf':
                $mimetype = "text/x-vcard";
                break;


                //Applications MIME Types

            case 'doc':
                $mimetype = "application/msword";
                break;
            case 'xls':
                $mimetype = "application/vnd.ms-excel";
                break;
            case 'ppt':
                $mimetype = "application/vnd.ms-powerpoint";
                break;
            case 'pps':
                $mimetype = "application/vnd.ms-powerpoint";
                break;
            case 'pot':
                $mimetype = "application/vnd.ms-powerpoint";
                break;

            case "ogg":
                $mimetype = "application/ogg";
                break;
            case "pls":
                $mimetype = "application/pls+xml";
                break;
            case "asf":
                $mimetype = "application/vnd.ms-asf";
                break;
            case "wmlc":
                $mimetype = "application/vnd.wap.wmlc";
                break;
            case 'dot':
                $mimetype = "application/msword";
                break;
            case 'class':
                $mimetype = "application/octet-stream";
                break;
            case 'exe':
                $mimetype = "application/octet-stream";
                break;
            case 'pdf':
                $mimetype = "application/pdf";
                break;
            case 'rtf':
                $mimetype = "application/rtf";
                break;
            case 'xla':
                $mimetype = "application/vnd.ms-excel";
                break;
            case 'xlc':
                $mimetype = "application/vnd.ms-excel";
                break;
            case 'xlm':
                $mimetype = "application/vnd.ms-excel";
                break;

            case 'msg':
                $mimetype = "application/vnd.ms-outlook";
                break;
            case 'mpp':
                $mimetype = "application/vnd.ms-project";
                break;
            case 'cdf':
                $mimetype = "application/x-cdf";
                break;
            case 'tgz':
                $mimetype = "application/x-compressed";
                break;
            case 'dir':
                $mimetype = "application/x-director";
                break;
            case 'dvi':
                $mimetype = "application/x-dvi";
                break;
            case 'gz':
                $mimetype = "application/x-gzip";
                break;
            case 'js':
                $mimetype = "application/x-javascript";
                break;
            case 'mdb':
                $mimetype = "application/x-msaccess";
                break;
            case 'dll':
                $mimetype = "application/x-msdownload";
                break;
            case 'wri':
                $mimetype = "application/x-mswrite";
                break;
            case 'cdf':
                $mimetype = "application/x-netcdf";
                break;
            case 'swf':
                $mimetype = "application/x-shockwave-flash";
                break;
            case 'tar':
                $mimetype = "application/x-tar";
                break;
            case 'man':
                $mimetype = "application/x-troff-man";
                break;
            case 'zip':
                $mimetype = "application/zip";
                break;
            case 'xlsx':
                $mimetype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                break;
            case 'pptx':
                $mimetype = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                break;
            case 'docx':
                $mimetype = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                break;
            case 'xltx':
                $mimetype = "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
                break;
            case 'potx':
                $mimetype = "application/vnd.openxmlformats-officedocument.presentationml.template";
                break;
            case 'ppsx':
                $mimetype = "application/vnd.openxmlformats-officedocument.presentationml.slideshow";
                break;
            case 'sldx':
                $mimetype = "application/vnd.openxmlformats-officedocument.presentationml.slide";
                break;

                ///Audio and Video Files

            case 'mp3':
                $mimetype = "audio/mpeg";
                break;
            case 'wav':
                $mimetype = "audio/x-wav";
                break;
            case 'au':
                $mimetype = "audio/basic";
                break;
            case 'snd':
                $mimetype = "audio/basic";
                break;
            case 'm3u':
                $mimetype = "audio/x-mpegurl";
                break;
            case 'ra':
                $mimetype = "audio/x-pn-realaudio";
                break;
            case 'mp2':
                $mimetype = "video/mpeg";
                break;
            case 'mov':
                $mimetype = "video/quicktime";
                break;
            case 'qt':
                $mimetype = "video/quicktime";
                break;
            case 'mp4':
                $mimetype = "video/mp4";
                break;
            case 'm4a':
                $mimetype = "audio/mp4";
                break;
            case 'mp4a':
                $mimetype = "audio/mp4";
                break;
            case 'm4p':
                $mimetype = "audio/mp4";
                break;
            case 'm3a':
                $mimetype = "audio/mpeg";
                break;
            case 'm2a':
                $mimetype = "audio/mpeg";
                break;
            case 'mp2a':
                $mimetype = "audio/mpeg";
                break;
            case 'mp2':
                $mimetype = "audio/mpeg";
                break;
            case 'mpga':
                $mimetype = "audio/mpeg";
                break;
            case '3gp':
                $mimetype = "video/3gpp";
                break;
            case '3g2':
                $mimetype = "video/3gpp2";
                break;
            case 'mp4v':
                $mimetype = "video/mp4";
                break;
            case 'mpg4':
                $mimetype = "video/mp4";
                break;
            case 'm2v':
                $mimetype = "video/mpeg";
                break;
            case 'm1v':
                $mimetype = "video/mpeg";
                break;
            case 'mpe':
                $mimetype = "video/mpeg";
                break;
            case 'avi':
                $mimetype = "video/x-msvideo";
                break;
            case 'midi':
                $mimetype = "audio/midi";
                break;
            case 'mid':
                $mimetype = "audio/mid";
                break;
            case 'amr':
                $mimetype = "audio/amr";
                break;


            default:
                $mimetype = "application/octet-stream";
        }
        return $mimetype;
    }
}
if (!function_exists('updateKYCDocument')) {

    function updateKYCDocument($request)
    {
        $documentnumber = $request->input('documentnumber');
        $selectdocument = $request->input('selectdocuments');
        $user_id = $request->input('user_id');
        $getUserKYCDetail = UserKYCDetail::where('user_id', $user_id)->first();
        $fileNamebackfile = "";
        $data = [];
        if ($request->has('frontfile')) {
            $objFile = $request->file('frontfile');
        }
        if ($selectdocument == 1) {
            $url = "/KYC/pan/";
            $destDirPath = Config::get('constants.FILE_UPLOAD_PATH') . "/" . $user_id . $url;

            if ($getUserKYCDetail['pan_file_name'] != "") {
                deleteFile($destDirPath, true);
            }
            // file_get_contents($image->getPath() . '/' . $image->getFilename())
            $fileNamefrontfile = strtolower(removeSpacesFromFileName($objFile->getClientOriginalName(), 'pan'));
            $destFilePath = $destDirPath . $fileNamefrontfile;
            uploadFile($objFile, $destFilePath);

            $data['pan_file_name'] = $fileNamefrontfile;
            $data['pan_status'] = 0;
            $data['pan_no'] = $documentnumber;
        } else if ($selectdocument == 2) {
            $url = "/KYC/aadhaar/";
            $destDirPath = Config::get('constants.FILE_UPLOAD_PATH') . "/" . $user_id . $url;
            if ($getUserKYCDetail['aadhar_file_name'] != "") {
                deleteFile($destDirPath, true);
            }
            $fileNamefrontfile = strtolower(removeSpacesFromFileName($objFile->getClientOriginalName(), 'aadhar'));
            $destFilePath = $destDirPath . $fileNamefrontfile;
            uploadFile($objFile, $destFilePath);
            $data['aadhar_file_name'] = $fileNamefrontfile;
            $data['aadhar_status'] = 0;
            $data['aadhar_no'] = $documentnumber;
            if ($request->hasFile('backfile')) {
                $objFile = $request->file('backfile');
                $fileNamebackfile = strtolower(removeSpacesFromFileName($objFile->getClientOriginalName(), 'aadhaar_back'));
                $destFilePath = $destDirPath . $fileNamebackfile;
                uploadFile($objFile, $destFilePath);
                $data['aadhar_back_file_name'] = $fileNamebackfile;
            }
        } else if ($selectdocument == 3) {
            $url = "/KYC/voter/";
            $destDirPath = Config::get('constants.FILE_UPLOAD_PATH') . "/" . $user_id . $url;
            if ($getUserKYCDetail['voting_front_file_name'] != "") {
                deleteFile($destDirPath, true);
            }
            $fileNamefrontfile = strtolower(removeSpacesFromFileName($objFile->getClientOriginalName(), 'voter_front'));
            $destFilePath = $destDirPath . $fileNamefrontfile;
            uploadFile($objFile, $destFilePath);
            $data['voting_front_file_name'] = $fileNamefrontfile;
            $data['voting_status'] = 0;
            $data['voting_no'] = $documentnumber;
            if ($request->hasFile('backfile')) {
                $objFile = $request->file('backfile');
                $fileNamebackfile = strtolower(removeSpacesFromFileName($objFile->getClientOriginalName(), 'voterback'));
                $destFilePath = $destDirPath . $fileNamebackfile;
                uploadFile($objFile, $destFilePath);
                $data['voting_back_file_name'] = $fileNamebackfile;
            }
        } else if ($selectdocument == 4) {
            $url = "/KYC/gst/";
            $destDirPath = Config::get('constants.FILE_UPLOAD_PATH') . "/" . $user_id . $url;
            if ($getUserKYCDetail['gst_file_name'] != "") {
                deleteFile($destDirPath, true);
            }
            $fileNamefrontfile = strtolower(removeSpacesFromFileName($objFile->getClientOriginalName(), 'gst'));
            $destFilePath = $destDirPath . $fileNamefrontfile;
            uploadFile($objFile, $destFilePath);
            $data['gst_file_name'] = $fileNamefrontfile;
            $data['gst_status'] = 0;
            $data['gst_no'] = $documentnumber;
        } else if ($selectdocument == 5) {
            $url = "/KYC/drivinglicense/";
            $destDirPath = Config::get('constants.FILE_UPLOAD_PATH') . "/" . $user_id . $url;
            if ($getUserKYCDetail['driving_license_file_name'] != "") {
                deleteFile($destDirPath, true);
            }
            $fileNamefrontfile = strtolower(removeSpacesFromFileName($objFile->getClientOriginalName(), 'drivinglicense'));
            $destFilePath = $destDirPath . $fileNamefrontfile;
            uploadFile($objFile, $destFilePath);
            $data['driving_license_file_name'] = $fileNamefrontfile;
            $data['driving_status'] = 0;
            $data['driving_license_no'] = $documentnumber;
            if ($request->hasFile('backfile')) {
                $objFile = $request->file('backfile');
                $fileNamebackfile = strtolower(removeSpacesFromFileName($objFile->getClientOriginalName(), 'drivinglicenseback'));
                $destFilePath = $destDirPath . $fileNamebackfile;
                uploadFile($objFile, $destFilePath);
                $data['driving_license_back_file_name'] = $fileNamebackfile;
            }
        } else if ($selectdocument == 6) {
            $url = "/KYC/cancelledcheck/";
            $destDirPath = Config::get('constants.FILE_UPLOAD_PATH') . "/" . $user_id . $url;
            if ($getUserKYCDetail['cancelled_check_file_name'] != "") {
                deleteFile($destDirPath, true);
            }
            $fileNamefrontfile = strtolower(removeSpacesFromFileName($objFile->getClientOriginalName(), 'cancelledcheck'));
            $destFilePath = $destDirPath . $fileNamefrontfile;
            uploadFile($objFile, $destFilePath);

            $data['cancelled_check_file_name'] = $fileNamefrontfile;
            $data['cheque_status'] = 0;
            $data['cancelled_check_no'] = $documentnumber;
        } else {
            $url = "/KYC/certificate/";
            $destDirPath = Config::get('constants.FILE_UPLOAD_PATH') . "/" . $user_id . $url;
            if ($getUserKYCDetail['certificate_file_name'] != "") {
                deleteFile($destDirPath, true);
            }
            $fileNamefrontfile = strtolower(removeSpacesFromFileName($objFile->getClientOriginalName(), 'certificate'));
            $destFilePath = $destDirPath . $fileNamefrontfile;
            uploadFile($objFile, $destFilePath);
            $data['certificate_file_name'] = $fileNamefrontfile;
            $data['certificate_status'] = 0;
            $data['certificate_no'] = $documentnumber;
        }

        $data = (array) $data;
        UserKYCDetail::where('user_id', $user_id)->update($data);
    }
}
if (!function_exists('prepareKYCChangeLog')) {

    function prepareKYCChangeLog($status, $text, $user_id)
    {
        $Arr = [];
        if ($status == 1) {
            array_push($Arr, [
                'fieldname' => "KYC Status", 'newvalue' => "Approved",
                'oldvalue' => "", 'user_id' => $user_id,
                'done' => Auth::user()->username,
                'created_at' => date('Y-m-d H:i:s')
            ]);
        } else {
            array_push($Arr, [
                'fieldname' => "KYC Status", 'newvalue' => "Rejected",
                'oldvalue' => "", 'user_id' => $user_id,
                'done' => Auth::user()->username,
                'created_at' => date('Y-m-d H:i:s')
            ]);
            array_push($Arr, [
                'fieldname' => "KYC Rejected Reason", 'newvalue' => $text,
                'oldvalue' => "", 'user_id' => $user_id,
                'done' => Auth::user()->username,
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }
        return $Arr;
    }
}

if (!function_exists('preparePOSTChangeLog')) {

    function preparePOSTChangeLog($status, $text, $request)
    {
        $Arr = [];
        $id = $request->input('post_id');
        $postType = $request->input('posttype');
        if ($status == 1) {
            array_push($Arr, [
                'fieldname' => "Post Status", 'newvalue' => "Approved",
                'oldvalue' => "",
                'post_id' => $id,
                'post_type' => $postType,
                'done' => Auth::user()->username,
                'created_at' => date('Y-m-d H:i:s')
            ]);
        } else {
            array_push($Arr, [
                'fieldname' => "Post Status", 'newvalue' => "Rejected",
                'oldvalue' => "",
                'post_id' => $id,
                'post_type' => $postType,
                'done' => Auth::user()->username,
                'created_at' => date('Y-m-d H:i:s')
            ]);
            $rejecttitle = PostRejectedStatus::where('id', $text)->first();
            if ($rejecttitle) {
                $reject_message = $rejecttitle['reason'];
            } else {
                $reject_message = "";
            }
            array_push($Arr, [
                'fieldname' => "Post Rejected Reason", 'newvalue' => $reject_message,
                'oldvalue' => "",
                'post_id' => $id,
                'post_type' => $postType,
                'done' => Auth::user()->username,
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }
        return $Arr;
    }
}

if (!function_exists('prepareKYCDocument')) {

    function prepareKYCDocument($request)
    {
        $role_id = Auth::user()->role_id;
        $role_type = Auth::user()->role_type;

        $criteriaId = $request->input('criteriaId');
        $PermissionDetail = getPermissionAndAccess($criteriaId, $role_id);
        $is_kycupdate = $PermissionDetail[0]['is_kycupdate'];
        $isKYCUpdate = 1;
        if ($is_kycupdate == 0) {
            $isKYCUpdate = 0;
        }
        $id = $request->input('id');
        $html = "";
        $getData = UserKYCDetail::where('user_id', $id)->first();
        if ($getData) {
            $pan_no = $getData['pan_no'];
            if ($pan_no != "" || $pan_no != null) {
                $html .= '<tr><td>PAN Card</td><td>' . $getData['pan_no'] . '</td>';
                if ($getData['pan_file_name'] != "") {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td ><button id="pan_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>
                            <a href="/kyc/download/pan/' . $getData['user_id'] . '/front" id="pan_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="Download" class=" btn btn-primary btn-icon submit"> <i  style="position:relative;top:7px;" data-feather="download"></i></a>
                            </td>';
                    } else {
                        $html .= '<td  class="pan_' . $getData['user_id'] . '"><button id="pan_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>
                            </td>';
                    }
                } else {
                    $html .= ' <td>
                                </td>';
                }
                $html .= ' <td>
                                </td>';
                if ($getData['pan_status'] == 0) {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td class="pan_' . $getData['user_id'] . '"><button id="pan_' . $getData['user_id'] . '_1" type="button" data-tooltip="tooltip" data-placement="top"
                                        title="Approve"
                                        class="approve btn btn-primary btn-icon submit"> <i data-feather="thumbs-up"></i></button>
                                <button id="pan_' . $getData['user_id'] . '_2" type="button" data-tooltip="tooltip" data-placement="top"
                                    title="Rejected" class="reject btn btn-danger btn-icon submit"> <i
                                        data-feather="thumbs-down"></i></button></td>';
                    } else {
                        '<td></td>';
                    }
                } else if ($getData['pan_status'] == 1) {

                    if ($role_type  == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                        $html .= '<td class="pan_' . $getData['user_id'] . '"><span class="badge badge-success">Approved</span><br><br><button id="pan_' . $getData['user_id'] . '_2" type="button" data-tooltip="tooltip" data-placement="top"
                                    title="Rejected" class="reject btn btn-danger btn-icon submit"> <i
                                        data-feather="thumbs-down"></i></button></td>';
                    } else {
                        $html .= '<td><span class="badge badge-success">Approved</span></td>';
                    }
                } else {

                    if ($role_type  == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                        $html .= '<td class="pan_' . $getData['user_id'] . '"><button id="pan_' . $getData['user_id'] . '_1" type="button" data-tooltip="tooltip" data-placement="top"
                                        title="Approve"
                                        class="approve btn btn-primary btn-icon submit"> <i data-feather="thumbs-up"></i></button><br><br><span class="badge badge-danger">Rejected</span></td>';
                    } else {
                        $html .= '<td><span class="badge badge-danger">Rejected</span></td>';
                    }
                }
                $html .= '</tr>';
            }
            $aadhar_no = $getData['aadhar_no'];
            if ($aadhar_no != "" || $aadhar_no != null) {
                $html .= '<tr><td>Aadhar Card</td><td>' . $getData['aadhar_no'] . '</td>';
                if ($getData['aadhar_file_name'] != "") {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td>
                            <button  id="aadhar_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>
                            <a  href="/kyc/download/aadhar/' . $getData['user_id'] . '/front"  id="aadhar_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="Download" class=" btn btn-primary btn-icon submit"> <i  style="position:relative;top:7px;" data-feather="download"></i></a>
                            </td>';
                    } else {
                        $html .= '<td>
                            <button  id="aadhar_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button></td>';
                    }
                } else {
                    $html .= ' <td>
                                </td>';
                }
                if ($getData['aadhar_back_file_name'] != "") {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td>
                            <button  id="aadhar_' . $getData['user_id'] . '_back" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>
                            <a  href="/kyc/download/aadhar/' . $getData['user_id'] . '/back" id="aadhar_' . $getData['user_id'] . '_back" data-tooltip="tooltip" data-placement="top"
                            title="Download" class=" btn btn-primary btn-icon submit"> <i  style="position:relative;top:7px;" data-feather="download"></i></a>
                            </td>';
                    } else {
                        $html .= '<td>
                            <button  id="aadhar_' . $getData['user_id'] . '_back" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>
                            </td>';
                    }
                } else {
                    $html .= '<td>
                                </td>';
                }
                if ($getData['aadhar_status'] == 0) {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td class="aadhar_' . $getData['user_id'] . '"><button id="aadhar_' . $getData['user_id'] . '_1" type="button" data-tooltip="tooltip" data-placement="top"
                                        title="Approve"
                                        class="approve btn btn-primary btn-icon submit"> <i data-feather="thumbs-up"></i></button>
                                <button  id="aadhar_' . $getData['user_id'] . '_2" type="button" data-tooltip="tooltip" data-placement="top"
                                    title="Rejected" class="reject btn btn-danger btn-icon submit"> <i
                                        data-feather="thumbs-down"></i></button></td>';
                    } else {
                        $html .= '<td></td>';
                    }
                } else if ($getData['aadhar_status'] == 1) {

                    if ($role_type  == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                        $html .= '<td class="aadhar_' . $getData['user_id'] . '"><span class="badge badge-success">Approved</span><br><br><button  id="aadhar_' . $getData['user_id'] . '_2" type="button" data-tooltip="tooltip" data-placement="top"
                                    title="Rejected" class="reject btn btn-danger btn-icon submit"> <i
                                        data-feather="thumbs-down"></i></button></td>';
                    } else {
                        $html .= '<td><span class="badge badge-success">Approved</span></td>';
                    }
                } else {

                    if ($role_type  == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                        $html .= '<td  class="aadhar_' . $getData['user_id'] . '"><button id="aadhar_' . $getData['user_id'] . '_1" type="button" data-tooltip="tooltip" data-placement="top"
                                        title="Approve"
                                        class="approve btn btn-primary btn-icon submit"> <i data-feather="thumbs-up"></i></button><br><br><span class="badge badge-danger">Rejected</span></td>';
                    } else {
                        $html .= '<td  class="aadhar' . $getData['user_id'] . '"><span class="badge badge-danger">Rejected</span></td>';
                    }
                }
                $html .= '<tr>';
            }
            $voting_no = $getData['voting_no'];
            if ($voting_no != "" || $voting_no != null) {

                $html .= '<tr><td>Voter ID</td><td>' . $getData['voting_no'] . '</td>';

                if ($getData['voting_front_file_name'] != "") {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td>
                            <button  id="voting_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>
                            <a  href="/kyc/download/voting/' . $getData['user_id'] . '/front" id="voting_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="Download" class=" btn btn-primary btn-icon submit"> <i   style="position:relative;top:7px;" data-feather="download"></i></a>
                            </td>';
                    } else {
                        $html .= '<td>
                            <button  id="voting_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>

                            </td>';
                    }
                } else {
                    $html .= ' <td>
                                </td>';
                }
                if ($getData['voting_back_file_name'] != "") {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td>
                            <button id="voting_' . $getData['user_id'] . '_back" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>
                            <a  href="/kyc/download/voting/' . $getData['user_id'] . '/back" id="voting_' . $getData['user_id'] . '_back" data-tooltip="tooltip" data-placement="top"
                            title="Download" class=" btn btn-primary btn-icon submit"> <i  style="position:relative;top:7px;" data-feather="download"></i></a>
                            </td>';
                    } else {
                        $html .= '<td>
                            <button id="voting_' . $getData['user_id'] . '_back" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>

                            </td>';
                    }
                } else {
                    $html .= '<td>
                                </td>';
                }
                if ($getData['voting_status'] == 0) {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td class="voting_' . $getData['user_id'] . '"><button id="voting_' . $getData['user_id'] . '_1" type="button" data-tooltip="tooltip" data-placement="top"
                                        title="Approve"
                                        class="approve btn btn-primary btn-icon submit"> <i data-feather="thumbs-up"></i></button>
                                <button  id="voting_' . $getData['user_id'] . '_2" type="button" data-tooltip="tooltip" data-placement="top"
                                    title="Rejected" class="reject btn btn-danger btn-icon submit"> <i
                                        data-feather="thumbs-down"></i></button></td>';
                    } else {
                        $html .= "<td></td>";
                    }
                } else if ($getData['voting_status'] == 1) {
                    if ($role_type  != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                        $html .= '<td><span class="badge badge-success">Approved</span></td>';
                    } else {
                        $html .= '<td class="voting_' . $getData['user_id'] . '"><span class="badge badge-success">Approved</span> <br><br><br><button  id="voting_' . $getData['user_id'] . '_2" type="button" data-tooltip="tooltip" data-placement="top"
                                    title="Rejected" class="reject btn btn-danger btn-icon submit"> <i
                                        data-feather="thumbs-down"></i></button></td>';
                    }
                } else {
                    if ($role_type  != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                        $html .= '<td><span class="badge badge-danger">Rejected</span></td>';
                    } else {
                        $html .= '<td class="voting_' . $getData['user_id'] . '"><button id="voting_' . $getData['user_id'] . '_1" type="button" data-tooltip="tooltip" data-placement="top"
                                        title="Approve"
                                        class="approve btn btn-primary btn-icon submit"> <i data-feather="thumbs-up"></i></button><br><br><span class="badge badge-danger">Rejected</span></td>';
                    }
                }
                $html .= '<tr>';
            }
            $driving_license_no = $getData['driving_license_no'];
            if ($driving_license_no != "" || $driving_license_no != null) {
                $html .= '<tr><td>Driving License</td><td>' . $getData['driving_license_no'] . '</td>';
                if ($getData['driving_license_file_name'] != "") {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td>
                            <button id="drivinglicense_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>
                            <a   href="/kyc/download/drivinglicense/' . $getData['user_id'] . '/front" id="drivinglicense_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="Download" class=" btn btn-primary btn-icon submit"> <i  style="position:relative;top:7px;" data-feather="download"></i></a>
                            </td>';
                    } else {
                        $html .= '<td>
                            <button id="drivinglicense_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>

                            </td>';
                    }
                } else {
                    $html .= ' <td>
                                </td>';
                }
                if ($getData['driving_license_back_file_name'] != "") {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td>
                            <button id="drivinglicense_' . $getData['user_id'] . '_back" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>
                            <a  href="/kyc/download/drivinglicense/' . $getData['user_id'] . '/back" id="drivinglicense_' . $getData['user_id'] . '_back" data-tooltip="tooltip" data-placement="top"
                            title="Download" class=" btn btn-primary btn-icon submit"> <i  style="position:relative;top:7px;" data-feather="download"></i></a>
                            </td>';
                    } else {
                        $html .= '<td>
                            <button id="drivinglicense_' . $getData['user_id'] . '_back" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>

                            </td>';
                    }
                } else {
                    $html .= '<td>
                                </td>';
                }
                if ($getData['driving_status'] == 0) {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td class="drivinglicense_' . $getData['user_id'] . '" ><button id="drivinglicense_' . $getData['user_id'] . '_1" type="button" data-tooltip="tooltip" data-placement="top"
                                        title="Approve"
                                        class="approve btn btn-primary btn-icon submit"> <i data-feather="thumbs-up"></i></button>
                                <button  id="drivinglicense_' . $getData['user_id'] . '_2" type="button" data-tooltip="tooltip" data-placement="top"
                                    title="Rejected" class="reject btn btn-danger btn-icon submit"> <i
                                        data-feather="thumbs-down"></i></button></td>';
                    } else {
                        $html .= "<td></td>";
                    }
                } else if ($getData['driving_status'] == 1) {
                    if ($role_type  == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                        $html .= '<td class="drivinglicense_' . $getData['user_id'] . '"><span class="badge badge-success">Approved</span><br><br><button  id="drivinglicense_' . $getData['user_id'] . '_2" type="button" data-tooltip="tooltip" data-placement="top"
                                    title="Rejected" class="reject btn btn-danger btn-icon submit"> <i
                                        data-feather="thumbs-down"></i></button></td>';
                    } else {
                        $html .= '<td><span class="badge badge-success">Approved</span></td>';
                    }
                } else {
                    if ($role_type  == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                        $html .= '<td class="drivinglicense_' . $getData['user_id'] . '"><button id="drivinglicense_' . $getData['user_id'] . '_1" type="button" data-tooltip="tooltip" data-placement="top"
                                        title="Approve"
                                        class="approve btn btn-primary btn-icon submit"> <i data-feather="thumbs-up"></i></button><br><br><span class="badge badge-danger">Rejected</span></td>';
                    } else {
                        $html .= '<td><span class="badge badge-danger">Rejected</span></td>';
                    }
                }
                $html .= '<tr>';
            }
            if ($getData['certificate_file_name'] != "") {
                $html .= '<tr><td>Certificate</td><td></td>';
                if ($getData['certificate_file_name'] != "") {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td>
                            <button id="certificate_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>
                            <a  href="/kyc/download/certificate/' . $getData['user_id'] . '/front"  id="certificate_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="Download" class=" btn btn-primary btn-icon submit"> <i  style="position:relative;top:7px;" data-feather="download"></i></a>
                            </td>';
                    } else {
                        $html .= '<td>
                            <button id="certificate_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>

                            </td>';
                    }
                } else {
                    $html .= ' <td>
                                </td>';
                }

                $html .= '<td>
                                </td>';

                if ($getData['certificate_status'] == 0) {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td class="certificate_' . $getData['user_id'] . '"><button id="certificate_' . $getData['user_id'] . '_1" type="button" data-tooltip="tooltip" data-placement="top"
                                        title="Approve"
                                        class="approve btn btn-primary btn-icon submit"> <i data-feather="thumbs-up"></i></button>
                                <button  id="certificate_' . $getData['user_id'] . '_2" type="button" data-tooltip="tooltip" data-placement="top"
                                    title="Rejected" class="reject btn btn-danger btn-icon submit"> <i
                                        data-feather="thumbs-down"></i></button></td>';
                    } else {
                        $html .= '<td></td>';
                    }
                } else if ($getData['certificate_status'] == 1) {
                    if ($role_type  != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                        $html .= '<td><span class="badge badge-success">Approved</span></td>';
                    } else {
                        $html .= '<td><span class="badge badge-success">Approved</span><br><br><button  id="certificate_' . $getData['user_id'] . '_2" type="button" data-tooltip="tooltip" data-placement="top"
                                    title="Rejected" class="reject btn btn-danger btn-icon submit"> <i
                                        data-feather="thumbs-down"></i></button></td>';
                    }
                } else {
                    if ($role_type  != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                        $html .= '<td><span class="badge badge-danger"><button id="certificate_' . $getData['user_id'] . '_1" type="button" data-tooltip="tooltip" data-placement="top"
                                        title="Approve"
                                        class="approve btn btn-primary btn-icon submit"> <i data-feather="thumbs-up"></i></button><br><br><span>Rejected</span></td>';
                    } else {
                        $html .= '<td><span class="badge badge-danger">Rejected</span></td>';
                    }
                }
                $html .= '<tr>';
            }
            $gst_no = $getData['gst_no'];
            if ($gst_no != "" || $gst_no != null) {

                $html .= '<tr><td>GST</td><td>' . $getData['gst_no'] . '</td>';
                if ($getData['gst_file_name'] != "") {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td>
                            <button id="gst_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>
                            <a  href="/kyc/download/gst/' . $getData['user_id'] . '/front"  id="gst_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="Download" class="  btn btn-primary btn-icon submit"> <i  style="position:relative;top:7px;" data-feather="download"></i></a>
                            </td>';
                    } else {
                        $html .= '<td>
                            <button id="gst_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>

                            </td>';
                    }
                } else {
                    $html .= ' <td>
                                </td>';
                }

                $html .= '<td>
                                </td>';

                if ($getData['gst_status'] == 0) {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td class="gst_' . $getData['user_id'] . '"><button id="gst_' . $getData['user_id'] . '_1" type="button" data-tooltip="tooltip" data-placement="top"
                                        title="Approve"
                                        class="approve btn btn-primary btn-icon submit"> <i data-feather="thumbs-up"></i></button>
                                <button  id="gst_' . $getData['user_id'] . '_2" type="button" data-tooltip="tooltip" data-placement="top"
                                    title="Rejected" class="reject btn btn-danger btn-icon submit"> <i data-feather="thumbs-down"></i></button></td>';
                    } else {
                        $html .= "<td></td>";
                    }
                } else if ($getData['gst_status'] == 1) {
                    if ($role_type  != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                        $html .= '<td class="gst_' . $getData['user_id'] . '"><span class="badge badge-success">Approved</span></td>';
                    } else {
                        $html .= '<td><span class="badge badge-success">Approved</span><br><br><button  id="gst_' . $getData['user_id'] . '_2" type="button" data-tooltip="tooltip" data-placement="top"
                                    title="Rejected" class="reject btn btn-danger btn-icon submit"> <i data-feather="thumbs-down"></i></button></td>';
                    }
                } else {
                    if ($role_type  != Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                        $html .= '<td class="gst_' . $getData['user_id'] . '"><button id="gst_' . $getData['user_id'] . '_1" type="button" data-tooltip="tooltip" data-placement="top"
                                        title="Approve"
                                        class="approve btn btn-primary btn-icon submit"> <i data-feather="thumbs-up"></i></button><br><br><span class="badge badge-danger">Rejected</span></td>';
                    } else {
                        $html .= '<td><span class="badge badge-danger">Rejected</span></td>';
                    }
                }
                $html .= '<tr>';
            }
            if ($getData['cancelled_check_file_name'] != "") {
                $html .= '<tr><td>Cancelled Cheque</td><td></td>';
                if ($getData['cancelled_check_file_name'] != "") {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td>
                            <button id="cheque_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>
                            <a  href="/kyc/download/cheque/' . $getData['user_id'] . '/front" id="cheque_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="Download" class=" btn btn-primary btn-icon submit"> <i  style="position:relative;top:7px;" data-feather="download"></i></a>
                            </td>';
                    } else {
                        $html .= '<td>
                            <button id="cheque_' . $getData['user_id'] . '_front" data-tooltip="tooltip" data-placement="top"
                            title="view"
                            class="view btn btn-primary btn-icon submit"> <i  data-feather="eye"></i></button>

                            </td>';
                    }
                } else {
                    $html .= ' <td>
                                </td>';
                }

                $html .= '<td>
                                </td>';

                if ($getData['cheque_status'] == 0) {
                    if ($isKYCUpdate == 1) {
                        $html .= '<td class="cheque_' . $getData['user_id'] . '"><button id="cheque_' . $getData['user_id'] . '_1" type="button" data-tooltip="tooltip" data-placement="top"
                                        title="Approve"
                                        class="approve btn btn-primary btn-icon submit"> <i data-feather="thumbs-up"></i></button>
                                <button  id="cheque_' . $getData['user_id'] . '_2" type="button" data-tooltip="tooltip" data-placement="top"
                                    title="Rejected" class="reject btn btn-danger btn-icon submit"> <i data-feather="thumbs-down"></i></button></td>';
                    } else {
                        $html .= '<td></td>';
                    }
                } else if ($getData['cheque_status'] == 1) {
                    if ($role_type  == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                        $html .= '<td class="cheque_' . $getData['user_id'] . '"><span class="badge badge-success">Approved</span><br><br><button  id="cheque_' . $getData['user_id'] . '_2" type="button" data-tooltip="tooltip" data-placement="top"
                                    title="Rejected" class="reject btn btn-danger btn-icon submit"> <i data-feather="thumbs-down"></i></button></td>';
                    } else {
                        $html .= '<td><span class="badge badge-success">Approved</span></td>';
                    }
                } else {
                    if ($role_type  == Config::get('constants.ROLE_TYPE.BACKEND_OPS_TEAM')) {
                        $html .= '<td class="cheque_' . $getData['user_id'] . '"><button id="cheque_' . $getData['user_id'] . '_1" type="button" data-tooltip="tooltip" data-placement="top"
                                        title="Approve"
                                        class="approve btn btn-primary btn-icon submit"> <i data-feather="thumbs-up"></i></button><br><br><span class="badge badge-danger">Rejected</span></td>';
                    } else {
                        $html .= '<td><span class="badge badge-danger">Rejected</span></td>';
                    }
                }
                $html .= '<tr>';
            }
        }
        return $html;
    }
}
if (!function_exists('prepareKYCDocumentApproveReject')) {

    function prepareKYCDocumentApproveReject($request)
    {
        $id = $request->input('id');
        $explodeId = explode("_", $id);
        $user_id = $explodeId[1];
        $type = $explodeId[0];
        $status = $explodeId[2];
        $arr = [];
        if ($type == "pan") {
            $arr['pan_status'] = $status;
        } else if ($type == "aadhar") {
            $arr['aadhar_status'] = $status;
        } else if ($type == "voting") {
            $arr['voting_status'] = $status;
        } else if ($type == "gst") {
            $arr['gst_status'] = $status;
        } else if ($type == "certificate") {
            $arr['certificate_status'] = $status;
        } else if ($type == "drivinglicense") {
            $arr['driving_status'] = $status;
        } else if ($type == "cheque") {
            $arr['cheque_status'] = $status;
        }
        UserKYCDetail::where('user_id', $user_id)->update((array)$arr);
    }
}
if (!function_exists('downloadKYCDoc')) {

    function downloadKYCDoc($id)
    {
        $explodeId = explode("_", $id);
        $user_id = $explodeId[1];
        $type = $explodeId[0];
        $filetype = $explodeId[2];
        $arr = [];
        $getUserKYCDetail = UserKYCDetail::where('user_id', $user_id)->first();
        if ($type == 'pan') {
            $url = "/KYC/pan/";
            $destDirPath = Config::get('constants.FILE_UPLOAD_PATH') . "/" . $user_id . $url;
            $fileName = $getUserKYCDetail['pan_file_name'];
            $documentNo = $getUserKYCDetail['pan_no'];
            $destFilePath = $destDirPath . $fileName;
        } else if ($type == 'aadhar') {
            $url = "/KYC/aadhaar/";
            $destDirPath = Config::get('constants.FILE_UPLOAD_PATH') . "/" . $user_id . $url;
            if ($filetype == "front") {
                $fileName = $getUserKYCDetail['aadhar_file_name'];
            } else {
                $fileName = $getUserKYCDetail['aadhar_back_file_name'];
            }
            $destFilePath = $destDirPath . $fileName;
            $documentNo = $getUserKYCDetail['aadhar_no'];
        } else if ($type == 'voting') {
            $url = "/KYC/voter/";
            $destDirPath = Config::get('constants.FILE_UPLOAD_PATH') . "/" . $user_id . $url;
            if ($filetype == "front") {
                $fileName = $getUserKYCDetail['voting_front_file_name'];
            } else {
                $fileName = $getUserKYCDetail['voting_back_file_name'];
            }
            $destFilePath = $destDirPath . $fileName;
            $documentNo = $getUserKYCDetail['voting_no'];
        } else if ($type == 'gst') {
            $url = "/KYC/gst/";
            $destDirPath = Config::get('constants.FILE_UPLOAD_PATH') . "/" . $user_id . $url;

            $fileName = $getUserKYCDetail['gst_file_name'];

            $destFilePath = $destDirPath . $fileName;
            $documentNo = $getUserKYCDetail['gst_no'];
        } else if ($type == 'drivinglicense') {
            $url = "/KYC/drivinglicense/";
            $destDirPath = Config::get('constants.FILE_UPLOAD_PATH') . "/" . $user_id . $url;
            if ($filetype == "front") {
                $fileName = $getUserKYCDetail['driving_license_file_name'];
            } else {
                $fileName = $getUserKYCDetail['driving_license_back_file_name'];
            }
            $destFilePath = $destDirPath . $fileName;
            $documentNo = $getUserKYCDetail['driving_license_no'];
        } else if ($type == 'cheque') {
            $url = "/KYC/cancelledcheck/";
            $destDirPath = Config::get('constants.FILE_UPLOAD_PATH') . "/" . $user_id . $url;
            $fileName = $getUserKYCDetail['cancelled_check_file_name'];
            $destFilePath = $destDirPath . $fileName;
            $documentNo = $getUserKYCDetail['cancelled_check_no'];
        } else {
            $url = "/KYC/certificate/";
            $destDirPath = Config::get('constants.FILE_UPLOAD_PATH') . "/" . $user_id . $url;
            $fileName = $getUserKYCDetail['certificate_check_file_name'];
            $destFilePath = $destDirPath . $fileName;
            $documentNo = $getUserKYCDetail['certificate_no'];
        }
        $fileURL = getFileURLPublicPath($destFilePath);
        $Arr['fileURL'] = $fileURL;
        $Arr['fileName'] = $fileName;
        $Arr['documentNo'] = $documentNo;
        return $Arr;
    }
}
if (!function_exists('getPermissionAndAccess')) {

    function getPermissionAndAccess($permissionId, $role_id)
    {

        $query = Roles::query();
        $query = $query->select('role_permission_mapping.*', 'permission.permission_name', 'permission.icon', 'permission.permission_path')
            ->leftJoin('role_permission_mapping', 'roles.role_id', "=", 'role_permission_mapping.role_id')
            ->leftJoin('permission', 'role_permission_mapping.permission_id', "=", 'permission.permission_id')
            ->where('role_permission_mapping.is_access', '!=', 0)
            ->where('roles.role_id', $role_id)
            ->orderBy('permission.display_order', 'ASC');
        if ($permissionId != 0) {
            $query = $query->where('role_permission_mapping.permission_id', $permissionId);
        }
        $query = $query->get();
        return $query;
    }
}
if (!function_exists('getCallStatus')) {
    function getCallStatus($getStatus)
    {
        $getText = CallStatus::where('id', $getStatus['status'])->first();

        if ($getStatus['status'] == Config::get('constants.CALL_STATUS.CUSTOMER_NOT_AVAILABLE')) {
            return "<span class='badge badge-danger'>" . $getText['title'] . "</span>";
        } else if ($getStatus['status'] == Config::get('constants.CALL_STATUS.FOLLOW_UP')) {
            return "<span class='badge badge-warning'>" . $getText['title'] . "</span>";
        } else if ($getStatus['status'] == Config::get('constants.CALL_STATUS.CLOSED')) {
            return "<span class='badge badge-success'>" . $getText['title'] . "</span>";
        } else {
            if ($getText) {
                return "<span class='badge badge-info'>" . $getText['title'] . "</span>";
            } else {
                return '';
            }
        }
    }
}
if (!function_exists('updateKYCstatus')) {

    function updateKYCstatus($request)
    {
        $status = $request->input('kycstatus');
        $Arr = [];
        $Arr['is_kyc_approved'] = $status;
        $Arr['kyc_rejcted_reason'] = "";
        $Arr['kyc_approved_rejected_date'] = date('Y-m-d H:i:s');
        $KYCArr = [];
        $text = "";
        if ($status == 2) {
            $pancardreason = $request->input('pan_reject_reason');
            if ($pancardreason) {
                $KYCArr['pan_reject_reason'] = $pancardreason;
                $text .= "Pan Card - " . $pancardreason . ',';
            }
            $votingreason = $request->input('voting_reject_reason');
            if ($votingreason) {
                $KYCArr['voting_reject_reason'] = $votingreason;
                $text .= "Voter ID - " . $votingreason . ',';
            }
            $aadharreason = $request->input('aadhar_reject_reason');
            if ($aadharreason) {
                $KYCArr['aadhar_reject_reason'] = $aadharreason;
                $text .= "Aadhar Card - " . $aadharreason . ',';
            }
            $certificate = $request->input('certificate_reject_reason');
            if ($certificate) {
                $KYCArr['certificate_reject_reason'] = $certificate;
                $text .= "Certificate - " . $certificate . ',';
            }
            $gstreason = $request->input('gst_reject_reason');
            if ($gstreason) {
                $KYCArr['gst_reject_reason'] = $gstreason;
                $text .= "GST - " . $gstreason . ',';
            }
            $cheque = $request->input('cheque_reject_reason');
            if ($cheque) {
                $KYCArr['cheque_reject_reason'] = $cheque;
                $text .= "Cheque - " . $cheque . ',';
            }
            $driving = $request->input('driving_reject_reason');
            if ($driving) {
                $KYCArr['driving_reject_reason'] = $driving;
                $text .= "Driving License - " . $driving . ',';
            }
            $text = substr($text, 0, -1);
            $Arr['kyc_rejcted_reason'] = $text;
        }
        $user_id = $request->input('user_id');
        $getUserData = User::where('id', $user_id)->first();
        $usertype = $getUserData['user_type'];
        if ($usertype != "C") {
            $Arr['is_kyc_updated'] = 1;
        }
        User::where('id', $user_id)->update($Arr);
        UserKYCDetail::where('user_id', $user_id)->update($KYCArr);
        return $text;
    }
}


if (!function_exists('updatepoststatus')) {

    function updatepoststatus($request)
    {
        $status = $request->input('poststatus');
        $rejectreason = $request->input('poststatusactionreason');
        $id = $request->input('post_id');
        $Arr = [];
        $postType = $request->input('posttype');
        if ($postType == "PR") {

            if ($status == 2) {
                $Arr['is_project_approved'] = 2;
                $Arr['reject_message'] = $rejectreason;
                $Arr['is_published'] = 0;
                $Arr['project_status'] = 0;
            } else {
                $Arr['is_project_approved'] = 1;
                $Arr['reject_message'] = 0;
                $Arr['is_published'] = 1;
                $Arr['project_status'] = 1;
            }
            $Arr['published_date'] = date('Y-m-d H:i:s');
            $Arr['is_send_for_approve_request'] = 0;
            $update = Project::where('id', $id)->first();
            $update = $update->update($Arr);
        } else if ($postType == "P") {
            $Arr['published_date'] = date('Y-m-d H:i:s');
            if ($status == 2) {
                $Arr['is_property_approved'] = 2;
                $Arr['reject_message'] = $rejectreason;
                $Arr['is_published'] = 0;
                $Arr['property_status'] = 0;
            } else {
                $Arr['is_property_approved'] = 1;
                $Arr['reject_message'] = 0;
                $Arr['is_published'] = 1;
                $Arr['property_status'] = 1;
            }
            // $Arr['published_date'] = date('Y-m-d H:i:s');
            $Arr['is_send_for_approve_request'] = 0;
            // Property::where('id', $id)->update($Arr);
            $update = Property::where('id', $id)->first();
            $update = $update->update($Arr);
        } else {
            $Arr['published_date'] = date('Y-m-d H:i:s');
            if ($status == 2) {
                $Arr['is_service_approved'] = 2;
                $Arr['reject_message'] = $rejectreason;
                $Arr['is_published'] = 0;
                $Arr['service_status'] = 0;
            } else {
                $Arr['is_service_approved'] = 1;
                $Arr['reject_message'] = 0;
                $Arr['is_published'] = 1;
                $Arr['service_status'] = 1;
            }
            $Arr['is_send_for_approve_request'] = 0;

            $update = Service::where('id', $id)->first();
            // echo json_encode($Arr);
            // echo $id;
            // exit;
            // exit;
            $update = $update->update($Arr);
        }
        return $rejectreason;
    }
}
if (!function_exists('refreshESIndex')) {
    function refreshESIndex($id)
    {
        $AWSElasticSearch = new AWSElasticSearch();
        $serviceElastic = ServiceService::where('id', $id)->first();
        //Check service index already exists
        // return
        $isExists = $AWSElasticSearch->checkIndexExists($serviceElastic);
        return $isExists;
        if ($isExists) {
            $AWSElasticSearch->deleteIndexDoc($serviceElastic, 'S');
        }
        // if ($serviceElastic->is_service_approved == 1 && $serviceElastic->is_published == 1) {
        // $AWSElasticSearch->createIndex($serviceElastic);
        // }

        unset($AWSElasticSearch);
    }
}
