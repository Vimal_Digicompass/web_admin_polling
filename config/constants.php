<?php

/**
 * Application constants
 *
 * Todo
 *  1. Page constants slugs for user activity logs
 *
 */

use Illuminate\Http\Response;

return [

    "FILE_UPLOAD_PATH" => 'uploads/users',
    "PROJECT_FILE_UPLOAD_PATH" => 'uploads/projects',
    "PROPERTY_FILE_UPLOAD_PATH" => 'uploads/properties',
    "SERVICE_FILE_UPLOAD_PATH" => 'uploads/services',

    'url' => env('APP_URL'),
    'EMAIL_SUBJECT' => [

        'RESET_PASSWORD' => "1SqFt::Reset Password",
        'RESET_PASSWORD_PATH' => "/admin/updatepassword/"
    ],
    'CALL_STATUS' => [
        'CUSTOMER_NOT_AVAILABLE' => 1,
        'FOLLOW_UP' => 2,
        'CLOSED' => 3,
        'ALL' => 5
    ],
    'ROLE_TYPE' => [
        'TEAM_LEAD' => 1,
        'BACKEND_OPS_TEAM' => 3,
        'SUPER_ADMIN' => 0,
        'CRM_USER' => 2,
        'SUB_ADMIN' => 4

    ],
    'SCHEDULING_RULE' => [
        'ROUND_ROBIN' => 'prod-every-10minutes-ist',
        'FOLLOW_UP' => 'prod-every-12am-ist'
    ],
    'CRITERION' => [
        'KYC_APPROVAL' => 1,
        'KYC_PENDING' => 2,
        'KYC_REJECTED' => 3,
        'POST_APPROVAL' => 4,
        'POST_REJECTED' => 5,
        'RECHARGE_FAILED' => 6,
        'PACKAGE_EXPIRY' => 7,
        'PAYMENT_PENDING' => 8,
        'NOT_ENQUIRE_POST' => 10,
        'NOT_POST' => 11,
        'NOT_POST_SERVICE_REQ' => 12,
        'MARKED_FAVOURITE' => 13,
        'INACTIVE_USER' => 14,
        'LEADS_NOT_VIEWED' => 15,
        'UPGRADE_TO_BUSINESS_USER' => 16,
        "ON_KYC_APPROVED"            => 163, //KYC approved
        "ON_KYC_REJECTED"            => 164, //KYC rejected
        "ON_PROPERTY_APPROVED"        => 166, //Property approved
        "ON_PROPERTY_REJECTED"        => 167, //Property rejected
        "ON_PROJECT_APPROVED"        => 176, //Project approved
        "ON_PROJECT_REJECTED"        => 177, //Project rejected
        "ON_SERVICE_APPROVED"        => 178, //SERVICE approved
        "ON_SERVICE_REJECTED"        => 179, //SERVICE rejected
    ],
    'BlockIPAddress' => ['210.18.138.188'], //'210.18.138.188'
    'NOTIFICATIONCONTENT' => [
        'KYC_APPROVAL' => 'Kudos! Your KYC has been approved. Your documents have been verified. Now, you can commence your business with us.',
        'KYC_REJECTED' => 'Sorry! Your documents have to be more specific. Please try again.',
        'PROPERTY_APPROVED' => 'Your property listing has been approved. Except response or leads soon. All the best.',
        'PROPERTY_REJECTED' => 'Your property listing has not been approved. You can try posting again.',
        'PROJECT_APPROVED' => 'Your project listing has been approved. Except response or leads soon. All the best.',
        'PROJECT_REJECTED' => 'Your project listing has not been approved. You can try posting again.',
        'SERVICE_APPROVED' => 'Your service listing has been approved. Except response or leads soon. All the best.',
        'SERVICE_REJECTED' => 'Your service listing has not been approved. You can try posting again.',

    ],
    'STATUS' => [
        'INITIATED'                     => 1001,
        'QUEUED'                        => 1002,
        'INITIATED_WITH_THIRD_PARTY'    => 1003,
        'THIRD_PARTY_ACKNOWLEDGED'      => 1004,
        'DELIVERED'                     => 1005,
        'OPENED'                        => 1006,
        'FAILED'                        => 1010,
        'FAILED_BY_THIRD_PARTY'         => 1011,
    ],
    'EXPIRY_DAYS' => [
        0,
        1,
        2,
        3,
        4,
        5,
    ],
    'HTTP_STATUS' => [
        /*
        201-store success
        200-update success
        200-delete success
        404-delete id not found
        200-collection emptied
        401 - Unauthorized
        400 - Bad Request
        404 - Not Found / document id not found
        408 - Request Timeout       -default
        500 - Internal Server Error -default
        502 - Bad Gateway           -default
        */
        'SUCCESS' => [
            'CREATE'    => Response::HTTP_CREATED,
            'UPDATE'    => Response::HTTP_OK,
            'DELETE'    => Response::HTTP_OK,
            'TRUNCATE'  => Response::HTTP_OK,
        ],


        'BAD_REQUEST'   => Response::HTTP_BAD_REQUEST,
        'UNAUTHORIZED'  => Response::HTTP_UNAUTHORIZED,
        'FORBIDDEN'     => Response::HTTP_FORBIDDEN,
        'NOT_FOUND'     => Response::HTTP_NOT_FOUND,
    ],

    'AREA_UNIT' => [
        1 => "sq. ft",
        2 => "sq. yrd",
        3 => "sq. m",
        4 => "acre",
        5 => "hectare",
        6 => "ground",
        7 => "cent"
    ],
    'APPROVAL_AUTHORITY' => [
        1 => "sq. ft",
        2 => "sq. yrd",
        3 => "sq. m",
        4 => "acre",
        5 => "hectare",
        6 => "ground"
    ],
    'service-type' => [
        '1' => 'Product', // Pase 3
        '2' => 'Service',
        '3' => 'Contract Service' // Pase 3
    ],

    'service-approve-status' => [
        '0' => 'Waiting',
        '1' => 'Approved',
        '2' => 'Rejected',
    ],
    'ADMIN_POLLING_FLOW' => [
        'SENT_TO_POLL' => 1,
        'ASSIGN_TO_CRM_USER' => 2,
        'ASSIGN_TO_CRM_USER_FOLLOWUP' => 3,
        'PROCESSED' => 4
    ],
    'PAYMENT_MODE' => [
        1 => 'By Cash',
        2 => 'By DD',
        3 => 'Online Payment',
        4 => "By Cheque"
    ],

    'FOOD' => ['Breakfast', 'Lunch', 'Dinner'],
    'GENDER' => ['Boys', 'Girls'],
    'ROOMTYPE' => ['Shared Room', 'Single Room'],
    'PGROOMTYPE' => ['Single Room', 'Double Sharing', 'Triple Sharing', 'Dormitory'],
    'ROOMAMENITIES' => ['CUPBOARD', 'TV', 'AC', 'GEYSER', 'Bed/Cot'],
    'COMMONAMENITIES' => ['LAUNDRY', 'ROOM CLEANING', 'WARDEN', 'WIFI', 'COMMON TV', 'LIFT', 'POWER BACKUP', 'MESS', 'REFRIGERATOR', 'COOKING'],
    'PROPERTY_NEGOTIABLE' => [
        1 => "Yes",
        0 => "No"
    ],

    'joint_venture' => [
        1 => "Yes",
        2 => "No"
    ],
    'property-approve-status' => [
        '0' => 'Waiting',
        '1' => 'Approved',
        '2' => 'Rejected',
    ],

    'personal-washroom' => [
        1 => "Yes",
        0 => "No"
    ],
    'boundary-wallmade' => [
        1 => "Yes",
        0 => "No"
    ],

    'corner-plot' => [
        1 => "Yes",
        0 => "No"
    ],
    'payment-type' => [
        '1' => 'By Cash',
        '2' => 'By DD',
        '3' => 'Online Payment',
        '4' => 'By Cheque'
    ],
    'property-visible-for' => [
        0 => "1sqft",
        1 => "Influencer",
        2 => "Both"
    ],
    'project-approve-status' => [
        '0' => 'Waiting',
        '1' => 'Approved',
        '2' => 'Rejected',
    ],

    'approval-authority' => [
        1 => "DTCP",
        2 => "BDA"
    ],
    'pantry-cafeteria' => [
        1 => "Dry",
        2 => "Wet",
        3 => "Not Available"
    ],

    'PG-Types' => [
        1 => "Boys",
        2 => "Girls",
    ],

    'PG-Preferred-Tenant' => [
        1 => "Student",
        2 => "Professional",
    ],

    'PG-Room-Type' => [
        'S' => "Single Room",
        'D' => "Double Sharing",
        'T' => "Triple Sharing",
        'F' => 'Dormitory'
    ],

    'AttachBathroom' => [
        "Yes" => "Yes",
        "No" => "No",
    ],

    'FoodFacility' => [
        1 => "Breakfast",
        2 => "Lunch",
        3 => "Dinner",
    ],

    "Parking" => [
        1 => "Car",
        2 => "Bike",
    ],

    "RoomAmenities" => [
        1 => "CUPBOARD",
        2 => "TV",
        3 => "AC",
        4 => "GEYSER",
        5 => "Bed/Cot",
    ],

    "PGRules" => [
        1 => "SMOKING",
        2 => "GUARDIAN",
        3 => "BOY'S ENTRY",
        4 => "DRINKING",
        5 => "NON-VEG",
    ],

    "CommonAmenities" => [
        1 => "LAUNDRY",
        2 => "WARDEN",
        3 => "ROOM CLEANING",
        4 => "WIFI",
        5 => "COMMON TV",
        6 => "LIFT",
        7 => "POWER BACKUP",
        8 => "MESS",
        9 => "REFRIGERATOR",
        10 => "COOKING"
    ],

    'property-visible-for' => [
        0 => "1sqft",
        1 => "Influencer",
        2 => "Both"
    ],

    'property-looking-for-influencer' => [
        0 => "Lead",
        1 => "Sale"
    ],

    'salaried-business' => [
        0 => "Salaried",
        1 => "Business"
    ],

    'property-flatmate-types' => [
        0 => "Apartment",
        1 => "Independent House or Villa",
        2 => "Gated community Villa"
    ],

    'Flatemates-tenant-Types' => [
        1 => "Boys",
        2 => "Girls",
    ],

    'Flatemates-room-Types' => [
        1 => "Single Room",
        2 => "Shared Room",
    ],

    'Flatemates-non-veg-allowed' => [
        1 => "Yes",
        2 => "No",
    ],

    'Flatemates-gated_security' => [
        1 => "Yes",
        2 => "No",
    ],

    'Preferred-tenant-Types' => [
        1 => "Student",
        2 => "Professional",
    ],
];
