@push('plugin-styles')
    <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
    <style>
        .card-body .form-group,
        .form-group.has-danger .form-control {
            border-color: #e8ebf1 !important;
        }

    </style>
    @endpush @extends('layout.master') @section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            @include('Components.heading')
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/dashboard"> <i style="width:13px;height:12px" class="link-icon"
                            data-feather="home"></i>&nbsp;Dashboard</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page"><a class="active" href="#">{{ $scenario }}</a>
                </li>
            </ol>
        </nav>
    </div>

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-head">
                    <div class="p-3 btn-group">
                        <div class="mr-2" role="group" aria-label="First group">
                            <a data-tooltip="tooltip" data-placement="top" title="Create Role" href="roles"
                                class="btn btn-danger submit">ROLE <i class="fas fa-project-diagram"></i></a>
                        </div>
                        <div role="group" aria-label="Third group">
                            <button id="adduser" type="button" data-tooltip="tooltip" data-placement="top"
                                title="Create user" class="btn btn-danger submit">ADD USER <i
                                    class="fas fa-user-plus"></i></button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="userTable" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Contact info</th>
                                    <th>Role</th>
                                    <th>Created Date</th>
                                    <th>Updated Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="tbody">


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal bd-example-modal-lg fade" id="usermodal" tabindex="-1" role="dialog" aria-labelledby="usermodalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form id="userform" class="forms-sample">
                    <input type="hidden" name="user_id" id="user_id" value="0" />
                    <div class="modal-header">
                        <h5 class="modal-title" id="usertext">Add User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 grid-margin">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Username: <span
                                                            style="color:red">*</span></label>
                                                    <input class="form-control" required id="username" name="username" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Role: <span
                                                            style="color:red">*</span></label>
                                                    <select id="role" name="role" class="form-control">
                                                        <option value="">Select Role</option>
                                                        @foreach ($roledata as $roleinfo)
                                                            <option value={{ $roleinfo->role_id }}>
                                                                {{ $roleinfo->role_title }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">First Name:<span
                                                            style="color:red">*</span></label>
                                                    <input class="form-control" id="firstname" name="firstname" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Last Name: <span
                                                            style="color:red">*</span></label>
                                                    <input class="form-control" id="lastname" name="lastname" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">State: <span
                                                            style="color:red">*</span></label>
                                                    <select id="state" name="state" class="form-control">
                                                        <option value="">Select state</option>
                                                        @foreach ($statedata as $state)
                                                            <option value={{ $state->id }}>{{ $state->title }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">City: <span
                                                            style="color:red">*</span></label>
                                                    <select id="city" name="city" class="form-control">
                                                        <option value="">Select city</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <div class="form-group">
                                                    <label class="control-label">Password: <span
                                                            style="color:red">*</span></label>
                                                    <div class="input-group">
                                                        <input type="password" class="form-control" id="password"
                                                            name="password" aria-label="Password"
                                                            aria-describedby="basic-addon2" />
                                                        <div class="input-group-append">
                                                            <div class="input-group-text visible" id="password_visible"><i
                                                                    class="far fa-eye-slash"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Confirm password: <span
                                                            style="color:red">*</span></label>
                                                    <div class="input-group">
                                                        <input type="password" class="form-control" id="confirmpassword"
                                                            name="confirmpassword" aria-label="confirm Password"
                                                            aria-describedby="basic-addon2" />
                                                        <div class="input-group-append">
                                                            <div class="input-group-text visible"
                                                                id="confirmpassword_visible"><i
                                                                    class="far fa-eye-slash"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Email Address: <span
                                                            style="color:red">*</span></label>
                                                    <input id="useremail" name="useremail" class="form-control"
                                                        type="email" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Mobile Number: <span
                                                            style="color:red">*</span></label>
                                                    <input id="mobilenumber" name="mobilenumber" value="+91"
                                                        class="form-control" data-inputmask-alias="(+99) 99999-99999" />
                                                    <span id="mobileerrors"
                                                        style="font-size:14px;color:#ff3366 !important"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Team Lead: </label>
                                                    <select id="teamlead" name="teamlead" class="form-control">
                                                        <option value=0>Select Team lead</option>
                                                        @foreach ($teamleaddata as $teamlead)
                                                            <option value={{ $teamlead->user_id }}>
                                                                {{ $teamlead->username }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Photo: </label>
                                                    <input class="form-control" type="file" id="filedata" name="filedata" />
                                                    <img src="" id="profilepic" alt=""
                                                        style="width:150px;margin-top:10px;border-radius:60px" />
                                                    <button type="button" data-tooltip="tooltip" data-placement="top"
                                                        title="Delete" style="display:none;margin-top:10px"
                                                        class="btn btn-danger imagedelete btn-icon submit"> <i
                                                            data-feather="trash-2"></i></button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Assign Limit: </label>
                                                    <input type="number" class="form-control"
                                                        oninput="this.value = Math.abs(this.value)" min='0' value=10
                                                        id="assignlimit" name="assignlimit" />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <span id="errors" style="font-size:14px;color:red"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" id="usersubmit" type="submit">Save</button>
                        <input class="btn btn-secondary" data-dismiss="modal" type="reset" value="CANCEL" />
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection @push('plugin-scripts')

    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

    <script src="{{ asset('assets/plugins/inputmask/jquery.inputmask.bundle.min.js') }}"></script>

    <script src="{{ asset('assets/js/inputmask.js') }}"></script>

    <script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>

    @endpush @push('custom-scripts')
    <script src="{{ asset('assets/js/form-validation.js') }}"></script>

    <script src="{{ asset('assets/js/data-table.js') }}"></script>
@endpush
