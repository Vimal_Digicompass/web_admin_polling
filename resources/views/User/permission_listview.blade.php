@push('plugin-styles')
    <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
@endpush
@extends('layout.master')
@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
          @include('Components.heading')
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard"> <i style="width:13px;height:12px" class="link-icon"
                            data-feather="home"></i>&nbsp;Dashboard</a></li>
                <li class="breadcrumb-item " aria-current="page"><a href="/users/lists">Users</a></li>
                <li class="breadcrumb-item " aria-current="page"><a href="/users/roles">Roles</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a class="active" href="#">{{ $scenario }}</a></li>

            </ol>
        </nav>
    </div>

    <div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="col-md-4" style="padding-left:0px;">
              <select id="permissionrole" name="permissionrole" class="js-example-basic-multiple w-100">

                   @foreach ($roledata as $roleinfo )
                        @if($requestedRoleID == $roleinfo->role_id)
                        <option selected value={{$roleinfo->role_id}}>{{$roleinfo->role_title}}</option>
                        @else
                        <option value={{$roleinfo->role_id}}>{{$roleinfo->role_title}}</option>
                        @endif

                   @endforeach
                </select>
        </div>
        <div class="table-responsive pt-3">
          <table class="table table-bordered  table-hover">
            <thead>
              <tr>
                <th>
                  #
                </th>
                <th>
                  Module
                </th>
                <th>
                  Assign Limit
                </th>
                <th>Access</th>

                 <th>
                  Read Mobile Number
                </th>
                 <th>
                  Read completed records
                </th>
                 <th>
                  Notification Access
                </th>
                 <th>
                  Export Access
                </th>
                 <th>
                  Approve Access
                </th>
                <th>
                  User Active Access
                </th>
                <th>
                  KYC Update
                </th>
                <th>
                 Follow Up
                </th>
                <th>
                 Call Status
                </th>
                <th>
                 Post Update
                </th>
                <th> Publish Status Update
                </th>
              </tr>
            </thead>
            <tbody>
                 @foreach ($permission as $permissioninfo )
                     <tr>
                <td>
                  {{ $permissioninfo->permission_id }}
                </td>
                <td>
                 {{ $permissioninfo->permission_name }}
                </td>
                <td>
                   <input id="assign_limit_{{ $permissioninfo->permission_id }}" oninput="this.value = Math.abs(this.value)" class="assignlimit form-control mb-4 mb-md-0 assignlimit" min="0" type="number" />
                </td>
                <td>
                    @if($permissioninfo->is_access == 0)
                    N/A
                    @else
                     <input type="checkbox" class="access" id="is_access_{{ $permissioninfo->permission_id }}">
                    @endif

                </td>

                <td>
                @if($permissioninfo->is_mobilenumberread == 0)
                    N/A
                    @else
                     <input type="checkbox" class="mobilenumberaccess" id="is_mobilenumberread_{{ $permissioninfo->permission_id }}">
                    @endif
                </td>
                <td>
                 @if($permissioninfo->is_readcompleteddata == 0)
                    N/A
                    @else
                     <input type="checkbox" class="completeddataaccess" id="is_readcompleteddata_{{ $permissioninfo->permission_id }}">
                    @endif
                </td>
                <td>
                  @if($permissioninfo->is_bulkknotificationaccess == 0)
                    N/A
                    @else
                     <input type="checkbox" class="notificationdataaccess" id="is_bulkknotificationaccess_{{ $permissioninfo->permission_id }}">
                    @endif
                </td>
                <td>
                  @if($permissioninfo->is_exportaccess == 0)
                    N/A
                    @else
                     <input type="checkbox" class="exportaccess" id="is_exportaccess_{{ $permissioninfo->permission_id }}">
                    @endif
                </td>
                <td>
                 @if($permissioninfo->is_approve == 0)
                    N/A
                    @else
                     <input type="checkbox" class="approveaccess" id="is_approve_{{ $permissioninfo->permission_id }}">
                    @endif
                </td>
                <td>
                 @if($permissioninfo->is_useractive == 0)
                    N/A
                    @else
                     <input type="checkbox" class="useractiveaccess" id="is_useractive_{{ $permissioninfo->permission_id }}">
                    @endif
                </td>
                <td>
                 @if($permissioninfo->is_kycupdate == 0)
                    N/A
                    @else
                     <input type="checkbox" class="kycaccess" id="is_kycupdate_{{ $permissioninfo->permission_id }}">
                    @endif
                </td>
                <td>
                 @if($permissioninfo->is_follow_up == 0)
                    N/A
                    @else
                     <input type="checkbox" class="followup" id="is_follow_up_{{ $permissioninfo->permission_id }}">
                    @endif
                </td>
                 <td>
                 @if($permissioninfo->is_callstatus == 0)
                    N/A
                    @else
                     <input type="checkbox" class="callstatus" id="is_callstatus_{{ $permissioninfo->permission_id }}">
                    @endif
                </td>
                 <td>
                 @if($permissioninfo->is_postupdate == 0)
                    N/A
                    @else
                     <input type="checkbox" class="postupdate" id="is_postupdate_{{ $permissioninfo->permission_id }}">
                    @endif
                </td>
                <td>
                 @if($permissioninfo->is_publishactive == 0)
                    N/A
                    @else
                     <input type="checkbox" class="publishstatus" id="is_publishactive_{{ $permissioninfo->permission_id }}">
                    @endif
                </td>
              </tr>

                @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection

@push('plugin-scripts')

    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

@endpush

