
          <div class="row">
              @include('Components.statecityfilter')


              @if ($scenario == 'Recharge Failed'  || $scenario == 'Payment Pending' || $scenario == 'Not Enquiry Post' || $scenario == 'Not Post' || $scenario == 'Not Post Service Req' || $scenario == 'Inactive User')
                  <div class="col-sm-3 ">
                      <div class="form-group">
                          <label class="control-label">User Type</label>
                          <select id="usertype" name="usertype" class="js-example-basic-multiple w-100">
                              <option value="">select</option>
                              @foreach ($userType as $key => $value)
                                  <option value="{{ $value['titlevalue'] }}">{{ $value['title'] }}</option>
                              @endforeach

                          </select>
                      </div>
                  </div><!-- Col -->
              @else
              @endif
              @if ($scenario == 'Recharge Failed' || $scenario == 'Commoner Package Expiry')

                  <div class="col-sm-3">
                      <div class="form-group">
                          <label class="control-label">Package Name</label>
                          <select id="packagename" name="packagename" class="js-example-basic-multiple w-100">
                              <option value="">select</option>
                              @foreach ($WalletMaster as $key => $value)
                                  <option value="{{ $value['id'] }}">{{ $value['package_name'] }}</option>
                              @endforeach
                          </select>
                      </div>
                  </div><!-- Col -->
              @else
              @endif
              @if ($scenario == 'Not Post' || $scenario == 'Inactive User')

                  <div class="col-sm-3 ">
                      <div class="form-group">
                          <label class="control-label">Period of activity</label>
                          <input type="text" name="dates" id='dates' class="form-control">
                      </div>
                  </div><!-- Col -->
              @else
              @endif
              @if ($scenario == 'Commoner Package Expiry')

                  <div class="col-sm-3">
                      <div class="form-group">
                          <label class="control-label">No of days Package Epiry</label>
                          <select id="daystoexpiry" name="daystoexpiry" class="js-example-basic-multiple w-100">
                              <option value="">select</option>
                              @foreach ($ExpiryDays as $key => $value)
                                  <option value="{{ $value }}">{{ $value }}</option>
                              @endforeach

                          </select>
                      </div>
                  </div><!-- Col -->
              @else
              @endif
              @include('Components.searchfilter')
          </div><!-- Row -->
