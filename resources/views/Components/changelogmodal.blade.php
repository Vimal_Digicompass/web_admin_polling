<div class="modal bd-example-modal-lg fade" id="changelogmodal" tabindex="-1" role="dialog"
    aria-labelledby="changelogmodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changelogmodalLabel">Changelog</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 grid-margin">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive pt-3">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Field
                                                </th>
                                                <th>
                                                    Old Valud
                                                </th>
                                                <th>
                                                    New Value
                                                </th>
                                                <th>
                                                    Changed by
                                                </th>
                                                <th>
                                                    Change Date
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    Call Remarks
                                                </td>
                                                <td>
                                                    Testing
                                                </td>
                                                <td>
                                                    Testing Changelog
                                                </td>
                                                <td>
                                                    Admin
                                                </td>
                                                <td>
                                                    12-05-2021
                                                </td>
                                            <tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
        </div>
    </div>
</div>
