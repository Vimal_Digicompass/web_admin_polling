@if ($userinfo->role_type == 0 || $userinfo->role_type == 3)
    <div class="row" id="myItems">
        <div class="col-sm-12 mb-3">
            <input type="text" id="myFilter" class="form-control" onkeyup="myFunction()"
                placeholder="Search for names..">
        </div>
        @foreach ($dashboardInfo as $key => $value)
            <div class="col-space col-12 col-md-6 col-md-4">
                <div class="card card-top">
                    <div class="img-center">
                        <img src="{{ url('assets/images/placeholder.jpg') }}" class=" img-class" alt="...">
                    </div>
                    <div class="card-body">
                        @if ($value['is_online'] == 1)
                            <h5 class="card-title">{{ $value['first_name'] . ' ' . $value['last_name'] }} <span
                                    class="badge badge-pill badge-success"> </span></h5>
                        @else
                            <h5 class="card-title">{{ $value['first_name'] . ' ' . $value['last_name'] }} <span
                                    class="badge badge-pill badge-danger"> </span></h5>
                        @endif
                        <a style="font-size:14px;" class="teamleadmoreinfo" href="javascript:void(0)"
                            id="{{ $value['user_id'] }}">More</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@elseif($userinfo->role_type == 1)
    <div class="row" id="myItems">
        <div class="col-sm-12 mb-3">
            <input type="text" id="myFilter" class="form-control" onkeyup="myFunction()"
                placeholder="Search for names..">
        </div>
        @foreach ($dashboardInfo as $key => $value)
            <div class="col-space col-12 col-md-6 col-md-4">
                <div class="card card-top">
                    <div class="img-center">
                        @php
                            $path = $value['image_name'];
                            $data = getFileURLPublicPath($path);
                            if ($data != '') {
                                $imageurl = $base64url = $data;
                            } else {
                                $base64url = 'https://via.placeholder.com/80x80';
                                $imageurl = '';
                            }
                        @endphp
                        <img src="{{ url('assets/images/placeholder.jpg') }}" class=" img-class" alt="...">
                    </div>
                    @php
                        $totalfollowup = 0;
                    @endphp
                    @foreach ($value['permissionDetail'] as $key => $val)
                        @php
                            $totalfollowup = $totalfollowup + (int) $val['totalFollowupCount'];
                        @endphp
                    @endforeach
                    <div class="card-body">
                        @if ($value['is_online'] == 1)
                            <h5 class="card-title">{{ $value['first_name'] . ' ' . $value['last_name'] }} <span
                                    class="badge badge-pill badge-success"> </span><br><br> Today Followup count: <span
                                    class="badge badge-warning">@php echo $totalfollowup; @endphp</span></h5>
                        @else
                            <h5 class="card-title">{{ $value['first_name'] . ' ' . $value['last_name'] }} <span
                                    class="badge badge-pill badge-danger"> </span><br><br>Today Followup count: <span
                                    class="badge badge-warning">@php  echo $totalfollowup; @endphp</span></h5>
                        @endif
                        <a style="font-size:14px;" class="moreinfo" href="javascript:void(0)"
                            id="{{ $value['user_id'] }}">More</a>
                        <div class="row">
                            @foreach ($value['permissionDetail'] as $key => $val)
                                <div class="col col-md-4">
                                    <a href="{{ $val['path'] }}/{{ $value['user_id'] }}"
                                        style="color:#727cf5;font-size: 13px;" class="">{{ $val['criteria'] }}
                                        {{ $val['totalAssignCount'] }}</a>
                                </div>
                            @endforeach

                        </div>

                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="row">
            <div class="col-8 col-xl-8 stretch-card">
                <div class="row flex-grow">

                    @foreach ($dashboardInfo as $key => $value)
                        <div class="col-md-6 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between align-items-baseline">
                                        <h3 style="color: #727cf5;" class="card-title mb-0"><a
                                                href="{{ $value['path'] }}/{{ Auth::user()->user_id }}">{{ $value['criteria'] }}</a>
                                            <h6 class="mb-2 badge badge-danger">{{ $value['totalAssignCount'] }}</h6>
                                        </h3>

                                    </div><br>
                                    <div class="row">
                                        <div class="col-6 col-md-12 col-xl-5">
                                            <h6 class="mb-2"> Pending</span> </h6><span
                                                class=" badge badge-pill badge-warning mb-2">{{ $value['waitingCount'] }}</span>
                                        </div>
                                        <div class="col-6 col-md-12 col-xl-5">
                                            <h6 class="mb-2"> Processed</h6><span
                                                class=" badge badge-pill badge-success mb-2">{{ $value['totalProcessed'] }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach


                </div>
            </div>

            <div class="col-4 col-md-4 stretch-card">
                <div class="row flex-grow">
                    <div class="col-md-12 grid-margin ">
                        <div class="card" style="border-top: 3px solid #084c61; !important">
                            <div class="card-header"
                                style="font-weight:bold;font-size:14px;color:white;background-color: #084c61;">
                                TODAY'S FOLLOW UP <i class="fas fa-bell bell"></i>
                            </div>
                            <div class="card-body">
                                @foreach ($dashboardInfo as $key => $value)

                                    <a style="font-size:14px;"
                                        href="{{ $value['path'] }}/{{ Auth::user()->user_id }}">{{ $value['totalFollowupCount'] }}
                                        follow up's</a> <span style="font-size:14px;"> for
                                        {{ $value['criteria'] }}</span> <br>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- row -->
@endif
