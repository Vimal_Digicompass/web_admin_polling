  @if ($scenario == 'Approval')
      <div class="col-sm-3">
          <div class="form-group">


              @if ($criteriaId == 4)
                  <label class="control-label">Post Status</label>
                  <select id="poststatus" name="poststatus" class="js-example-basic-multiple w-100">
                      @foreach ($KYCStatus as $key => $value)

                          @if ($value['id'] == 0 || $value['id'] == 1 || $value['id'] == 2 || $value['id'] == 6)
                              @if ($value['id'] == 0)
                                  <option selected value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                              @else
                                  <option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                              @endif
                          @else
                          @endif

                      @endforeach

                  @else
                      <label class="control-label">KYC Status</label>
                      <select id="kycstatus" name="kycstatus" class="js-example-basic-multiple w-100">
                          @foreach ($KYCStatus as $key => $value)
                              @if ($value['title'] == 'All')
                                  <option selected value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                              @else
                                  <option value="{{ $value['id'] }}">{{ $value['title'] }}</option>

                              @endif
                          @endforeach
              @endif

              </select>
          </div>
      </div><!-- Col -->
  @else

      @if ($criteriaId == 5)
          <div class="col-sm-3">
              <div class="form-group">
                  <label class="control-label">Post Status</label>
                  <select id="poststatus" name="poststatus" class="js-example-basic-multiple w-100">
                      @foreach ($KYCStatus as $key => $value)

                          @if ($value['id'] == 0 || $value['id'] == 1 || $value['id'] == 2 || $value['id'] == 6)
                              @if ($value['id'] == 0)
                                  <option selected value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                              @else
                                  <option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                              @endif
                          @else
                          @endif

                      @endforeach
                  </select>
              </div>
          </div>
      @elseif($criteriaId == 16)
          <div class="col-sm-3">
              <div class="form-group">
                  <label class="control-label">KYC Status</label>
                  <select id="kycstatus" name="kycstatus" class="js-example-basic-multiple w-100">
                      @foreach ($KYCStatus as $key => $value)
                          @if ($value['title'] == 'All')
                              <option selected value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                          @else
                              <option value="{{ $value['id'] }}">{{ $value['title'] }}</option>

                          @endif
                      @endforeach
                  </select>
              </div>
          </div>
      @else
      @endif
  @endif
  @if ($scenario == 'Not Post Service Req')
      <div id="servicecategoryediv" class="col-sm-3 service">
          <div class="form-group">
              <label class="control-label">Last searched Category</label>
              <select id="lastsearchcategory" name="lastsearchcategory" class="js-example-basic-multiple w-100">
                  <option value="">Select</option>
                  @foreach ($ServiceCategory as $key => $value)
                      <option value="{{ $value['title'] }}">{{ $value['title'] }}</option>
                  @endforeach
              </select>
          </div>
      </div><!-- Col -->
  @else

  @endif
  <div class="col-sm-3">
      <div class="form-group">

          <label class="control-label">Calling Status</label>
          <select id="searchcallstatus" class="js-example-basic-multiple w-100">
              @foreach ($callStatus as $key => $value)
                  @if ($value['title'] == 'All')
                      <option selected value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                  @else
                      @if ($value['id'] == 3)
                          <!--  closed -->
                          @if (Auth::user()->role_type == 1)
                              <option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                          @else
                          @endif
                      @else
                          <option value="{{ $value['id'] }}">{{ $value['title'] }}</option>
                      @endif

                  @endif
              @endforeach

          </select>
      </div>
  </div><!-- Col -->

  <div class="col-sm-6">
      <button id="filtersubmit" style="margin-top: 30px;
                padding: 10px;" type="button" data-tooltip="tooltip" data-placement="top" title="Click to search"
          class="btn btn-danger submit">Search</button>
      @php
          $getpermissionData = getPermissionAndAccess($criteriaId, Auth::user()->role_id);
          $count = count($getpermissionData);
      @endphp

      @if ($count != 0)
          @if ($getpermissionData[0]['is_exportaccess'] != 0)
              <button style="margin-top: 30px;
                padding: 10px;" id="exportexcel" type="button" data-tooltip="tooltip" data-placement="top"
                  title="Export" class="btn btn-primary submit">Export to Excel</button>
          @else
          @endif
           @if ($getpermissionData[0]['is_bulkknotificationaccess'] != 0)
                   <a id="sendnotify" style="margin-top: 30px;
                padding: 10px;"  href="javascript:void(0)" data-tooltip="tooltip" data-placement="top" title="Send In-App Notification"
                   class="btn btn-danger submit">Send Manual Notification</a>
                @else

                @endif
      @else
      @endif

  </div>
