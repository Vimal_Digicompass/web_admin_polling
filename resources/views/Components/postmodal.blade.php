<div class="modal bd-example-modal-lg fade" id="postdetailview" tabindex="-1" role="dialog"
    aria-labelledby="postdetailviewLabel" aria-hidden="true">
</div>

<div class="modal bd-example-modal-lg fade" id="propertyeditviewmodal" tabindex="-1" role="dialog"
    aria-labelledby="propertyeditviewmodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="propertyeditviewmodalLabel">Edit Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 grid-margin">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"> Property Type:</label>
                                                <p style="font-size:14px;">Rent</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"> Property Category:</label>
                                                <p style="font-size:14px;">Flat Mate</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"> Flatmate Property Type<span
                                                        style="color:red;">*</span></label>
                                                <select class="js-example-basic-multiple w-100">
                                                    <option value="">Select</option>
                                                    <option value="Apartment">Apartment</option>
                                                    <option value="Independent House or Villa" selected="">Independent
                                                        House or Villa</option>
                                                    <option value="Gated community Villa">Gated community Villa</option>
                                                </select>
                                            </div>
                                        </div><!-- Col -->

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Tenant Type<span
                                                        style="color:red;">*</span></label>
                                                <select class="js-example-basic-multiple w-100">
                                                    <option value="">Select</option>
                                                    <option value="girls">Girls</option>
                                                    <option value="boys" selected="">Boys</option>
                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"> Room Type<span
                                                        style="color:red;">*</span></label>
                                                <select class="js-example-basic-multiple w-100">
                                                    <option value="">Select</option>
                                                    <option value="sharedroom">Share Room</option>
                                                    <option value="singleroom" selected="">Single Room</option>
                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"> Non veg allowed</label>
                                                <select class="js-example-basic-multiple w-100">
                                                    <option value="">Select</option>
                                                    <option value="yes">Yes</option>
                                                    <option value="no" selected="">NO</option>
                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"> Gated Security</label>
                                                <select class="js-example-basic-multiple w-100">
                                                    <option value="">Select</option>
                                                    <option value="yes">Yes</option>
                                                    <option value="no" selected="">NO</option>
                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"> Bathrooms</label>
                                                <select class="js-example-basic-multiple w-100">
                                                    <option value="">Select</option>
                                                    <option value="1">1</option>
                                                    <option value="2" selected="">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="4+">4+</option>
                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"> Preferred Tenant<span
                                                        style="color:red;">*</span></label>
                                                <select multiple class="js-example-basic-multiple w-100">
                                                    <option value="">Select</option>
                                                    <option value="professional">Professional</option>
                                                    <option value="students" selected="">Students</option>
                                                </select>
                                            </div>
                                        </div><!-- Col -->

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Furnished Type<span
                                                        style="color:red;">*</span></label>
                                                <select multiple class="js-example-basic-multiple w-100">
                                                    <option value="">Select</option>
                                                    <option value="1">Fully-Furnished</option>
                                                    <option value="2" selected="">Semi-Furnished</option>
                                                    <option value="3">Unfurnished</option>
                                                    <option value="4">Half Furnish</option>
                                                    <option value="6">None</option>
                                                    <option value="5">test</option>
                                                </select>
                                            </div>
                                        </div><!-- Col -->

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">parking</label>
                                                <select multiple class="js-example-basic-multiple w-100">
                                                    <option value="">Select</option>
                                                    <option value="1">Car</option>
                                                    <option value="2" selected="">Bike</option>

                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Monthly Rent<span
                                                        style="color:red;">*</span></label>
                                                <input class="form-control mb-4 mb-md-0" value="45,000" type="text" />
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Room Amenities</label>
                                                <select multiple class="js-example-basic-multiple w-100">
                                                    <option value="">Select</option>
                                                    <option value="1">TV</option>
                                                    <option value="2" selected="">AC</option>

                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Possession Date<span
                                                        style="color:red;">*</span></label>
                                                <input class="form-control mb-4 mb-md-0 followdate" type="text" />
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Common Amenities</label>
                                                <select multiple class="js-example-basic-multiple w-100">
                                                    <option value="">Select</option>
                                                    <option value="1">Lift</option>
                                                    <option value="2" selected="">Wifi</option>

                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Floor<span
                                                        style="color:red;">*</span></label>
                                                <select multiple class="js-example-basic-multiple w-100">
                                                    <option value="">Select</option>
                                                    <option value="1">1</option>
                                                    <option value="2" selected="">2</option>

                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Security Deposit<span
                                                        style="color:red;">*</span></label>
                                                <input class="form-control mb-4 mb-md-0" value="50,000" type="text" />
                                            </div>
                                        </div><!-- Col -->

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label"> Negotiable<span
                                                        style="color:red;">*</span></label>
                                                <select class="js-example-basic-multiple w-100">
                                                    <option value="">Select</option>
                                                    <option value="yes">Yes</option>
                                                    <option value="no" selected="">NO</option>
                                                </select>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Address</label>
                                                <input class="form-control mb-4 mb-md-0" value="50,000" type="text" />
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">State</label>
                                                <p style="font-size:14px;">Tamil Nadu</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">City</label>
                                                <p style="font-size:14px;">Chennai</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Area</label>
                                                <p style="font-size:14px;">Medavakkam</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Pincode</label>
                                                <p style="font-size:14px;">600100</p>
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Description</label>
                                                <input class="form-control mb-4 mb-md-0" type="text" />
                                            </div>
                                        </div><!-- Col -->
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Main Photo</label>
                                                <input class="form-control mb-4 mb-md-0" type="file" />
                                            </div>
                                        </div><!-- Col -->

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Other Photos<span style="color:red;"> [Max
                                                        7-Images]</span></label>
                                                <input multiple class="form-control mb-4 mb-md-0" type="file" />
                                            </div>
                                        </div><!-- Col -->
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input class="btn btn-danger" type="submit" value="UPDATE">
                    <input class="btn btn-secondary" data-dismiss="modal" type="reset" value="CANCEL">
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal bd-example-modal-lg fade" id="serviceeditviewmodal" tabindex="-1" role="dialog"
    aria-labelledby="serviceeditviewmodalLabel" aria-hidden="true">

</div>

<div class="modal bd-example-modal-xl fade" id="projecteditviewmodal" tabindex="-1" role="dialog"
    aria-labelledby="projecteditviewmodalLabel" aria-hidden="true">

</div>
