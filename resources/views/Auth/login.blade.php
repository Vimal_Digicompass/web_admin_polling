@extends('layout.master2')

@section('content')
    <div class="page-content d-flex align-items-center justify-content-center">

        <div class="row w-65 mx-0 auth-page">
            <div class="col-md-12 col-xl-6 mx-auto">
                <div style="border-top:0px !important;" class="card">
                    <div class="row">
                        <div class="col-md-12  pl-md-0">
                            <div class="auth-form-wrapper px-4 py-5">
                                <img src="/assets/images/1sqft_sm_logo.png" class="center-block" title="1SqFt" alt="1SqFt"
                                    style="">
                                <h4 style="margin-top:10px;font-size:16px;text-align: center"> Administration Login</h4>
                                <form id="loginform" style="margin-top:30px" class="forms-sample">
                                    @csrf
                                    <div class="form-group mb-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control mb-4 mb-md-0" placeholder="Username"
                                                id="username" name="username" />
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i
                                                        style="height:15px;width:15px;cursor:pointer;"
                                                        data-feather="user"></i></div>
                                            </div>
                                        </div>

                                    </div><br>
                                    <div class="form-group mb-3">
                                        <div class="input-group">
                                            <input type="password" placeholder="Password" class="form-control mb-4 mb-md-0"
                                                id="password" name="password" />
                                            <div id="password_visible" class="input-group-append visible">
                                                <div class="input-group-text"><i
                                                        style="height:15px;width:15px;cursor:pointer;"
                                                        data-feather="eye-off"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mb-3">
                                        <span id="error" style="font-size:14px;color:red"></span>
                                    </div>
                                    <div style="float:right;margin-bottom:10px;">
                                        <a href="/admin/forgotpassword" style="font-size:14px;" class="a-link color_blue"><i
                                                style="width: 15px;height:12px;" data-feather="unlock"></i>Forgot
                                            password?</a>
                                    </div>
                                    <div class="mt-3">
                                        <button type="submit" style="width: 100%;" id="submit"
                                            class="btn btn-danger mr-2 mb-2 mb-md-0">Login</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
