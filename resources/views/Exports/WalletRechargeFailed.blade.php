<table>

    <tbody>
        <tr>
            <td style="text-align:center;font-size:15px;font-weight:bold;" colspan="9">{{ $text }}</td>
        </tr>
    </tbody>
</table><br>
<table id="customers" style="width:100%">
    <thead>
        <tr>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                Name</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                User Type</th>

            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                Mobile Number</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                Package Name</th>

            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                State</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                City</th>

            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                Calling Status</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                Follow-up Date</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                CALL Remarks</th>
            <th
                style=" width: 20px;font-weight:bold;border: 1px solid black;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #b2b2bd;color: black;">
                Assigned To</th>


        </tr>
    </thead>
    <tbody>
        @foreach ($WalletRechargeFailed as $key => $info)
            <tr>
                <td style="border: 1px solid black;">{{ $info['Name'] }}</td>
                <td style="border: 1px solid black;">{{ $info['UserType'] }}</td>
                <td style="border: 1px solid black;">{{ $info['MobileNumber'] }}</td>
                <td style="border: 1px solid black;">{{ $info['Packagename'] }}</td>
                <td style="border: 1px solid black;">{{ $info['State'] }}</td>
                <td style="border: 1px solid black;">{{ $info['City'] }}</td>
                <td style="border: 1px solid black;">{{ $info['CallingStatus'] }}</td>
                <td style="border: 1px solid black;">{{ $info['FollowupDate'] }}</td>
                <td style="border: 1px solid black;">{{ strip_tags($info['CallRemarks']) }}</td>
                <td style="border: 1px solid black;">{{ $info['assignedTo'] }}</td>
            </tr>
        @endforeach
    </tbody>
