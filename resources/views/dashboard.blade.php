@extends('layout.master')
@push('plugin-styles')
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet" />
@endpush

@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            @include('Components.heading')

        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i style="width:13px;height:12px" class="link-icon"
                            data-feather="home"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active" aria-current=""><a class="active" href="#">{{ $scenario }}</a></li>
            </ol>
        </nav>
    </div>
    <form>
        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label style="font-weight:bold;font-size:14px;" class="control-label">From Date - To Date</label>
                    <input type="text" name="dashboarddate" id="dashboarddate" class="filterdate form-control">
                </div>
            </div><!-- Col -->

            <div class="col-sm-3">
                <button id="dashboardsearch" style="margin-top: 30px;
                                        padding: 10px;" type="button" data-tooltip="tooltip" data-placement="top" title=""
                    class="btn btn-danger submit" data-original-title="Click to search">Search</button>
            </div>



        </div><!-- Row -->
    </form>
    <div id="dashboardappend"></div>
    <div class="modal bd-example-modal-lg fade" id="useractivity" tabindex="-1" role="dialog"
        aria-labelledby="useractivityLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="useractivityLabel">User Activity Detail &nbsp;
                    <button title="Download as PDF"  data-original-title="Click to download" data-tooltip="tooltip" data-placement="top" type="button" id="download_btn">
                        <i class="far fa-file-pdf"></i>
                    </button>
                    <button title="Download as Excel"  data-original-title="Click to download" data-tooltip="tooltip" data-placement="top" type="button" id="btnExport">
                        <i class="far fa-file-excel"></i>
                    </button>
                </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <div id="teamleadinfobody" class="modal-body">
                    <div id="teamleadinfo" class="row">
                        <div class="col-md-12 grid-margin">

                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <span style="font-size:14px;font-weight:bold">Date: </span>
                                            <span id="datecount" style="font-size:14px;font-weight:bold"></span><br>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <span style="font-size:14px;">Total Assigned count: </span>
                                            <span id="totalassignedusercount" class="badge badge-primary"
                                                style="font-size:14px;"></span><br>
                                        </div>
                                         <div class="col-md-4"><span style="font-size:14px;">Total Processed count: </span>
                                            <span id="totalprocessedusercount" class="badge badge-success"
                                                style="font-size:14px"></span>
                                        </div>
                                        <div class="col-md-4"> <span style="font-size:14px;">Total Pending count: </span>
                                            <span style="font-size:14px;" class="badge badge-danger"
                                                id="totalpendingusercount"></span><br>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 grid-margin">

                            <div class="card">
                                <div class="card-body">

                                    <div class="table-responsive pt-3">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Criteria
                                                    </th>
                                                    <th>
                                                        Total Assigned Count
                                                    </th>
                                                    <th>
                                                        Total Processed Count
                                                    </th>
                                                    <th>
                                                        Total Pending Count
                                                    </th>

                                                </tr>
                                            </thead>
                                            <tbody id="useractivitybody">

                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
    </div>
@endsection


@push('plugin-scripts')
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
@endpush

@push('custom-scripts')
    <script src="{{ asset('assets/js/useractivity/dashboard.js') }}"></script>
    <script src="{{ asset('assets/js/datepicker.js') }}"></script>
@endpush
