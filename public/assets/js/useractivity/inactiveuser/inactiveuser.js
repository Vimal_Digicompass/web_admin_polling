$("#filtersubmit").on('click', function () {
    $('#inactiveuser').DataTable().destroy();
    $("#filtersubmit").text("Processing...")
    common_load_data("click");
});

common_load_data('onload');
var CommonArr = [];
var NotifyArr = [];

function common_load_data(type) {

    var fromdate = "";
    var todate = "";
    if (type == "onload") {
        fromdate = moment().subtract(7, 'd').format('YYYY-MM-DD');
        todate = moment().format('YYYY-MM-DD');
    } else {
        fromdate = $('#dates').data('daterangepicker').startDate.format('YYYY-MM-DD');
        todate = $('#dates').data('daterangepicker').endDate.format('YYYY-MM-DD');
    }
    var state = $("#state").val();
    var city = $("#city").val();
    var searchcallstatus = $("#searchcallstatus").val();
    var usertype = $("#usertype").val();

    load_data(fromdate, todate, usertype, state, city, searchcallstatus)
}

function load_data(fromdate = "", todate = "", usertype = "", state = "", city = "", status = "") {
    var getnotifydata = $("#getnotifydata").text();
    var input = {};
    input['id'] = $('meta[name="fetchid"]').attr('content');
    input['state'] = state;
    input['city'] = city;
    input['fromdate'] = fromdate;
    input['todate'] = todate;
    input['searchcallstatus'] = status;
    input['criteriaId'] = $('meta[name ="criteria"]').attr('content');
    input['usertype'] = usertype;
    var obj = [];
    if (parseInt(getnotifydata) == 0) {
        obj = [{
                'data': 'name',
            'name': 'name',
                'orderable': false,
            },
            {
                'data': 'usertype',
                'name': 'usertype'
            },
            {
                'data': 'category',
                'name': 'category'
            },
            {
                'data': 'mobile_number',
                'name': 'mobile_number'
            },
            {
                'data': 'dateofregistration',
                'name': 'dateofregistration'
            },

            {
                'data': 'state',
                'name': 'state'
            },
            {
                'data': 'city',
                'name': 'city'
            },

            {
                'data': 'status',
                'name': 'status'
            },
            {
                'data': 'followup_date',
                'name': 'followup_date'
            },
            {
                'data': 'remarks',
                'name': 'remarks'
            }, {
                'data': 'crmusername',
                'name': 'crmusername'
            },
            {
                'data': 'action',
                'name': 'action',

                orderable: false,
                searchable: false
            }
        ]
    } else {
        obj = [{
                'data': 'checkbox',
                'name': 'checkbox',
                'orderable': false,
                'className': 'select-checkbox'
            }, {
                'data': 'name',
                'name': 'name'
            }, {
                'data': 'usertype',
                'name': 'usertype'
            }, {
                'data': 'category',
                'name': 'category'
            }, {
                'data': 'mobile_number',
                'name': 'mobile_number'
            }, {
                'data': 'dateofregistration',
                'name': 'dateofregistration'
            },

            {
                'data': 'state',
                'name': 'state'
            }, {
                'data': 'city',
                'name': 'city'
            },

            {
                'data': 'status',
                'name': 'status'
            }, {
                'data': 'followup_date',
                'name': 'followup_date'
            }, {
                'data': 'remarks',
                'name': 'remarks'
            }, {
                'data': 'crmusername',
                'name': 'crmusername'
            }, {
                'data': 'action',
                'name': 'action',

                orderable: false,
                searchable: false
            }
        ]
    }
    var commontable = $('#inactiveuser').DataTable({
        serverSide: true,
        responsive: false,
        processing: true,

        scrollX: true,
        dom: 'lBfrtip',

        select: {
            style: 'multi', // 'single', 'multi', 'os', 'multi+shift'
            selector: 'td:first-child',
        },
        "ajax": {
            "url": "/useractivity/getinactiveuserlist",
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            "type": "POST",
            data: input,
        },
        drawCallback: function (settings) {
            CommonArr = settings.json.data
            $('.tools').tooltip({
                container: 'body'
            });
            feather.replace();

        },
        rowCallback: function (row, data) {
            var hideout = data.hideout;
            if (hideout == 1) {
                $(row).addClass("blurrow");
                $('td:eq(0)', row).removeClass('select-checkbox');
            }
        },
        "columns": obj,
        "aLengthMenu": [
            [10, 30, 50],
            [10, 30, 50]
        ],
        "iDisplayLength": 10,
        "language": {
            search: "",
            "searchPlaceholder": "search",
        }
    })
    $("#filtersubmit").text("Search")
    commontable.on('select deselect draw', function () {
        var all = commontable.rows({
            search: 'applied'
        }).count(); // get total count of rows
        var selectedRows = commontable.rows({
            selected: true,
            search: 'applied'
        }).count(); // get total count of selected rows
        var data = commontable.rows({
            selected: true,
            search: 'applied'
        }).data()
        NotifyArr = [];
        if (selectedRows == 0) {
            $('#CheckAllButton i').attr('class', 'fa fa-square-o');
        } else {
            if (selectedRows == all) {
                $('#CheckAllButton i').attr('class', 'fa fa-check-square-o');
            }
            for (var i = 0; i < data.length; i++) {
                notify_Id = data[i]['id'];
                if (data[i]['hideout'] == 0) {
                    NotifyArr.push(
                        notify_Id);
                }

            }
        }
    });
    $('#CheckAllButton').on('click', function () {
        var all = commontable.rows({
            search: 'applied'
        }).count(); // get total count of rows
        var selectedRows = commontable.rows({
            selected: true,
            search: 'applied'
        }).count(); // get total count of selected rows
        if (selectedRows < all) {
            //Added search applied in case user wants the search items will be selected
            commontable.rows({
                search: 'applied'
            }).deselect();
            commontable.rows({
                search: 'applied'
            }).select();
        } else {
            commontable.rows({
                search: 'applied'
            }).deselect();
        }
    });

    $(document).on('click', '.inactiveuserdetailview', function () {
        var id = this.id;
        var userinfo = CommonArr[CommonArr.map(function (item) {
            return item.trn_id;
        }).indexOf(parseInt(id))];
        $("#detailviewfirstname").text(userinfo.firstname);
        $("#detailviewlastname").text(userinfo.lastname);
        $("#detailviewemail").text(userinfo.email);
        $("#detailviewcompany").text(userinfo.company_name);
        $("#detailviewusertype").text(userinfo.usertype);
        $("#detailviewcategory").text(userinfo.category);
        $("#detailviewpincode").text(userinfo.pincode);
        $('#detailviewcity').text(userinfo.city)
        $("#detailviewaddress").text(userinfo.address)
        $("#detailviewstate").text(userinfo.state)
        $("#detailviewlastactivedate").text(userinfo.last_login_date)
        $("#detailviewmobilenumber").text(userinfo.mobile_number)
        $("#detailviewserviceposted").text(userinfo.serviceposted)
        $("#detailviewpropertyposted").text(userinfo.propertyposted)
        $("#detailviewprojectposted").text(userinfo.projectposted)
        $("#detailviewservicerequirementposted").text(userinfo.servicerequirementposted)
        $("#detailprojecttotallead").text(userinfo.totalnoofleadsproject)
        $("#detailprojecttotalleadviewed").text(userinfo.totalnoofviewedleadsproject)
        $("#detailpropertytotallead").text(userinfo.totalnoofleadsproperty)
        $("#detailpropertytotalleadviewed").text(userinfo.totalnoofviewedleadsproperty)
        $("#detailservicetotallead").text(userinfo.totalnoofleadsservice)
        $("#detailservicetotalleadviewed").text(userinfo.totalnoofviewedleadsservice)
        $("#inactivedetailviewmodal").modal({
            backdrop: 'static',
            keyboard: false
        });
    })
}
