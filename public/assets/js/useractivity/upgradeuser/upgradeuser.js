var CommonArr = [];
var NotifyArr = [];

$("#filtersubmit").on('click', function () {
    $('#upgradeuser').DataTable().destroy();
    $("#filtersubmit").text("Processing...")
    common_load_data("click");
});

common_load_data('onload');

function common_load_data(type) {



    var state = $("#state").val();
    var city = $("#city").val();
    var kycstatus = $("#kycstatus").val();
    var usercategory = $("#searchusercategory").val()
    var usertype = $("#usertype").val();
    var searchcallstatus = $("#searchcallstatus").val();
    load_data(usertype, state, city, kycstatus, usercategory, searchcallstatus)
}

function load_data(usertype = "", state = "", city = "", status = "", usercategory = "", searchcallstatus= "") {
    var getnotifydata = $("#getnotifydata").text();
    var obj = [];
    if (parseInt(getnotifydata) == 0) {
        obj = [{
                'data': 'name',
            'name': 'name',
                'orderable': false,
            },
            {
                'data': 'usertype',
                'name': 'usertype'
            },
            {
                'data': 'category',
                'name': 'category'
            },
            {
                'data': 'mobile_number',
                'name': 'mobile_number'
            },

            {
                'data': 'dateofregistration',
                'name': 'dateofregistration'
            }, {
                'data': 'dateofupgrade',
                'name': 'dateofupgrade'
            },
            {
                'data': 'state',
                'name': 'state'
            },
            {
                'data': 'city',
                'name': 'city'
            },
            {
                'data': 'address',
                'name': 'address'
            },
            {
                'data': 'kycdocument',
                'name': 'kycdocument'
            },
            {
                'data': 'kycstatus',
                'name': 'kycstatus'
            },
            {
                'data': 'kyc_rejcted_reason',
                'name': 'kyc_rejcted_reason'
            },
            {
                'data': 'status',
                'name': 'status'
            },
            {
                'data': 'followup_date',
                'name': 'followup_date'
            },
            {
                'data': 'remarks',
                'name': 'remarks'
            }, {
                'data': 'crmusername',
                'name': 'crmusername'
            },
            {
                'data': 'action',
                'name': 'action',

                orderable: false,
                searchable: false
            }
        ];
    } else {
        obj = [{
                'data': 'checkbox',
                'name': 'checkbox',
                'orderable': false,
                'className': 'select-checkbox'
            }, {
                'data': 'name',
                'name': 'name'
            },
            {
                'data': 'usertype',
                'name': 'usertype'
            },
            {
                'data': 'category',
                'name': 'category'
            },
            {
                'data': 'mobile_number',
                'name': 'mobile_number'
            },

            {
                'data': 'dateofregistration',
                'name': 'dateofregistration'
            }, {
                'data': 'dateofupgrade',
                'name': 'dateofupgrade'
            },
            {
                'data': 'state',
                'name': 'state'
            },
            {
                'data': 'city',
                'name': 'city'
            },
            {
                'data': 'address',
                'name': 'address'
            },
            {
                'data': 'kycdocument',
                'name': 'kycdocument'
            },
            {
                'data': 'kycstatus',
                'name': 'kycstatus'
            },
            {
                'data': 'kyc_rejcted_reason',
                'name': 'kyc_rejcted_reason'
            },
            {
                'data': 'status',
                'name': 'status'
            },
            {
                'data': 'followup_date',
                'name': 'followup_date'
            },
            {
                'data': 'remarks',
                'name': 'remarks'
            }, {
                'data': 'crmusername',
                'name': 'crmusername'
            },
            {
                'data': 'action',
                'name': 'action',

                orderable: false,
                searchable: false
            }
        ]

    }
    var commontable = $('#upgradeuser').DataTable({
        serverSide: true,
        responsive: false,
        processing: true,
        scrollX: true,
        dom: 'lBfrtip',
        columnDefs: [

            {
                orderable: false,
                className: 'select-checkbox',
                targets: 0,
            }
        ],
        select: {
            style: 'multi', // 'single', 'multi', 'os', 'multi+shift'
            selector: 'td:first-child',
        },
        "ajax": {
            "url": '/useractivity/getupgradeuserlist',
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            "type": "POST",
            data: {
                id: $('meta[name="fetchid"]').attr('content'),

                searchcallstatus: searchcallstatus,
                usertype: usertype,
                state: state,
                city: city,
                status: status,
                usercategory: usercategory
            },
        },
        drawCallback: function (settings) {
            CommonArr = settings.json.data
            $('.tools').tooltip({
                container: 'body'
            });
            feather.replace();

        }, rowCallback: function (row, data) {
            var hideout = data.hideout;
            if (hideout == 1) {
                 $('td:eq(0)', row).removeClass('select-checkbox')
                $(row).addClass("blurrow");
            }
        },
        "columns": obj,
        "aLengthMenu": [
            [10, 30, 50],
            [10, 30, 50]
        ],
        "iDisplayLength": 10,
        "language": {
            search: "",
            "searchPlaceholder": "search",
        }
    })
    $("#filtersubmit").text("Search")
    commontable.on('select deselect draw', function () {
        var all = commontable.rows({
            search: 'applied'
        }).count(); // get total count of rows
        var selectedRows = commontable.rows({
            selected: true,
            search: 'applied'
        }).count(); // get total count of selected rows
        var data = commontable.rows({
            selected: true,
            search: 'applied'
        }).data()
        NotifyArr = [];
         for (var i = 0; i < data.length; i++) {
            notify_Id = data[i]['id'];
            if (data[i]['hideout'] == 0) {
                NotifyArr.push(
                    notify_Id);
            }

        }

        if (selectedRows == 0) {
            $('#CheckAllButton i').attr('class', 'fa fa-square-o');
        } else {
            if (selectedRows == all) {
                $('#CheckAllButton i').attr('class', 'fa fa-check-square-o');
            }

            //
        }
    });
    $('#CheckAllButton').on('click', function () {
        var all = commontable.rows({
            search: 'applied'
        }).count(); // get total count of rows
        var selectedRows = commontable.rows({
            selected: true,
            search: 'applied'
        }).count(); // get total count of selected rows
        if (selectedRows < all) {
            //Added search applied in case user wants the search items will be selected
            commontable.rows({
                search: 'applied'
            }).deselect();
            commontable.rows({
                search: 'applied'
            }).select();
        } else {
            commontable.rows({
                search: 'applied'
            }).deselect();
        }
    });
}
$(document).on('click', '.click', function (e) {
    var id = this.id;
    var userinfo = CommonArr[CommonArr.map(function (item) {
        return item.id;
    }).indexOf(parseInt(id))];
    $("#states").val(userinfo.state_id);
    $("#states").change()
    $("#firstname").val(userinfo.firstname);
    $("#lastname").val(userinfo.lastname);
    $("#email").val(userinfo.email);
    $("#company_name").val(userinfo.company_name);
    $("#mobile_number").val(userinfo.mobile_number);
    $("#whatsapp_number").val(userinfo.whatsapp_number);
    $("#website_url").val(userinfo.website_url);
    $("#user_type").val(userinfo.user_type);
    $("#category_id").val(userinfo.category_id);


    $("#address").val(userinfo.address);
    $("#pincode").val(userinfo.pincode);
    $("#user_idd").val(id);
    setTimeout(function () {
        $("#city_id").val(userinfo.city_id);
        $(".js-example-basic-multiple").select2();
    }, 1000)

    $(".js-example-basic-multiple").select2();
    $("#userprofileeditmodal").modal({
        backdrop: 'static',
        keyboard: false
    });
});
$(document).on('change', '#whatsapp_mobileno', function () {
    var mobile = $('#mobile_number').val();
    if ($(this).is(':checked')) {
        $('#whatsapp_number').val(mobile);
    } else {
        $('#whatsapp_number').val('');
    }
});
