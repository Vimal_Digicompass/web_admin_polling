$("#filtersubmit").on('click', function () {
    $('#walletrechargefailed').DataTable().destroy();
    $("#filtersubmit").text("Processing...")
    common_load_data("click");
});
var NotifyArr = [];
var CommonArr = [];
common_load_data('onload');

function common_load_data(type) {



    var state = $("#state").val();
    var city = $("#city").val();
    var packagename = $("#packagename").val();
    var usertype = $("#usertype").val();

    load_data(usertype, state, city, packagename)
}

function load_data(usertype = "", state = "", city = "", packagename = "") {
    var getnotifydata = $("#getnotifydata").text();
    var searchcallstatus = $("#searchcallstatus").val();
    var input = {};
    input['id'] = $('meta[name="fetchid"]').attr('content');
    input['state'] = state;
    input['city'] = city;
    input['packagename'] = packagename;
    input['searchcallstatus'] = searchcallstatus;
    input['criteriaId'] = $('meta[name ="criteria"]').attr('content');
    input['usertype'] = usertype;
    var obj = [];
    if (parseInt(getnotifydata) == 0) {
        obj = [{
                'data': 'name',
            'name': 'name',
                'orderable': false,
            },
            {
                'data': 'usertype',
                'name': 'usertype'
            },
            {
                'data': 'mobile_number',
                'name': 'mobile_number'
            },

            {
                'data': 'state',
                'name': 'state'
            },
            {
                'data': 'city',
                'name': 'city'
            }, {
                'data': 'packagename',
                'name': 'packagename'
            },
            {
                'data': 'status',
                'name': 'status'
            },
            {
                'data': 'followup_date',
                'name': 'followup_date'
            },
            {
                'data': 'remarks',
                'name': 'remarks'
            } ,{
                'data': 'crmusername',
                'name': 'crmusername'
            },
            {
                'data': 'action',
                'name': 'action',

                orderable: false,
                searchable: false
            }
        ]
    } else {
        obj = [{
                'data': 'checkbox',
                'name': 'checkbox',
                'orderable': false,
                'className': 'select-checkbox'
            }, {
                'data': 'name',
                'name': 'name'
            }, {
                'data': 'usertype',
                'name': 'usertype'
            }, {
                'data': 'mobile_number',
                'name': 'mobile_number'
            },

            {
                'data': 'state',
                'name': 'state'
            }, {
                'data': 'city',
                'name': 'city'
            }, {
                'data': 'packagename',
                'name': 'packagename'
            }, {
                'data': 'status',
                'name': 'status'
            }, {
                'data': 'followup_date',
                'name': 'followup_date'
            }, {
                'data': 'remarks',
                'name': 'remarks'
            }, {
                'data': 'crmusername',
                'name': 'crmusername'
            }, {
                'data': 'action',
                'name': 'action',

                orderable: false,
                searchable: false
            }
        ]
    }
    var commontable = $('#walletrechargefailed').DataTable({
        serverSide: true,
        responsive: false,
        processing: true,
        scrollX: true,
        dom: 'lBfrtip',

        select: {
            style: 'multi', // 'single', 'multi', 'os', 'multi+shift'
            selector: 'td:first-child',
        },
        "ajax": {
            "url": "/payment/getwalletrechargefailedlist",
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            "type": "POST",
            data: input,
        },
        drawCallback: function (settings) {
            CommonArr = settings.json.data
            $('.tools').tooltip({
                container: 'body'
            });
            feather.replace();
        },
        rowCallback: function (row, data) {
            var hideout = data.hideout;
            console.log("row",data)
            if (hideout == 1) {
                $(row).addClass("blurrow");
                 $('td:eq(0)', row).removeClass('select-checkbox')
            }
        },
        "columns": obj,
        "aLengthMenu": [
            [10, 30, 50],
            [10, 30, 50]
        ],
        "iDisplayLength": 10,
        "language": {
            search: "",
            "searchPlaceholder": "search",
        }
    })
    $("#filtersubmit").text("Search")
    commontable.on('select deselect draw', function () {
        var all = commontable.rows({
            search: 'applied'
        }).count(); // get total count of rows
        var selectedRows = commontable.rows({
            selected: true,
            search: 'applied'
        }).count(); // get total count of selected rows
        var data = commontable.rows({
            selected: true,
            search: 'applied'
        }).data()
        NotifyArr = [];

        for (var i = 0; i < data.length; i++) {
            notify_Id = data[i]['id'];
            if (data[i]['hideout'] == 0) {
                NotifyArr.push(
                    notify_Id);
            }

        }
        if (selectedRows == 0) {
            $('#CheckAllButton i').attr('class', 'fa fa-square-o');
        } else {
            if (selectedRows == all) {
                $('#CheckAllButton i').attr('class', 'fa fa-check-square-o');
            }


        }
    });
    $('#CheckAllButton').on('click', function () {
        var all = commontable.rows({
            search: 'applied'
        }).count(); // get total count of rows
        var selectedRows = commontable.rows({
            selected: true,
            search: 'applied'
        }).count(); // get total count of selected rows
        if (selectedRows < all) {
            //Added search applied in case user wants the search items will be selected
            commontable.rows({
                search: 'applied'
            }).deselect();
            commontable.rows({
                search: 'applied'
            }).select();
        } else {
            commontable.rows({
                search: 'applied'
            }).deselect();
        }
    });
}
$(document).on('click', '.walletrechargefaileddetailview', function () {
    var id = this.id;
    var userinfo = CommonArr[CommonArr.map(function (item) {
        return item.trn_id;
    }).indexOf(parseInt(id))];
    $("#detailviewleads").text(userinfo.totalnoofleads);
    $("#detailviewleadsnotview").text(userinfo.totalnoofleadsnotview);
    $("#detailviewfirstname1").text(userinfo.firstname);
    $("#detailviewlastname1").text(userinfo.lastname);
    $("#detailviewemail1").text(userinfo.email);

    $("#detailviewusertype1").text(userinfo.usertype);
    $("#detailviewcompany1").text(userinfo.company_name);
    $("#detailviewpincode1").text(userinfo.pincode);
    $('#detailviewcity1').text(userinfo.city);
    $("#detailviewaddress1").text(userinfo.address);
    $("#detailviewstate1").text(userinfo.state);
    $("#detailviewmobilenumber1").text(userinfo.mobile_number);
    $("#detailviewpackagename1").text(userinfo.packagename);
    $("#detailviewpackagevalidity1").text(userinfo.packagevalidity);


    $("#walletrechargefaileddetailview").modal({
        backdrop: 'static',
        keyboard: false
    });
})

$(document).on('click', '.click', function () {
    var id = this.id;
    var input = new FormData();
    input.append('id', id);
    var url = "/payment/gettoptransaction";
    var method = 'post'
    commonajaxcall(url, input, method).then(function (result) {
        var data = result;
        $("#toptransactiontablebody").empty();
        $.each(data, function (key, item) {
            var is_success = item.is_success;
            if (is_success == true) {
                html = '<span class="badge badge-success">Success</span>';
            } else {
                html = '<span class="badge badge-danger">Failed</span>';
            }
            $("#toptransactiontablebody").append('<tr><td>' + item.package_name + '</td><td>' + item.package_value + '</td><td>' + html + '</td><td>' + moment.unix(item.transaction_time_as_timestamp).format('MM-DD-YYYY HH:mm A') + '</td></tr>')
        });
        $("#transactionmodal").modal({
            backdrop: 'static',
            keyboard: false
        });

    }).catch(function (error) {
        console.log(error);
    });
});
