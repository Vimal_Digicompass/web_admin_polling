//KYC - Approval
var CommonArr = [];
var NotifyArr = [];

$("#filtersubmit").on('click', function () {
    $('#kycapproval').DataTable().destroy();
    $("#filtersubmit").text("Processing...")
    common_load_data("click");
});

common_load_data('onload');

function common_load_data(type) {

    var fromdate = "";
    var todate = "";
    if (type == "onload") {
        fromdate = moment().subtract(7, 'd').format('YYYY-MM-DD');
        todate = moment().format('YYYY-MM-DD');
    } else {
        fromdate = $('#kycdate').data('daterangepicker').startDate.format('YYYY-MM-DD');
        todate = $('#kycdate').data('daterangepicker').endDate.format('YYYY-MM-DD');
    }

    var state = $("#state").val();
    var city = $("#city").val();
    var kycstatus = $("#kycstatus").val();
    var usertype = $("#usertype").val();
    load_data(fromdate, todate, usertype, state, city, kycstatus)
}

function load_data(fromdate = "", todate = "", usertype = "", state = "", city = "", status = "") {
    var getnotifydata = $("#getnotifydata").text();
    var obj = [];
    var columnDefs
    if (parseInt(getnotifydata) == 0) {
        columnDefs = [];
        obj = [{
                'data': 'name',
            'name': 'name',
                'orderable': false,
            },
            {
                'data': 'usertype',
                'name': 'usertype'
            },
            {
                'data': 'category',
                'name': 'category'
            },
            {
                'data': 'mobile_number',
                'name': 'mobile_number'
            },

            {
                'data': 'dateofregistration',
                'name': 'dateofregistration'
            },
            {
                'data': 'state',
                'name': 'state'
            },
            {
                'data': 'city',
                'name': 'city'
            },
            {
                'data': 'address',
                'name': 'address'
            },
            {
                'data': 'kycdocument',
                'name': 'kycdocument'
            },
            {
                'data': 'kycstatus',
                'name': 'kycstatus'
            },
            {
                'data': 'kyc_rejcted_reason',
                'name': 'kyc_rejcted_reason'
            },
             {
                 'data': 'crmusername',
                 'name': 'crmusername'
             },
            {
                'data': 'status',
                'name': 'status'
            },
            {
                'data': 'followup_date',
                'name': 'followup_date'
            },
            {
                'data': 'remarks',
                'name': 'remarks'
            },
            {
                'data': 'action',
                'name': 'action',

                orderable: false,
                searchable: false
            }
        ];
    } else {
        obj = [{
                'data': 'checkbox',
                'name': 'checkbox',
                'orderable': false,
                'className': 'select-checkbox'
            }, {
                'data': 'name',
                'name': 'name'
            },
            {
                'data': 'usertype',
                'name': 'usertype'
            },
            {
                'data': 'category',
                'name': 'category'
            },
            {
                'data': 'mobile_number',
                'name': 'mobile_number'
            },

            {
                'data': 'dateofregistration',
                'name': 'dateofregistration'
            },
            {
                'data': 'state',
                'name': 'state'
            },
            {
                'data': 'city',
                'name': 'city'
            },
            {
                'data': 'address',
                'name': 'address'
            },
            {
                'data': 'kycdocument',
                'name': 'kycdocument'
            },
            {
                'data': 'kycstatus',
                'name': 'kycstatus'
            },
            {
                'data': 'kyc_rejcted_reason',
                'name': 'kyc_rejcted_reason'
            },
            {
                'data': 'crmusername',
                'name': 'crmusername'
            },
            {
                'data': 'status',
                'name': 'status'
            },
            {
                'data': 'followup_date',
                'name': 'followup_date'
            },
            {
                'data': 'remarks',
                'name': 'remarks'
            },
            {
                'data': 'action',
                'name': 'action',

                orderable: false,
                searchable: false
            }
        ]
        columnDefs = [

            {
                orderable: false,
                className: 'select-checkbox',
                targets: 0,
            }
        ]
    }
    var searchcallstatus = $("#searchcallstatus").val();
    var commontable = $('#kycapproval').DataTable({
        serverSide: true,
        responsive: false,
        processing: true,
        scrollX: true,
        dom: 'lBfrtip',
        columnDefs: columnDefs,
        select: {
            style: 'multi', // 'single', 'multi', 'os', 'multi+shift'
            selector: 'td:first-child',
        },
        "ajax": {
            "url": '/kyc/getkycapprovallist',
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            "type": "POST",
            data: {
                id: $('meta[name="fetchid"]').attr('content'),
                fromdate: fromdate,
                todate: todate,
                searchcallstatus: searchcallstatus,
                usertype: usertype,
                state: state,
                city: city,
                status: status
            },
        },
        rowCallback: function (row, data) {
            var hideout = data.hideout;
            if (hideout == 1) {
                $(row).addClass("blurrow");
                $('td:eq(0)', row).removeClass('select-checkbox');
            }
        },
        drawCallback: function (settings) {
            CommonArr = settings.json.data
            $('.tools').tooltip({
                container: 'body'
            });
            feather.replace();

        },
        "columns": obj,
        "aLengthMenu": [
            [10, 30, 50],
            [10, 30, 50]
        ],
        "iDisplayLength": 10,
        "language": {
            search: "",
            "searchPlaceholder": "search",
        }
    })
    $("#filtersubmit").text("Search")
    commontable.on('select deselect draw', function () {
        var all = commontable.rows({
            search: 'applied'
        }).count(); // get total count of rows
        var selectedRows = commontable.rows({
            selected: true,
            search: 'applied'
        }).count(); // get total count of selected rows
        var data = commontable.rows({
            selected: true,
            search: 'applied'
        }).data()
        NotifyArr = [];
        for (var i = 0; i < data.length; i++) {
            notify_Id = data[i]['id'];
            if (data[i]['hideout'] == 0) {
                NotifyArr.push(
                    notify_Id);
            }

        }

        if (selectedRows == 0) {
            $('#CheckAllButton i').attr('class', 'fa fa-square-o');
        } else {
            if (selectedRows == all) {
                $('#CheckAllButton i').attr('class', 'fa fa-check-square-o');
            }

            //
        }
    });
    $('#CheckAllButton').on('click', function () {
        var all = commontable.rows({
            search: 'applied'
        }).count(); // get total count of rows
        var selectedRows = commontable.rows({
            selected: true,
            search: 'applied'
        }).count(); // get total count of selected rows
        if (selectedRows < all) {
            //Added search applied in case user wants the search items will be selected
            commontable.rows({
                search: 'applied'
            }).deselect();
            commontable.rows({
                search: 'applied'
            }).select();
        } else {
            commontable.rows({
                search: 'applied'
            }).deselect();
        }
    });
}
//End KYC - Approval
