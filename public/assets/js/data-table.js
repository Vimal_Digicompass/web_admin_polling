$(function () {
    'use strict';
    $('#dataTableExample').DataTable({

        "aLengthMenu": [
            [10, 30, 50],
            [10, 30, 50]
        ],
        "iDisplayLength": 10,
        "language": {
            search: "",
            "searchPlaceholder": "search",
        }
    });

    roleTable = $('#roleTable').DataTable({
        serverSide: true,
        responsive: false,
        processing: true,
        "ajax": {
            "url": 'roles',
            'headers': {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            "type": "GET"
        },
        drawCallback: function (settings) {
            roleArr = settings.json.data
            console.log(roleArr)
            feather.replace();
            $('.edit').tooltip();
            $('.delete').tooltip();
            $('.permission').tooltip();
        },
        "columns": [{
                'data': 'role_title',
            'name': 'role_title',
                'orderable': false,
            },
            {
                'data': 'noofuser',
                'name': 'noofuser'
            },
            {
                'data': 'role_type',
                'name': 'role_type'
            },
            {
                'data': 'created_at',
                'name': 'created_at'
            },
            {
                'data': 'updated_at',
                'name': 'updated_at'
            },
            {
                'data': 'action',
                'name': 'action',

                orderable: false,
                searchable: false
            }
        ],
        "aLengthMenu": [
            [10, 30, 50],
            [10, 30, 50]
        ],
        "iDisplayLength": 10,
        "language": {
            search: "",
            "searchPlaceholder": "search",
        }
    })

    var notficationtable = $('#notficationtable').DataTable({
            serverSide: true,
            responsive: false,
            processing: true,
            "ajax": {
                "url": '/users/getnotificationlist',
                'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                "type": "GET"
            },
            drawCallback: function (settings) {
                feather.replace();
            },
            "columns": [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false

                },
                {
                    'data': 'description',
                    'name': 'description'
                },
                {
                    'data': 'created_at',
                    'name': 'created_at'
                }, {
                    'data': 'updated_at',
                    'name': 'updated_at'
                }

            ],
            "aLengthMenu": [
                [10, 30, 50],
                [10, 30, 50]
            ],
            "iDisplayLength": 10,
            "language": {
                search: "",
                "searchPlaceholder": "search",
            }
        }

    )
    userTable = $('#userTable').DataTable({
        serverSide: true,
        responsive: false,
        processing: true,
        "ajax": {
            "url": 'lists',
            'headers': {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            "type": "GET"
        },
        drawCallback: function (settings) {
            userArr = settings.json.data
            feather.replace();
            $('.useredit').tooltip();
            $('.statusactive').tooltip();
            $('.statusinactive').tooltip();
        },
        "columns": [{
                'data': 'name',
            'name': 'name',
                'orderable': false,
            },
            {
                'data': 'username',
                'name': 'username'
            },
            {
                'data': 'mobilenumber',
                'name': 'mobilenumber'
            },
            {
                'data': 'rolename',
                'name': 'rolename'
            },
            {
                'data': 'created_at',
                'name': 'created_at'
            },
            {
                'data': 'updated_at',
                'name': 'updated_at'
            },
            {
                'data': 'active',
                'name': 'active'
            },
            {
                'data': 'action',
                'name': 'action',

                orderable: false,
                searchable: false
            }
        ],
        "aLengthMenu": [
            [10, 30, 50],
            [10, 30, 50]
        ],
        "iDisplayLength": 10,
        "language": {
            search: "",
            "searchPlaceholder": "search",
        }
    })

    $('#approveposttable').DataTable({

        "aLengthMenu": [
            [10, 30, 50],
            [10, 30, 50]
        ],
        "iDisplayLength": 10,
        "language": {
            search: "",
            "searchPlaceholder": "search",
        },
        scrollX: true,
        dom: 'lBfrtip'
    });

});
